import React from 'react';
import Paper from '@material-ui/core/Paper';

import {handleTabChange} from "../actions/dataActions";
import {connect} from 'react-redux';
import TabsEnglish from "../components/componentsFooter/TabsEnglish"
import TabsGerman from "../components/componentsFooter/TabsGerman"
import { withStyles } from '@material-ui/core/styles';
import classNames from "classnames";

const styles = theme => ({
    tabs: {
        overflow: "visible",
        position: "fixed",
        bottom: 0,
        width:"100%",
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    drawerOpen: {
        marginLeft: 250,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }
});

class MenuTabs extends React.Component {

    render() {

        const { classes } = this.props;

        const handleChange = (value) => {
            this.props.handleTabChange(value)

        };

        return (
                <Paper className={classNames(classes.tabs, this.props.isDrawerOpen && classes.drawerOpen)} >
                    {this.props.language === "English" ?
                        <TabsEnglish handleChange={handleChange} value={this.props.value}/>
                    : this.props.language === "German" ?
                            <TabsGerman handleChange={handleChange} value={this.props.value}/>
                            : null}
                </Paper>
        )
    }
}

const mapStateToProps = state => ({
    value: state.allReducers.tabsValue,
    language: state.allReducers.language,
    isDrawerOpen: state.allReducers.isDrawerOpen
        })

export default withStyles(styles)(connect(mapStateToProps,{handleTabChange})(MenuTabs))