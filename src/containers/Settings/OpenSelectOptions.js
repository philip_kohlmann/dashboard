
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue'
import amber from '@material-ui/core/colors/amber'
import {connect} from 'react-redux'
import red from '@material-ui/core/colors/red'
import green from '@material-ui/core/colors/green'
import yellow from '@material-ui/core/colors/yellow'
import {compose} from 'redux'
import {handleOpenPrimaryColor, handleClosePrimaryColor, handleOpenSecondaryColor, handleCloseSecondaryColor,
handleOpenFontsize, handleCloseFontsize, handleOpenFontstyle, handleCloseFontstyle, changeFontsize, changeFontstyle,
changeColorPrimary, changeColorSecondary} from "../../actions/dataActions";
import PrimaryColorSelect from "../../components/componentsSettings/componentsDisplaySettings/PrimaryColorSelect"
import SecondaryColorSelect from "../../components/componentsSettings/componentsDisplaySettings/SecondaryColorSelect"
import FontsizeSelect from "../../components/componentsSettings/componentsDisplaySettings/FontsizeSelect"
import FontstyleSelect from "../../components/componentsSettings/componentsDisplaySettings/FontstyleSelect"

const styles = theme => ({
});

class OpenSelectOptions extends React.Component {
    state = {
        primaryOpen: false,
        secondaryOpen: false,
        fontsizeOpen: false,
        fontstyleOpen: false
    };

    handleChange = event => {
            console.log(event.target.name)
            console.log(event.target.value)
            if (event.target.name === "fontsize") {
                this.props.changeFontsize(event.target.value)
            }
            else if (event.target.name === "fontstyle") {
                this.props.changeFontstyle(event.target.value)
            }
            else if (event.target.name === "primary") {
                var color = {
                    value: null,
                    name: ""
                }
                switch (event.target.value) {
                    case "Blue":
                        color.value = "#0065B3",
                            color.name = "Blue"
                        break;
                    case "Red":
                        color.value = red[500],
                            color.name = "Red"
                        break;
                    case "Green":
                        color.value = green[500],
                            color.name = "Green"
                        break;
                    case "Yellow":
                        color.value = yellow[500],
                            color.name = "Yellow"
                        break;
                    case "Amber":
                        color.value = "#FF6600",
                            color.name = "Amber"
                        break;
                }
                this.props.changeColorPrimary(color)
            }
            else if (event.target.name === "secondary") {
                var color = {
                    value: null,
                    name: ""
                }
                switch (event.target.value) {
                    case "Blue":
                        color.value = "#0065B3",
                            color.name = "Blue"
                        break;
                    case "Red":
                        color.value = red[500],
                            color.name = "Red"
                        break;
                    case "Green":
                        color.value = green[500],
                            color.name = "Green"
                        break;
                    case "Yellow":
                        color.value = yellow[500],
                            color.name = "Yellow"
                        break;
                    case "Amber":
                        color.value = "#FF6600",
                            color.name = "Amber"
                        break;
                }
                this.props.changeColorSecondary(color)
            }
    };

    render() {

        return (
            <form autoComplete="off">
                <table width="95%" >
                 <tbody>
                 <tr>
                <PrimaryColorSelect language={this.props.language} primaryColorOpen={this.props.primaryColorOpen} handleClosePrimaryColor={this.props.handleClosePrimaryColor}
                                    handleOpenPrimaryColor={this.props.handleOpenPrimaryColor} primaryColor={this.props.primaryColor} handleChange={this.handleChange}/>

                 <SecondaryColorSelect language={this.props.language} secondaryColorOpen={this.props.secondaryColorOpen} handleCloseSecondaryColor={this.props.handleCloseSecondaryColor}
                                      handleOpenSecondaryColor={this.props.handleOpenSecondaryColor} secondaryColor={this.props.secondaryColor} handleChange={this.handleChange}/>
                 </tr>
                 <tr>
                     <FontsizeSelect language={this.props.language} fontsizeOpen={this.props.fontsizeOpen} handleCloseFontsize={this.props.handleCloseFontsize}
                                     handleOpenFontsize={this.props.handleOpenFontsize} fontsize={this.props.fontsize} handleChange={this.handleChange}/>
                 </tr>
                 <tr>
                     <FontstyleSelect language={this.props.language} fontstyleOpen={this.props.fontstyleOpen} handleCloseFontstyle={this.props.handleCloseFontstyle}
                                      handleOpenFontstyle={this.props.handleOpenFontstyle} fontstyle={this.props.fontstyle} handleChange={this.handleChange}/>
                 </tr>
                 </tbody>
                </table>
            </form>
        );
    }
}

OpenSelectOptions.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    primaryColorOpen: state.allReducers.primaryColorOpen,
    secondaryColorOpen: state.allReducers.secondaryColorOpen,
    fontsizeOpen: state.allReducers.fontsizeOpen,
    fonststyleOpen: state.allReducers.fontstyleOpen,
    primaryColor: state.allReducers.colorPrimary,
    secondaryColor: state.allReducers.colorSecondary,
    fontsize: state.allReducers.fontsize,
    fontstyle: state.allReducers.fontstyle,
    language: state.allReducers.language
})

export default compose(withStyles(styles, {withTheme: true}),connect(mapStateToProps,{handleOpenPrimaryColor, handleClosePrimaryColor,
handleOpenSecondaryColor, handleCloseSecondaryColor, changeFontsize, changeFontstyle,   handleOpenFontsize, handleCloseFontsize,
    handleOpenFontstyle, handleCloseFontstyle, changeColorPrimary, changeColorSecondary}))(OpenSelectOptions);
