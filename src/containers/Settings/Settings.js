import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {
    handleDrawerOpen,
    handleDrawerClose,
    resetSettings,
    openNewUserForm,
    closeNewUserForm,
    handleDrawerChange,
    logOut
} from "../../actions/dataActions";
import MenuBar from "../../components/componentsSettings/MenuBar(Deprecated)"
import DrawerMenu from "../../components/componentsSettings/Drawer"
import NewUserDialog from "../../components/componentsSettings/NewUserDialog"
import DisplayPanel from "../../components/componentsSettings/componentsDisplaySettings/DisplayPanel"
import ConfigurationsPanel from "../../components/componentsSettings/componentsConfigurationSettings/ConfigurationsPanel"
import ResetSettingsPanel from "../../components/componentsSettings/ResetSettingsPanel"
import SecurityPanel from "../../components/componentsSettings/componentsSecuritySettings/SecurityPanel"
import MenuBarDashboardEnglish
    from "../../components/componentsDashboard/componentsDashboardEnglish/MenuBarDashboardEnglish";
import MenuBarDashboardGerman
    from "../../components/componentsDashboard/componentsDashboardGerman/MenuBarDashboardGerman";




const styles = theme => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        height: '100vh',
        overflow: 'auto',
    }
});

class DashboardSettings extends React.Component {


    render() {

        const { classes } = this.props;

        return (
            <React.Fragment>
                <CssBaseline />
                <div className={classes.root}>
                    {this.props.language === "English" ?
                        <MenuBarDashboardEnglish isDrawerOpen={this.props.isDrawerOpen} handleDrawerOpen={this.props.handleDrawerOpen} loggedIn={this.props.loggedIn} handleDrawerChange={this.props.handleDrawerChange}
                                                 logOut={this.props.logOut} tooltipsEnabled={this.props.tooltipsEnabled} nameEnglish={"Settings"} userName={this.props.userName}
                                                 />
                    : this.props.language === "German" ?
                            <MenuBarDashboardGerman isDrawerOpen={this.props.isDrawerOpen} handleDrawerOpen={this.props.handleDrawerOpen} loggedIn={this.props.loggedIn} handleDrawerChange={this.props.handleDrawerChange}
                                                     logOut={this.props.logOut} tooltipsEnabled={this.props.tooltipsEnabled} nameGerman={"Einstellungen"} userName={this.props.userName}
                            />
                            : null}
                        <DrawerMenu language={this.props.language} isDrawerOpen={this.props.isDrawerOpen} handleDrawerClose={this.props.handleDrawerClose}/>
                    <main className={classes.content}>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <ConfigurationsPanel language={this.props.language}/>
                        <DisplayPanel language={this.props.language} />
                        <SecurityPanel language={this.props.language}/>
                        <NewUserDialog language={this.props.language} newUserFormOpen={this.props.newUserFormOpen} closeNewUserForm={this.props.closeNewUserForm}/>
                        <ResetSettingsPanel language={this.props.language} resetSettings={this.props.resetSettings} fontsize={this.props.fontSize}/>
                    </main>
                </div>
            </React.Fragment>
        );
    }
}

DashboardSettings.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    isDrawerOpen: state.allReducers.drawerOpen,
    newUserFormOpen: state.allReducers.newUserFormOpen,
    language: state.allReducers.language,
    loggedIn: state.allReducers.isAccessGranted,
    tooltipsEnabled: state.allReducers.tooltipsEnabled,
    userName: state.allReducers.userName,
    fontSize: state.allReducers.fontsize

});


export default compose(
    withStyles(styles, {
        name: 'DashboardSettings', withTheme: true
    }),
    connect(mapStateToProps, {handleDrawerOpen, handleDrawerClose, resetSettings, openNewUserForm, logOut, handleDrawerChange,
        closeNewUserForm})
)(DashboardSettings);