
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {handleDeleteChartsPossible, handleLoggedInToggle, openNewUserForm} from "../../actions/dataActions";
import StayLoggedInSettings from "../../components/componentsSettings/componentsSecuritySettings/StayLoggedInSettings"
import DeleteChartsSettings from "../../components/componentsSettings/componentsSecuritySettings/DeleteChartsSettings"
import AddNewUser from "../../components/componentsSettings/componentsSecuritySettings/AddNewUser"


class OpenSelectOptionsSecurity extends React.Component {

    render() {

        return (
            <form autoComplete="off">
                <table width="95%">
                    <tbody>
                    <tr>
                        <StayLoggedInSettings language={this.props.language} stayLoggedIn={this.props.stayLoggedIn} onChange={() => this.props.handleLoggedInToggle(this.props.stayLoggedIn)}/>
                    </tr>
                    <tr>
                        <DeleteChartsSettings language={this.props.language} deleteChartsPossible={this.props.deleteChartsPossible}
                                              handleDeleteChartsPossible={this.props.handleDeleteChartsPossible} />
                    </tr>
                    <tr>
                        <AddNewUser language={this.props.language} openNewUserForm={this.props.openNewUserForm}/>
                    </tr>
                    </tbody>
                </table>
            </form>
        );
    }
}

OpenSelectOptionsSecurity.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    stayLoggedIn : state.allReducers.stayLoggedIn,
    deleteChartsPossible: state.allReducers.deleteChartsPossible,
    language: state.allReducers.language
})

export default connect(mapStateToProps,{handleDeleteChartsPossible, handleLoggedInToggle, openNewUserForm })(OpenSelectOptionsSecurity);
