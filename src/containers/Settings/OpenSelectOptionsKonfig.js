
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import {handleToggleLanguage, handleToggleCharts, handleToggleTooltips, changeLanguage, changeChartLibrary} from "../../actions/dataActions";
import LanguageSelect from "../../components/componentsSettings/componentsConfigurationSettings/LanguageSelect"
import ChartLibrarySelect from "../../components/componentsSettings/componentsConfigurationSettings/ChartLibrarySelect"
import TooltipSettings from "../../components/componentsSettings/componentsConfigurationSettings/TooltipSettings"


class OpenSelectOptionsKonfig extends React.Component {

    handleChange = event => {
        if(event.target.name === "language"){
            this.props.changeLanguage(event.target.value)
        }
        else if(event.target.name === "chartLibrary"){
            this.props.changeChartLibrary(event.target.value)
        }
    };


    render() {

        return (
            <form autoComplete="off">
                <table width="95%">
                    <tbody>
                    <tr>
                        <LanguageSelect language={this.props.language} languageOpen={this.props.languageOpen} handleToggleLanguage={this.props.handleToggleLanguage}
                        handleChange={this.handleChange}/>
                    </tr>
                    <tr>
                        <ChartLibrarySelect language={this.props.language} chartsOpen={this.props.chartsOpen} handleToggleCharts={this.props.handleToggleCharts}
                                            library={this.props.library} handleChange={this.handleChange}/>
                    </tr>
                    <tr>
                        <TooltipSettings language={this.props.language} tooltipsEnabled={this.props.tooltipsEnabled} handleToggleTooltips={this.props.handleToggleTooltips}
                        />
                    </tr>
                    </tbody>
                </table>
            </form>
        );
    }
}

OpenSelectOptionsKonfig.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state =>({
  languageOpen: state.allReducers.languageOpen,
  chartsOpen: state.allReducers.chartsOpen,
  tooltipsEnabled: state.allReducers.tooltipsEnabled,
    language: state.allReducers.language,
    library: state.allReducers.library,
})

export default connect(mapStateToProps, {handleToggleLanguage, changeLanguage, changeChartLibrary, handleToggleCharts, handleToggleTooltips})(OpenSelectOptionsKonfig);
