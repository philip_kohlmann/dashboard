import React, {Component} from  'react'
import {loginPressed, logOut} from "../actions/dataActions";
import {connect} from 'react-redux';
import LoginFormEnglish from "../components/loginForm/LoginFormEnglish"
import LoginFormGerman from "../components/loginForm/LoginFormGerman"




class Form extends Component {
    state = {
        username: "",
        password: "",
        showPassword: false
    }


    handleChange = name => ({target: {value}}) =>
        this.setState({
            [name]: value
        })

    handleMouseDownPassword = event => {
        event.preventDefault();
    };

    handleClickShowPassword = () => {
        this.setState(state => ({ showPassword: !state.showPassword }));
    };

    handleSubmit = () => {
        const user={
            username: this.state.username,
            password: this.state.password
        }
        this.setState({
            username: "",
            password: ""
        })
        this.props.loginPressed(user)
    }

    render(){

        const {username, password, showPassword} = this.state

        return(
            <form>
                {this.props.language === "English" ?
                <LoginFormEnglish username={username} password={password} handleChangeUsername={this.handleChange("username")}
                                  handleChangePassword={this.handleChange("password")}  handleMouseDownPassword={this.handleMouseDownPassword}
                                  handleClickShowPassword={this.handleClickShowPassword} handleSubmit={this.handleSubmit} showPassword={showPassword}/>
                    : this.props.language === "German" ?
                    <LoginFormGerman username={username} password={password} handleChangeUsername={this.handleChange("username")}
                                      handleChangePassword={this.handleChange("password")}  handleMouseDownPassword={this.handleMouseDownPassword}
                                      handleClickShowPassword={this.handleClickShowPassword} handleSubmit={this.handleSubmit} showPassword={showPassword}/>
                    : null}
            </form>)
    }
}

const mapStateToProps = state => ({
    language: state.allReducers.language
})

export default connect(mapStateToProps,{loginPressed, logOut})(Form);