import React, {Component} from  'react'
import {addUser, closeNewUserForm} from "../actions/dataActions";
import {connect} from 'react-redux';
import NewUserFormEnglish from "../components/componentsNewUserForm/NewUserFormEnglish"
import NewUserFormGerman from "../components/componentsNewUserForm/NewUserFormGerman"






class NewUserForm extends Component {
    state = {
        username: "",
        password: ""
    }


    handleChange = name => ({target: {value}}) =>
        this.setState({
            [name]: value
        })

    handleSubmit = () => {
        const user={
            username: this.state.username,
            password: this.state.password
        }
        this.setState({
            username: "",
            password: ""
        })
        this.props.closeNewUserForm();
        try {
            this.props.addUser(user)
        }
        catch(err){
            console.log("Async Error")
        }

    }

    render(){

        const {username, password} = this.state

        return(
            <form>
                    {this.props.language === "English"?
                        <NewUserFormEnglish username={username} password={password} handleChangeUsername={this.handleChange("username")}
                        handleChangePassword={this.handleChange("password")} handleSubmit={this.handleSubmit}/>
                        : this.props.language === "German"?
                            <NewUserFormGerman username={username} password={password} handleChangeUsername={this.handleChange("username")}
                                                handleChangePassword={this.handleChange("password")} handleSubmit={this.handleSubmit}/>
                            : null}
            </form>)
    }
}

const mapStateToProps = state => ({
    language: state.allReducers.language
});

export default connect(mapStateToProps,{addUser, closeNewUserForm})(NewUserForm);