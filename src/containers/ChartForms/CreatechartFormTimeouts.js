import React, {Component} from  'react'
import {createChartTimeouts, createTableTimeouts, deleteDateTimeouts, closeChartFormTimeouts, timelineSelectedTimeouts, singleDatesSelectedTimeouts, addDateTimeouts} from "../../actions/dataActions";
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import SingleDateSelector from "../../components/componentsChartForm/SingleDateSelector"
import ChartFormEnglisch from "../../components/componentsChartForm/componentsChartFormEnglish/ChartFormEnglisch"
import ChartFormGerman from "../../components/componentsChartForm/componentsChartFormGerman/ChartFormGerman"

const styles = theme => ({
    formControl: {
        margin: theme.spacing.unit * 3,
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: "6%"
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
});

class ChartFormTimeouts extends Component {
    state = {
        simple: null,
        title: "",
        type: "",
        color: "",
        labels: [],
        datasets: [{
            label: "",
            data: [],
        }],
        startYear: "",
        startMonth: "",
        endYear: "",
        endMonth: "",
        indicator: "",
        test:[],
        year0:"",
        year1:"",
        year2:"",
        year3:"",
        year4:"",
        year5:"",
        year6:"",
        year7:"",
        year8:"",
        year9:"",
        year10:"",
        month0:"",
        month1:"",
        month2:"",
        month3:"",
        month4:"",
        month5:"",
        month6:"",
        month7:"",
        month8:"",
        month9:"",
        month10:"",
        years:[],
        months:[],
        date: "",
        rbValue: null

    }


    handleChange = name => ({target: {value}}) =>
        this.setState({
            [name]: value
        })

    hello = event =>{
        this.setState({
            date: event.target.value
        })
    }


    handleChangeTwo = event => {
        var index = parseInt(event.target.name[event.target.name.length - 1])
        this.setState({ [event.target.name]: event.target.value });
        if(event.target.name.includes("year")){
            this.state.years[index] = event.target.value
        }else if (event.target.name.includes("month")){
            this.state.months[index] = event.target.value
        }
    };


    handleChangeRb = event => {
        this.setState({ rbValue: event.target.value });

        if(event.target.value === "simple"){
            this.setState({
                simple: true,
            })
        }else if (event.target.value === "advanced"){
            this.setState({
                simple:false,
            })
        }

        if(event.target.value === "Timeline"){
            this.props.timelineSelectedTimeouts()
            this.setState({
                years:[],
                months:[],
                year0:"",
                year1:"",
                year2:"",
                year3:"",
                year4:"",
                year5:"",
                year6:"",
                year7:"",
                year8:"",
                year9:"",
                year10:"",
                month0:"",
                month1:"",
                month2:"",
                month3:"",
                month4:"",
                month5:"",
                month6:"",
                month7:"",
                month8:"",
                month9:"",
                month10:"",
            })
        }else{
            this.props.singleDatesSelectedTimeouts(),
                this.setState({
                    startYear: "",
                    startMonth: "",
                    endYear: "",
                    endMonth: "",
                })
        }
    };



    handleSubmit = () => {

        var dt = new Date();
        var month = dt.getMonth()+1;
        var day = dt.getDate();
        var year = dt.getFullYear();
        var date = day + "/" + month + "/" + year
        if(this.props.isTimelineSelected === true || this.state.type === "Table") {
            var chart
            if(this.state.simple !== false){
                chart = {
                    title: this.state.title,
                    color: this.state.color,
                    selection: this.state.type,
                    chartTimeStart: {
                        year: this.state.startYear,
                        month: this.state.startMonth
                    },
                    chartTimeEnd: {
                        year: this.state.endYear,
                        month: this.state.endMonth
                    },
                    indicator: "Timeouts",
                    origin: "Timeouts",
                    timeline: true,
                    date: date,
                    simple: this.state.simple
                };
            }else{
                chart = {
                    title: this.state.title,
                    color: this.state.color,
                    selection: this.state.type,
                    dateSelected: this.state.date,
                    indicator: "Timeouts",
                    origin: "Timeouts",
                    timeline: true,
                    date: date,
                    simple: this.state.simple
                };
            }
            if (chart.selection === "Table") {
                this.props.createTableTimeouts(chart);
            } else {
                this.props.createChartTimeouts(chart);
            }
            this.props.closeChartFormTimeouts();
        }else{

            var helpArray= []
            for(var i = 0; i < this.state.years.length; i++){
                var monthString
                if(parseInt(this.state.months[i]) < 10){
                    monthString = "0" + this.state.months[i]
                }
                var string = this.state.years[i] + monthString + ""
                helpArray[i] = parseInt(string)
            }
            helpArray.sort(function(a,b){return a-b});
            for(var j = 0; j < this.state.years.length; j++){
                var k = helpArray[j].toString()
                this.state.years[j] = k.substring(0,4)
                if(parseInt(k.substring(4)) > 9){
                    this.state.months[j] = k.substring(4)
                }else{
                    this.state.months[j] = k.substring(5)
                }
            }
            const chart = {
                title: this.state.title,
                color: this.state.color,
                selection: this.state.type,
                indicator: "Timeouts",
                origin: "Timeouts",
                timeline: false,
                date: date,
                chartDates:{
                    years: this.state.years,
                    months: this.state.months,
                    counter: this.state.years.length
                }
            };
            if (chart.selection === "Table") {
                this.props.createTableTimeouts(chart);
            } else {
                this.props.createChartTimeouts(chart);
            }
            this.props.closeChartFormTimeouts();
        }
    }

    addDate = () => {
        this.props.addDateTimeouts();
    }

    deleteDate = () => {
        if(this.state.years.length === this.props.dateCounter){
            this.state.years.pop()
        }
        if(this.state.months.length === this.props.dateCounter){
            this.state.months.pop()
        }
        this.props.deleteDateTimeouts();

        this.props.dateCounter === 1? this.state.year0 = "" : this.props.dateCounter === 2? this.state.year1 = "" : this.props.dateCounter === 3? this.state.year2 = "":this.props.dateCounter === 4? this.state.year3= "":
            this.props.dateCounter === 5? this.state.year4= "": this.props.dateCounter === 6? this.state.year5= "": this.props.dateCounter === 7? this.state.year6= "":
                this.props.dateCounter === 8? this.state.year7= "":this.props.dateCounter === 9? this.state.year8= "":this.props.dateCounter === 10? this.state.year9= "": this.state.year10 = ""

        this.props.dateCounter === 1? this.state.month0 = "" :this.props.dateCounter === 2? this.state.month1 = "" : this.props.dateCounter === 3? this.state.month2 = "":this.props.dateCounter === 4? this.state.month3= "":
            this.props.dateCounter === 5? this.state.month4= "": this.props.dateCounter === 6? this.state.month5= "": this.props.dateCounter === 7? this.state.month6= "":
                this.props.dateCounter === 8? this.state.month7= "":this.props.dateCounter === 9? this.state.month8= "":this.props.dateCounter === 10? this.state.month9= "": this.state.month10 = ""

    }



    render(){



        const {title, type, color,startYear, startMonth, endYear, endMonth, indicator,
            year1,year2, year0, year3, year4, year5, year6, year7, year8, year9, year10,
            month0, month1, month2, month3, month4, month5, month6, month7, month8, month9, month10, years, months, simple} = this.state


        var Dates = this.props.allDates.map( date => {
            return(
                <SingleDateSelector year1={year1} year2={year2} year0={year0} year3={year3} year4={year4} year5={year5} year6={year6} year7={year7} year8={year8}
                                    year9={year9} year10={year10} month0={month0} month1={month1} month2={month2} month3={month3} month4={month4}
                                    month5={month5} month6={month6} month7={month7} month8={month8} month9={month9} month10={month10} date={date} handleChange={this.handleChangeTwo}/>
            )
        })
        return(
            <form>
                {this.props.language === "English" ?
                    <ChartFormEnglisch handleChange={this.handleChange} handleChangeRb={this.handleChangeRb} hello={this.hello} addDate={this.addDate} deleteDate={this.deleteDate}
                                       handleSubmit={this.handleSubmit} type={type} color={color} simple={simple} indicator={indicator} isTimelineSelected={this.props.isTimelineSelected}
                                       startYear={startYear} startMonth={startMonth} endYear={endYear} endMonth={endMonth} isSingleDatesSelected={this.props.isSingleDatesSelected}
                                       dates={Dates} years={years} months={months} title={title} rbValue={this.state.rbValue} formType={this.props.formType}/>:
                    this.props.language === "German" ?
                        <ChartFormGerman handleChange={this.handleChange} handleChangeRb={this.handleChangeRb} hello={this.hello} addDate={this.addDate} deleteDate={this.deleteDate}
                                         handleSubmit={this.handleSubmit} type={type} color={color} simple={simple} indicator={indicator} isTimelineSelected={this.props.isTimelineSelected}
                                         startYear={startYear} startMonth={startMonth} endYear={endYear} endMonth={endMonth} isSingleDatesSelected={this.props.isSingleDatesSelected}
                                         dates={Dates} years={years} months={months} title={title} rbValue={this.state.rbValue} formType={this.props.formType}/>
                        :null
                }
            </form>)
    }
}


const mapStateToProps = state => ({
    isTimelineSelected: state.allReducers.timelineSelectedTimeouts,
    isSingleDatesSelected: state.allReducers.singleDatesSelectedTimeouts,
    allDates: state.allReducers.datesTimeouts,
    dateCounter: state.allReducers.dateCounterTimeouts,
    language: state.allReducers.language,
    formType: state.allReducers.formType

});

export default withStyles(styles)(connect(mapStateToProps,{createChartTimeouts, createTableTimeouts, deleteDateTimeouts, closeChartFormTimeouts, timelineSelectedTimeouts, singleDatesSelectedTimeouts, addDateTimeouts})(ChartFormTimeouts));