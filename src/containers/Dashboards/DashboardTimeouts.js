import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {
    closeChartFormTimeouts,
    deleteChartTimeouts,
    deleteTableTimeouts,
    fetchData,
    fetchDataTimeouts,
    fetchTableDataTimeouts,
    handleDrawerClose,
    handleDrawerOpen,
    openChartFormTimeouts,
    logOut,
    handleDrawerChange,
    openTableComment,
    editTableComment,
    closeTableComment,
    getComment,
    editChartComment,
    openChartEdit,
    closeChartEdit,
    editChart,
    closeNoAdminAccess,
    openDotDetailsDialog,
    closeDotDetailsDialog,
    getDotDetails,
    fetchDataAllRequests,

} from "../../actions/dataActions";
import BarChart from "../../components/charts/Bar";
import LineChart from "../../components/charts/Line";
import PieChart from "../../components/charts/Pie";
import simpleTable from "../../components/table/simpleTable";
import BarRechart from "../../components/charts/BarChart";
import LineRechart from "../../components/charts/LineChart";
import Delete from "@material-ui/icons/Delete";
import Tooltip from "@material-ui/core/Tooltip";
import DashboardEnglish from "../../components/componentsDashboard/componentsDashboardEnglish/DashboardEnglish";
import DashboardGerman from "../../components/componentsDashboard/componentsDashboardGerman/DashboardGerman";
import ChartForm from "../ChartForms/CreatechartFormTimeouts"
import Edit from "@material-ui/icons/Edit";
import Message from "@material-ui/icons/Message"


const styles = theme => ({
    root: {
    },
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: -10,
    },
});

class DashboardTimeouts extends React.Component {

    constructor(){
        super();
        this.state = {
            chartData:{},
            comment: "",
            id: null,
            type: "",
            typeComment: "",
            title:"",
            color:"",
            table: false
        }
    }

    componentWillMount() {
        this.props.fetchData();
        this.props.fetchDataTimeouts();
        this.props.fetchTableDataTimeouts();
    }

    componentDidMount(){
        this.props.fetchDataAllRequests()
    }

    componentDidUpdate(prevProps) {
        if (this.props.allCharts === prevProps.allCharts) {
            this.props.fetchDataTimeouts();
            if (this.props.allTables === prevProps.allTables) {
                this.props.fetchTableDataTimeouts();
                this.props.fetchTableDataTimeouts();
                this.props.fetchTableDataTimeouts();
            }
        }
    }

    handleDotClick(dot, self){

        var month = parseInt((dot.substring(dot.length - 7, dot.length - 5))) - 1
        var year = parseInt((dot.substring(dot.indexOf("/") + 1))) - 2012

        let i = 1
        let j = 0

        let dotDetails = dot += "\n \n"

        for (let request of self.props.requests[0].Jahr[year][month]){
            dotDetails += ("HTTP-Request " + i + ":" + "\n")
            for(j ; j < 10 ; j++){
                switch(j){
                    case 0:
                        dotDetails += ("HTTP-Body Short: " + request[j] + "\n")
                        break;
                    case 1:
                        dotDetails += ("Request UUID: " + request[j] + "\n")
                        break;
                    case 2:
                        dotDetails += ("Request URL: " + request[j] + "\n")
                        break;
                    case 3:
                        dotDetails += ("HTTP-Method: " + request[j] + "\n")
                        break;
                    case 4:
                        dotDetails += ("Origin: " + request[j] + "\n")
                        break;
                    case 5:
                        dotDetails += ("Timestamp: " + request[j] + "\n")
                        break;
                    case 6:
                        dotDetails += ("Content-Type: " + request[j] + "\n")
                        break;
                    case 7:
                        dotDetails += ("Incoming Number: " + request[j] + "\n")
                        break;
                    case 8:
                        dotDetails += ("Primary Key: " + request[j] + "\n")
                        break;
                    case 9:
                        dotDetails += ("HTTP-Body: " + request[j] + "\n \n")
                        break;
                }
            }
            j = 0
            i++
        }
        self.props.getDotDetails(dotDetails)
        self.props.openDotDetailsDialog()
    }


    handleCommentClick(id, type){
        this.setState({
            typeComment: type
        })

        if(type === "table"){
            this.props.getComment(this.props.allTables.filter(table => table.id === id)[0].comment)
        } else if(type === "chart"){
            this.props.getComment(this.props.allCharts.filter(chart => chart.id === id)[0].comment)

        }

        if(type === "table"){
            this.setState({
                id: id,
                comment: this.props.allTables.filter(table => table.id === id)[0].comment
            })}
        else{
            this.setState({
                id: id,
                comment: this.props.allCharts.filter(table => table.id === id)[0].comment
            })
        }

        this.props.openTableComment()
    }

    clearComment(){
        this.setState({
            comment: ""
        })
    }

    handleCommentEdit() {
        try {
            this.props.editTableComment(this.state.id, this.state.comment,"tableDataTimeouts")
        } catch (err) {
            console.log("err")
        }
        this.props.closeTableComment()
    }

    handleChartCommentEdit() {
        try {
            this.props.editChartComment(this.state.id, this.state.comment, "chartDataTimeouts")
        } catch (err) {
            console.log("err")
        }
        this.props.closeTableComment()
    }

    handleChange = event => {
        if(event.target.name === "comment"){
            this.setState({
                comment: event.target.value,
            });
        }else{
            this.setState({
                title: event.target.value,
            });
        }
    };

    editClicked(id, test){
        if(test === "table"){
            this.setState({
                id: id,
                table: true,
                title: this.props.allTables[id - 1].title,
            })
        }else {
            this.setState({
                id: id,
                table: false,
                title: this.props.allCharts[id - 1].title,
                color: this.props.allCharts[id - 1].color,
                type: this.props.allCharts[id - 1].selection
            })
        }
        this.props.openChartEdit()
    }

    handleEditChange = () => ({target: {value}}) => {
        if (value === "Green" || value === "Blue" || value === "Red") {
            this.setState({
                color: value
            })
        } else {
            this.setState({
                type: value
            })
        }
    }

    handleEdit(){
        try{
            this.props.editChart(this.state.id, this.state.type, this.state.title, this.state.color, this.state.table, "Timeouts")
        }catch(err){
            console.log("err")
        }
        this.props.closeChartEdit()
    }

    render() {
        const { classes } = this.props;

        var tableCounter = 0

        var Table = this.props.allTables.map(table =>{
        tableCounter++
            return(
                <div>
                    <Paper style={styles.root && styles.paper}>
                        <br/>
                        <table width="99%">
                            <tbody>
                            <tr>
                                <td width="3%">
                                    {(this.props.deleteChartsPossible && this.props.tooltipsEnabled)  ?
                                        <Tooltip title="Delete">
                                            <IconButton
                                                color="secondary"
                                                onClick= {() => deleteTable(table, this)}
                                                style={{marginLeft: 22.5}}
                                            >
                                                <Delete/>
                                            </IconButton>
                                        </Tooltip>
                                        : (this.props.deleteChartsPossible === true && this.props.tooltipsEnabled === false) &&
                                        <IconButton
                                            color="secondary"
                                            onClick= {() => deleteTable(table, this)}
                                            style={{marginLeft: 22.5}}
                                        >
                                            <Delete/>
                                        </IconButton>}
                                </td>
                                <td width="3%">
                                    {this.props.tooltipsEnabled ?
                                        <Tooltip title="Edit">
                                            <IconButton
                                                color="secondary"
                                                onClick={() => this.editClicked(table.id, "table")}
                                            >
                                                <Edit/>
                                            </IconButton>
                                        </Tooltip>
                                        : <IconButton
                                            color="secondary"
                                            onClick={() => this.editClicked(table.id, "table")}
                                        >
                                            <Edit/>
                                        </IconButton>
                                    }
                                </td>
                                <td>
                                    {this.props.tooltipsEnabled ?
                                        <Tooltip title="Comment">
                                            <IconButton
                                                color="secondary"
                                                onClick={() => this.handleCommentClick(table.id, "table")}
                                            >
                                                <Message/>
                                            </IconButton>
                                        </Tooltip>
                                        : <IconButton
                                            color="secondary"
                                            onClick={() => this.handleCommentClick(table.id, "table")}
                                        >
                                            <Message/>
                                        </IconButton>
                                    }
                                </td>
                                <td>
                                    <Typography align="center" variant="title" component="h3">
                                        {table.title}
                                    </Typography>
                                </td>
                                <td>
                                    <Typography align="right"  component="h3">
                                        Stand: {table.date}
                                    </Typography>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br/>
                        {simpleTable(table, this.props.data, "Timeouts", this.props.fontSize)}
                    </Paper>
                    <br/>
                </div>
            )})

        var tablesExist = tableCounter !== 0

        var datas = this.props.data;

        function deleteTable(table, abc){
            abc.props.deleteTableTimeouts(table.id);
        }

        var chartLibrary = this.props.library

        var counter = 0

        var Chart = this.props.allCharts.map(chart => {
           counter++
            var self = this
            return(
                chart.timeline === true?
                    <div>
                        <Paper className={classes.paper}>
                            <br/>
                            <table width="100%">
                                <tbody>
                                <tr>
                                    <td width="3%">
                                        {(this.props.deleteChartsPossible && this.props.tooltipsEnabled)  ?
                                            <Tooltip title="Delete">
                                                <IconButton
                                                    color="secondary"
                                                    onClick={() => {this.props.deleteChartTimeouts(chart.id)}}
                                                >
                                                    <Delete/>
                                                </IconButton>
                                            </Tooltip>
                                            : (this.props.deleteChartsPossible === true && this.props.tooltipsEnabled === false) &&
                                            <IconButton
                                                color="secondary"
                                                onClick={() => {this.props.deleteChartTimeouts(chart.id)}}
                                            >
                                                <Delete/>
                                            </IconButton>}
                                    </td>
                                    <td width="3%">
                                        {this.props.tooltipsEnabled ?
                                            <Tooltip title="Edit">
                                                <IconButton
                                                    color="secondary"
                                                    onClick={() => this.editClicked(chart.id)}
                                                >
                                                    <Edit/>
                                                </IconButton>
                                            </Tooltip>
                                            : <IconButton
                                                color="secondary"
                                                onClick={() => this.editClicked(chart.id)}
                                            >
                                                <Edit/>
                                            </IconButton>
                                        }
                                    </td>
                                    <td>
                                        {this.props.tooltipsEnabled ?
                                            <Tooltip title="Comment">
                                                <IconButton
                                                    color="secondary"
                                                    onClick={() => this.handleCommentClick(chart.id, "chart")}
                                                >
                                                    <Message/>
                                                </IconButton>
                                            </Tooltip>
                                            : <IconButton
                                                color="secondary"
                                                onClick={() => this.handleCommentClick(chart.id, "chart")}
                                            >
                                                <Message/>
                                            </IconButton>
                                        }
                                    </td>
                                    <td>
                                        <Typography align="center" variant="title" component="h3">
                                            {chart.title}
                                        </Typography>
                                    </td>
                                    <td>
                                        <Typography align="right"  component="h3">
                                            Stand: {chart.date}
                                        </Typography>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            {(function(){

                                const rechartsData=[]

                                var startYear = parseInt(chart.chartTimeStart.year)
                                var startMonth = parseInt(chart.chartTimeStart.month)
                                var endYear = parseInt(chart.chartTimeEnd.year)
                                var endMonth = parseInt(chart.chartTimeEnd.month)

                                var startYearData = parseInt(chart.chartTimeStart.year) - 2012
                                var startMonthData = parseInt(chart.chartTimeStart.month) - 1

                                if(startYear === endYear){
                                    while( startMonth !== endMonth + 1){

                                        var row = {
                                            name:"",
                                            Timeouts: 0,

                                        }

                                        row.name = ("0" + startMonth + "/" + startYear);
                                        row.Timeouts = (parseInt(datas.Timeouts[0].Jahr[startYearData][startMonthData]))


                                        startMonthData++;
                                        startMonth++;
                                        rechartsData.push(row)
                                    }
                                }
                                else{
                                    while(startYear !== endYear || startMonth !== endMonth + 1){

                                        var row = {
                                            name:"",
                                            Timeouts: 0,

                                        }

                                        row.name = ("0" + startMonth + "/" + startYear);
                                        row.Timeouts = (parseInt(datas.Timeouts[0].Jahr[startYearData][startMonthData]))


                                        startMonthData++;
                                        if(startMonthData > 11){
                                            startMonthData = 0;
                                            startYearData++
                                        }

                                        startMonth++;
                                        if(startMonth > 12){
                                            startMonth = 1;
                                            startYear++;
                                        }
                                        rechartsData.push(row)
                                    }
                                }



                                var i = 0;
                                var startYearJs = parseInt(chart.chartTimeStart.year)
                                var startMonthJs = parseInt(chart.chartTimeStart.month)
                                var endYearJs = parseInt(chart.chartTimeEnd.year)
                                var endMonthJs = parseInt(chart.chartTimeEnd.month)
                                var labelsArray= new Array();

                                if(startYearJs === endYearJs){
                                    while(startMonthJs !== endMonthJs + 1){
                                        labelsArray[i]=("0" + startMonthJs + "/" + startYearJs);
                                        startMonthJs++;
                                        i++;
                                    }
                                }
                                else{
                                    while(startYearJs !== endYearJs || startMonthJs !== endMonthJs + 1){
                                        labelsArray[i]=("0" + startMonthJs + "/" + startYearJs);
                                        startMonthJs++;
                                        i++;
                                        if(startMonthJs > 12){
                                            startMonthJs = 1;
                                            startYearJs++;
                                        }
                                    }
                                }


                                var dataArray= new Array();
                                var pieColorCounter = 0;
                                startYearJs = parseInt(chart.chartTimeStart.year) - 2012
                                startMonthJs = parseInt(chart.chartTimeStart.month) - 1
                                endYearJs = parseInt(chart.chartTimeEnd.year) - 2012
                                endMonthJs = parseInt(chart.chartTimeEnd.month) - 1

                                if(startYearJs === endYearJs){
                                    while(startMonthJs !== endMonthJs + 1){
                                        dataArray.push(parseInt(datas.Timeouts[0].Jahr[startYearJs][startMonthJs]))
                                        startMonthJs++;
                                        pieColorCounter++;
                                    }
                                }
                                else{
                                    while(startYearJs !== endYearJs || startMonthJs !== endMonthJs + 1){
                                        dataArray.push(parseInt(datas.Timeouts[0].Jahr[startYearJs][startMonthJs]))
                                        startMonthJs++;
                                        pieColorCounter++;
                                        if(startMonthJs > 11){
                                            startMonthJs = 0;
                                            startYearJs++
                                        }
                                    }
                                }

                                var pieChartColors = new Array();

                                for(var k = 0; k < pieColorCounter; k++){
                                    pieChartColors.push("#" + Math.floor(16777215 * Math.pow(0.65, k + 1)).toString(16));
                                }
                                switch(chart.selection){
                                    case "Bar":
                                        if(chartLibrary === "Recharts") {
                                            return <BarRechart chartData={rechartsData} color={chart.color} indicator="Timeouts" self={self} openDotDetailsDialog={self.handleDotClick}/>;
                                        } else if(chartLibrary === "ChartJS"){
                                            return <BarChart chartData = {{
                                                labels: labelsArray,
                                                datasets: [{
                                                    data: dataArray,
                                                    label: "Timeouts",
                                                    backgroundColor: chart.color
                                                }]
                                            }}/>
                                        }
                                        break;
                                    case "Pie":
                                        return <PieChart chartData = {{
                                            labels: labelsArray,
                                            datasets: [{
                                                data: dataArray,
                                                label: "Timeouts",
                                                backgroundColor: pieChartColors
                                            }]
                                        }} title = {chart.title}/>;
                                    case "Line":
                                        if(chartLibrary === "Recharts") {
                                            return <LineRechart chartData={rechartsData} color={chart.color} indicator="Timeouts" self={self} openDotDetailsDialog={self.handleDotClick}/>;
                                        } else if(chartLibrary === "ChartJS"){
                                            return <LineChart chartData = {{
                                                labels: labelsArray,
                                                datasets: [{
                                                    data: dataArray,
                                                    label: "Timeouts",
                                                    backgroundColor: chart.color,
                                                    borderColor: chart.color,
                                                    fill: false
                                                }]
                                            }}/>
                                        }
                                        break;
                                    default:
                                        return null;
                                }
                            })()}
                        </Paper><br/></div>
                    :<div>
                        <Paper className={classes.paper}>
                            <br/>
                            <table width="90%">
                                <tbody>
                                <tr>
                                    <td width="3%">
                                        {(this.props.deleteChartsPossible && this.props.tooltipsEnabled)  ?
                                            <Tooltip title="Delete">
                                                <IconButton
                                                    color="secondary"
                                                    onClick={() => {this.props.deleteChartTimeouts(chart.id)}}
                                                >
                                                    <Delete/>
                                                </IconButton>
                                            </Tooltip>
                                            : (this.props.deleteChartsPossible === true && this.props.tooltipsEnabled === false) &&
                                            <IconButton
                                                color="secondary"
                                                onClick={() => {this.props.deleteChartTimeouts(chart.id)}}
                                            >
                                                <Delete/>
                                            </IconButton>}
                                    </td>
                                    <td width="3%">
                                        {this.props.tooltipsEnabled ?
                                            <Tooltip title="Edit">
                                                <IconButton
                                                    color="secondary"
                                                    onClick={() => this.editClicked(chart.id)}
                                                >
                                                    <Edit/>
                                                </IconButton>
                                            </Tooltip>
                                            : <IconButton
                                                color="secondary"
                                                onClick={() => this.editClicked(chart.id)}
                                            >
                                                <Edit/>
                                            </IconButton>
                                        }
                                    </td>
                                    <td>
                                        {this.props.tooltipsEnabled ?
                                            <Tooltip title="Comment">
                                                <IconButton
                                                    color="secondary"
                                                    onClick={() => this.handleCommentClick(chart.id, "chart")}
                                                >
                                                    <Message/>
                                                </IconButton>
                                            </Tooltip>
                                            : <IconButton
                                                color="secondary"
                                                onClick={() => this.handleCommentClick(chart.id, "chart")}
                                            >
                                                <Message/>
                                            </IconButton>
                                        }
                                    </td>
                                    <td>
                                        <Typography align="center" variant="title" component="h3">
                                            {chart.title}
                                        </Typography>
                                    </td>
                                    <td>
                                        <Typography align="right"  component="h3">
                                            {chart.date}
                                        </Typography>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            {(function(){
                                const rechartsData=[]

                                var counter = chart.chartDates.counter

                                for(var i = 0; i < counter; i++){

                                    var row

                                    row = {
                                        name:"",
                                        Timeouts: 0,
                                    }


                                    row.name = (chart.chartDates.months[i] + "/" + chart.chartDates.years[i]);
                                    row.Timeouts = parseInt((datas.Timeouts[0].Jahr[parseInt(chart.chartDates.years[i])-2012][parseInt(chart.chartDates.months[i]) - 1]))

                                    rechartsData.push(row)
                                }

                                var counterTwo = chart.chartDates.counter
                                var pieColorCounter = 0;

                                var labelsArray=[];
                                var dataArray=[];

                                for(var j = 0; j < counterTwo; j++) {
                                    labelsArray.push(chart.chartDates.months[j] + "/" + chart.chartDates.years[j])
                                    dataArray.push(parseInt((datas.Timeouts[0].Jahr[parseInt(chart.chartDates.years[j])-2012][parseInt(chart.chartDates.months[j]) - 1])))
                                    pieColorCounter++;
                                }

                                var pieChartColors = [];

                                for(var k = 0; k < pieColorCounter; k++){
                                    pieChartColors.push("#" + Math.floor(16777215 * Math.pow(0.65, k + 1)).toString(16));
                                }
                                switch(chart.selection){
                                    case "Bar":
                                        if(chartLibrary === "Recharts") {
                                            return <BarRechart chartData={rechartsData} color={chart.color} indicator="Timeouts" self={self} openDotDetailsDialog={self.handleDotClick}/>;
                                        } else if(chartLibrary === "ChartJS"){
                                            return <BarChart chartData = {{
                                                labels: labelsArray,
                                                datasets: [{
                                                    data: dataArray,
                                                    label: "Timeouts",
                                                    backgroundColor: chart.color
                                                }]
                                            }}/>
                                        }
                                        break;
                                    case "Pie":
                                        return <PieChart chartData = {{
                                            labels: labelsArray,
                                            datasets: [{
                                                data: dataArray,
                                                label: "Timeouts",
                                                backgroundColor: pieChartColors
                                            }]
                                        }} title = {chart.title}/>;
                                    case "Line":
                                        if(chartLibrary === "Recharts") {
                                            return <LineRechart chartData={rechartsData} color={chart.color} indicator="Timeouts" self={self} openDotDetailsDialog={self.handleDotClick}/>;
                                        } else if(chartLibrary === "ChartJS"){
                                            return <LineChart chartData = {{
                                                labels: labelsArray,
                                                datasets: [{
                                                    data: dataArray,
                                                    label: "Timeouts",
                                                    backgroundColor: chart.color,
                                                    borderColor: chart.color,
                                                    fill: false
                                                }]
                                            }}/>
                                        }
                                        break;
                                    default:
                                        return null;
                                }
                            })()}
                        </Paper><br/>
                    </div>
            )
        })

        var chartsExist = counter !== 0

        var nameEnglish = "Timeouts"

        var nameGerman = "Zeitüberschreitungen"


        return (
            <React.Fragment>
                <CssBaseline />
                <div className={classes.root}>
                    {this.props.language === "English" ?
                        <DashboardEnglish isDrawerOpen={this.props.isDrawerOpen} handleDrawerOpen={this.props.handleDrawerOpen} loggedIn={this.props.loggedIn}
                                          logOut={this.props.logOut} loginFormOpen={this.props.loginFormOpen} closeLoginForm={this.props.closeLoginForm}
                                          openLoginForm={this.props.openLoginForm} noAccess={this.props.noAccess} closeNoAccess={this.props.closeNoAccess}
                                          openChartForm={this.props.openChartFormTimeouts} showChartForm={this.props.showChartForm} closeChartForm={this.props.closeChartFormTimeouts}
                                          handleDrawerClose={this.props.handleDrawerClose} loggingIn={this.props.loggingIn} Chart={Chart} Table={Table} userName={this.props.userName}
                                          chartsExist={chartsExist} tablesExist={tablesExist} ChartForm={ChartForm} nameEnglish={nameEnglish}
                                          handleDrawerChange={this.props.handleDrawerChange} tableCommentOpen={this.props.tableCommentOpen}
                                          closeTableComment={this.props.closeTableComment} chartEditOpen={this.props.chartEditOpen} closeChartEdit={this.props.closeChartEdit}
                                          handleEditChange={this.handleEditChange} type={this.state.type} editChart={() => this.handleEdit()} comment={this.state.comment}
                                          clearComment={() => this.clearComment()} handleChange = {this.handleChange} handleCommentEdit={() => this.handleCommentEdit()}
                                          handleChartCommentEdit={() => this.handleChartCommentEdit()} typeComment={this.state.typeComment}
                                          noAdminAccessOpen={this.props.noAdminAccessOpen} closeNoAdminAccess={this.props.closeNoAdminAccess}
                                          dotDetails={this.props.dotDetails} dotDetailsDialogOpen={this.props.dotDetailsDialogOpen} closeDotDetailsDialog={this.props.closeDotDetailsDialog}
                                          title={this.state.title} color={this.state.color} table={this.state.table}/>
                        : this.props.language === "German" ?
                            <DashboardGerman isDrawerOpen={this.props.isDrawerOpen} handleDrawerOpen={this.props.handleDrawerOpen} loggedIn={this.props.loggedIn}
                                             logOut={this.props.logOut} loginFormOpen={this.props.loginFormOpen} closeLoginForm={this.props.closeLoginForm} userName={this.props.userName}
                                             openLoginForm={this.props.openLoginForm} noAccess={this.props.noAccess} closeNoAccess={this.props.closeNoAccess}
                                             openChartForm={this.props.openChartFormTimeouts} showChartForm={this.props.showChartForm} closeChartForm={this.props.closeChartFormTimeouts}
                                             handleDrawerClose={this.props.handleDrawerClose} loggingIn={this.props.loggingIn} Chart={Chart} Table={Table} chartsExist={chartsExist}
                                             tablesExist={tablesExist} ChartForm={ChartForm} nameGerman={nameGerman} handleDrawerChange={this.props.handleDrawerChange}
                                             tableCommentOpen={this.props.tableCommentOpen} closeTableComment={this.props.closeTableComment} chartEditOpen={this.props.chartEditOpen} closeChartEdit={this.props.closeChartEdit}
                                             handleEditChange={this.handleEditChange} type={this.state.type} editChart={() => this.handleEdit()} comment={this.state.comment}
                                             clearComment={() => this.clearComment()} handleChange = {this.handleChange} handleCommentEdit={() => this.handleCommentEdit()}
                                             handleChartCommentEdit={() => this.handleChartCommentEdit()} typeComment={this.state.typeComment}
                                             noAdminAccessOpen={this.props.noAdminAccessOpen} closeNoAdminAccess={this.props.closeNoAdminAccess}
                                             dotDetails={this.props.dotDetails} dotDetailsDialogOpen={this.props.dotDetailsDialogOpen} closeDotDetailsDialog={this.props.closeDotDetailsDialog}
                                             title={this.state.title} color={this.state.color} table={this.state.table}/>
                            : null}
                </div>
            </React.Fragment>
        )
    }
}

DashboardTimeouts.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    isDrawerOpen: state.allReducers.drawerOpen,
    showChartForm: state.allReducers.chartFormTimeoutsOpen,
    allCharts: state.allReducers.itemsTimeouts,
    data: state.allReducers.data[0],
    allTables: state.allReducers.tablesTimeouts,
    requests: state.allReducers.requestData,
    deleteChartsPossible: state.allReducers.deleteChartsPossible,
    tooltipsEnabled: state.allReducers.tooltipsEnabled,
    language: state.allReducers.language,
    library: state.allReducers.library,
    loggedIn: state.allReducers.isAccessGranted,
    userName: state.allReducers.userName,
    fontSize: state.allReducers.fontsize,
    tableCommentOpen: state.allReducers.tableCommentOpen,
    comment: state.allReducers.comment,
    chartEditOpen: state.allReducers.chartEditOpen,
    noAdminAccessOpen: state.allReducers.noAdminAccessOpen,
    dotDetailsDialogOpen: state.allReducers.dotDetailDialogOpen,
    dotDetails: state.allReducers.dotDetails




});


export default compose(
    withStyles(styles, {
        name: 'DashboardTimeouts',
    }),
    connect(mapStateToProps, {fetchData, fetchDataTimeouts, logOut, handleDrawerChange, deleteTableTimeouts, fetchTableDataTimeouts, deleteChartTimeouts,
        handleDrawerOpen, handleDrawerClose, openChartFormTimeouts, closeChartFormTimeouts, closeNoAdminAccess,
        openTableComment, editTableComment,closeTableComment, getComment,editChartComment, openChartEdit, closeChartEdit, editChart,
        openDotDetailsDialog, closeDotDetailsDialog, getDotDetails, fetchDataAllRequests})
)(DashboardTimeouts);
