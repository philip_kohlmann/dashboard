import {
    HANDLE_DRAWER_OPEN,
    HANDLE_DRAWER_CLOSE,
    HANDLE_DRAWER_CHANGE,
    LOGIN_PRESSED,
    ACCESS_DENIED,
    ACCESS_GRANTED,
    OPEN_LOGIN_FORM,
    CLOSE_LOGIN_FORM,
    LOG_OUT,
    OPEN_CHART_FORM,
    CLOSE_CHART_FORM,
    OPEN_CHART_FORM_DOWNTIME,
    CLOSE_CHART_FORM_DOWNTIME,
    OPEN_CHART_FORM_REQUESTS,
    CLOSE_CHART_FORM_REQUESTS,
    OPEN_CHART_FORM_TIMEOUTS,
    CLOSE_CHART_FORM_TIMEOUTS,
    FETCH_DATA,
    FETCH_HOME_DATA,
    FETCH_DOWNTIME_DATA,
    FETCH_REQUESTS_DATA,
    FETCH_TIMEOUTS_DATA,
    CREATE_HOME_CHART,
    CREATE_DOWNTIME_CHART,
    CREATE_TIMEOUTS_CHART,
    CREATE_REQUESTS_CHART,
    CREATE_DOWNTIME_TABLE,
    CREATE_REQUESTS_TABLE,
    CREATE_TIMEOUTS_TABLE,
    FETCH_TABLE_DOWNTIME_DATA,
    FETCH_TABLE_REQUESTS_DATA,
    FETCH_TABLE_TIMEOUTS_DATA,
    FETCH_TABLE_DATA_HOME,
    CREATE_HOME_TABLE,
    HANDLE_TAB_CHANGE,
    DELETE_CHART_DOWNTIME,
    DELETE_CHART_HOME,
    DELETE_CHART_REQUESTS,
    DELETE_CHART_TIMEOUTS,
    DELETE_TABLE_DOWNTIME,
    DELETE_TABLE_HOME,
    DELETE_TABLE_REQUESTS,
    DELETE_TABLE_TIMEOUTS,
    CREATE_HOME_CHART_TWO,
    CREATE_HOME_CHART_THREE,
    CREATE_HOME_CHART_FOUR,
    CREATE_HOME_TABLE_TWO,
    CREATE_HOME_TABLE_THREE,
    CREATE_HOME_TABLE_FOUR,
    FETCH_HOME_DATA_TWO,
    FETCH_HOME_DATA_THREE,
    FETCH_HOME_DATA_FOUR,
    FETCH_TABLE_DATA_HOME_TWO,
    FETCH_TABLE_DATA_HOME_THREE,
    FETCH_TABLE_DATA_HOME_FOUR,
    DELETE_CHART_HOME_TWO,
    DELETE_CHART_HOME_THREE,
    DELETE_CHART_HOME_FOUR,
    DELETE_TABLE_HOME_TWO,
    DELETE_TABLE_HOME_THREE,
    DELETE_TABLE_HOME_FOUR,
    OPEN_CHART_FORM_TWO,
    OPEN_CHART_FORM_THREE,
    OPEN_CHART_FORM_FOUR,
    CLOSE_CHART_FORM_TWO,
    CLOSE_CHART_FORM_THREE,
    CLOSE_CHART_FORM_FOUR,
    SINGLE_DATES_SELECTED,
    SINGLE_DATES_SELECTED_TWO,
    SINGLE_DATES_SELECTED_THREE,
    SINGLE_DATES_SELECTED_FOUR,
    SINGLE_DATES_SELECTED_DOWNTIME,
    SINGLE_DATES_SELECTED_REQUESTS,
    SINGLE_DATES_SELECTED_TIMEOUTS,
    TIMELINE_SELECTED,
    TIMELINE_SELECTED_TWO,
    TIMELINE_SELECTED_THREE,
    TIMELINE_SELECTED_FOUR,
    TIMELINE_SELECTED_DOWNTIME,
    TIMELINE_SELECTED_REQUESTS,
    TIMELINE_SELECTED_TIMEOUTS,
    ADD_DATE,
    ADD_DATE_TWO,
    ADD_DATE_THREE,
    ADD_DATE_FOUR,
    ADD_DATE_DOWNTIME,
    ADD_DATE_REQUESTS,
    ADD_DATE_TIMEOUTS,
    DELETE_DATE,
    DELETE_DATE_TWO,
    DELETE_DATE_THREE,
    DELETE_DATE_FOUR,
    DELETE_DATE_DOWNTIME,
    DELETE_DATE_REQUESTS,
    DELETE_DATE_TIMEOUTS,
    HANDLE_CLOSE_FONTSIZE,
    HANDLE_CLOSE_FONTSTYLE,
    HANDLE_CLOSE_PRIMARY_COLOR,
    HANDLE_CLOSE_SECONDARY_COLOR,
    HANDLE_OPEN_FONTSIZE,
    HANDLE_OPEN_FONTSTYLE,
    HANDLE_OPEN_PRIMARY_COLOR,
    HANDLE_OPEN_SECONDARY_COLOR,
    HANDLE_TOGGLE_CHARTS,
    HANDLE_TOGGLE_LANGUAGE,
    HANDLE_TOGGLE_TOOLTIPS,
    HANDLE_TOGGLE_DELETECHARTSPOSSIBLE,
    HANDLE_TOGGLE_STAYLOGGEDIN,
    CHANGE_FONTSIZE,
    CHANGE_FONTSTYLE,
    CHANGE_COLOR_PRIMARY,
    CHANGE_COLOR_SECONDARY,
    RESET_SETTINGS,
    CLOSE_NEW_USER_FORM,
    OPEN_NEW_USER_FORM,
    ADD_USER,
    CHANGE_LANGUAGE,
    CHANGE_CHART_LIBRARY,
    HANDLE_FILTER,
    OPEN_HTTP_DIALOG,
    CLOSE_HTTP_DIALOG,
    OPEN_TABLE_COMMENT,
    CLOSE_TABLE_COMMENT,
    EDIT_TABLE_COMMENT,
    GET_COMMENT,
    OPEN_CHART_EDIT,
    CLOSE_CHART_EDIT,
    EDIT_CHART_COMMENT,
    OPEN_NO_ADMIN_ACCESS,
    CLOSE_NO_ADMIN_ACCESS,
    OPEN_DOT_DETAILS_DIALOG,
    CLOSE_DOT_DETAILS_DIALOG,
    GET_DOT_DETAILS,
    FETCH_REQUEST_DATA,

} from "./types";

var timeout

export const handleDrawerOpen = () => {
    return{
        type: HANDLE_DRAWER_OPEN
    }
}

export const handleDrawerClose = () => {
    return{
        type: HANDLE_DRAWER_CLOSE
    }
}

export const loginPressed =(user) => dispatch => {
    fetch('https://my-cool-api.herokuapp.com/api/login', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(user)
    })
        .then(res => res.json())
        .then(token => dispatch({
            type: LOGIN_PRESSED,
            payload: token,
            userName: user
        }))
};

export const accessGranted = () => {
    return(dispatch) => {
        dispatch({
            type: ACCESS_GRANTED
        });
        timeout = setTimeout(() => {dispatch(logOut())}, 600000)
    }
};

export const accessDenied = () => {
    return{
        type: ACCESS_DENIED
    }
}

export const openLoginForm = () => {
    return{
        type: OPEN_LOGIN_FORM
    }
}

export const closeLoginForm = () => {
    return{
        type: CLOSE_LOGIN_FORM
    }
}

export const openChartForm = (type) => {
    return{
        type: OPEN_CHART_FORM,
        payload: type
    }
}

export const closeChartForm = () => {
    return{
        type: CLOSE_CHART_FORM
    }
}

export const openChartFormRequests = (type) => {
    return{
        type: OPEN_CHART_FORM_REQUESTS,
        payload: type

    }
}

export const closeChartFormRequests = () => {
    return{
        type: CLOSE_CHART_FORM_REQUESTS
    }
}

export const openChartFormDowntime = (type) => {
    return{
        type: OPEN_CHART_FORM_DOWNTIME,
        payload: type

    }
}

export const closeChartFormDowntime = () => {
    return{
        type: CLOSE_CHART_FORM_DOWNTIME
    }
}

export const openChartFormTimeouts = (type) => {
    return{
        type: OPEN_CHART_FORM_TIMEOUTS,
        payload: type
    }
}

export const closeChartFormTimeouts = () => {
    return{
        type: CLOSE_CHART_FORM_TIMEOUTS
    }
}

export const logOut = () => {
    return{
        type: LOG_OUT
    }
}

export const createChartHome =(chart) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(chart)
    })
        .then(res => res.json())
        .then(chart => dispatch({
            type: CREATE_HOME_CHART,
            payload: chart,
        }))
};

export const createChartDowntime =(chart) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataDowntime', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(chart)
    })
        .then(res => res.json())
        .then(chart => dispatch({
            type: CREATE_DOWNTIME_CHART,
            payload: chart,
        }))
};

export const createTableRequests =(table) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataRequests', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(table)
    })
        .then(res => res.json())
        .then(table => dispatch({
            type: CREATE_REQUESTS_TABLE,
            payload: table,
        }))
};

export const createTableTimeouts =(table) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataTimeouts', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(table)
    })
        .then(res => res.json())
        .then(table => dispatch({
            type: CREATE_TIMEOUTS_TABLE,
            payload: table,
        }))
};

export const createTableHome =(table) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(table)
    })
        .then(res => res.json())
        .then(table => dispatch({
            type: CREATE_HOME_TABLE,
            payload: table,
        }))
};

export const createTableDowntime =(table) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataDowntime', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(table)
    })
        .then(res => res.json())
        .then(table => dispatch({
            type: CREATE_DOWNTIME_TABLE,
            payload: table,
        }))
};

export const createChartRequests =(chart) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataRequests', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(chart)
    })
        .then(res => res.json())
        .then(chart => dispatch({
            type: CREATE_REQUESTS_CHART,
            payload: chart,
        }))
};

export const createChartTimeouts =(chart) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataTimeouts', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(chart)
    })
        .then(res => res.json())
        .then(chart => dispatch({
            type: CREATE_TIMEOUTS_CHART,
            payload: chart,
        }))
};

export const fetchDataHome = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_HOME_DATA,
            payload: data
        }))
};

export const fetchDataTimeouts = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDatatimeouts')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_TIMEOUTS_DATA,
            payload: data
        }))
};

export const fetchDataRequests = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataRequests')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_REQUESTS_DATA,
            payload: data
        }))
};

export const fetchDataDowntime = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataDowntime')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_DOWNTIME_DATA,
            payload: data
        }))
};

export const fetchTableDataDowntime = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataDowntime')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_TABLE_DOWNTIME_DATA,
            payload: data
        }))
};

export const fetchTableDataRequests = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataRequests')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_TABLE_REQUESTS_DATA,
            payload: data
        }))
};

export const fetchTableDataHome = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_TABLE_DATA_HOME,
            payload: data
        }))
};

export const fetchTableDataTimeouts = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataTimeouts')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_TABLE_TIMEOUTS_DATA,
            payload: data
        }))
};

export const fetchData = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/data')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_DATA,
            payload: data

        }));
}

export const handleTabChange = (value) =>  {
    return{
        type: HANDLE_TAB_CHANGE,
        payload: value
    }
}

export const deleteChartHome = (id) => dispatch => {
        fetch('https://my-json-db.herokuapp.com/chartDataHome' + "/" + id, {
            method: 'DELETE',
            body: id
        })
            .then(id => dispatch({
                type: DELETE_CHART_HOME,
                payload: id,
            }))
}

export const deleteChartTimeouts = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataTimeouts' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_CHART_TIMEOUTS,
            payload: id,
        }))
}

export const deleteChartRequests = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataRequests' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_CHART_REQUESTS,
            payload: id,
        }))
}

export const deleteChartDowntime = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataDowntime' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_CHART_DOWNTIME,
            payload: id,
        }))
}




export const deleteTableHome = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_TABLE_HOME,
            payload: id,
        }))
}

export const deleteTableTimeouts = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataTimeouts' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_TABLE_TIMEOUTS,
            payload: id,
        }))
}

export const deleteTableRequests = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataRequests' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_TABLE_REQUESTS,
            payload: id,
        }))
}

export const deleteTableDowntime = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataDowntime' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_TABLE_DOWNTIME,
            payload: id,
        }))
}

export const createChartHomeTwo =(chart) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome2', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(chart)
    })
        .then(res => res.json())
        .then(chart => dispatch({
            type: CREATE_HOME_CHART_TWO,
            payload: chart,
        }))
};

export const createChartHomeThree =(chart) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome3', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(chart)
    })
        .then(res => res.json())
        .then(chart => dispatch({
            type: CREATE_HOME_CHART_THREE,
            payload: chart,
        }))
};

export const createChartHomeFour =(chart) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome4', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(chart)
    })
        .then(res => res.json())
        .then(chart => dispatch({
            type: CREATE_HOME_CHART_FOUR,
            payload: chart,
        }))
};

export const createTableHomeTwo =(table) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome2', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(table)
    })
        .then(res => res.json())
        .then(table => dispatch({
            type: CREATE_HOME_TABLE_TWO,
            payload: table,
        }))
};

export const createTableHomeThree =(table) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome3', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(table)
    })
        .then(res => res.json())
        .then(table => dispatch({
            type: CREATE_HOME_TABLE_THREE,
            payload: table,
        }))
};

export const createTableHomeFour =(table) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome4', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(table)
    })
        .then(res => res.json())
        .then(table => dispatch({
            type: CREATE_HOME_TABLE_FOUR,
            payload: table,
        }))
};

export const deleteChartHomeTwo = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome2' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_CHART_HOME_TWO,
            payload: id,
        }))
}

export const deleteChartHomeThree = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome3' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_CHART_HOME_THREE,
            payload: id,
        }))
}

export const deleteChartHomeFour = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome4' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_CHART_HOME_FOUR,
            payload: id,
        }))
}

export const deleteTableHomeTwo = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome2' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_TABLE_HOME_TWO,
            payload: id,
        }))
}

export const deleteTableHomeThree = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome3' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_TABLE_HOME_THREE,
            payload: id,
        }))
}

export const deleteTableHomeFour = (id) => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome4' + "/" + id, {
        method: 'DELETE',
        body: id
    })
        .then(id => dispatch({
            type: DELETE_TABLE_HOME_FOUR,
            payload: id,
        }))
}

export const fetchDataHomeTwo = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome2')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_HOME_DATA_TWO,
            payload: data
        }))
};

export const fetchDataHomeThree = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome3')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_HOME_DATA_THREE,
            payload: data
        }))
};

export const fetchDataHomeFour = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/chartDataHome4')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_HOME_DATA_FOUR,
            payload: data
        }))
};

export const fetchTableDataHomeTwo = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome2')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_TABLE_DATA_HOME_TWO,
            payload: data
        }))
};

export const fetchTableDataHomeThree = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome3')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_TABLE_DATA_HOME_THREE,
            payload: data
        }))
};

export const fetchTableDataHomeFour = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/tableDataHome4')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_TABLE_DATA_HOME_FOUR,
            payload: data
        }))
};


export const openChartFormTwo = (type) => {
    return{
        type: OPEN_CHART_FORM_TWO,
        payload: type

    }
}

export const closeChartFormTwo = () => {
    return{
        type: CLOSE_CHART_FORM_TWO
    }
}


export const openChartFormThree = (type) => {
    return{
        type: OPEN_CHART_FORM_THREE,
        payload: type

    }
}

export const closeChartFormThree = () => {
    return{
        type: CLOSE_CHART_FORM_THREE
    }
}

export const openChartFormFour = (type) => {
    return{
        type: OPEN_CHART_FORM_FOUR,
        payload: type
    }
}

export const closeChartFormFour = () => {
    return{
        type: CLOSE_CHART_FORM_FOUR
    }
}

export const timelineSelected = () => {
    return{
        type: TIMELINE_SELECTED
    }
}

export const singleDatesSelected = () => {
    return{
        type: SINGLE_DATES_SELECTED
    }
}

export const addDate = () => {
    return{
        type: ADD_DATE
    }
}

export const timelineSelectedTwo = () => {
    return{
        type: TIMELINE_SELECTED_TWO
    }
}

export const singleDatesSelectedTwo = () => {
    return{
        type: SINGLE_DATES_SELECTED_TWO
    }
}

export const addDateTwo = () => {
    return{
        type: ADD_DATE_TWO
    }
}

export const timelineSelectedThree = () => {
    return{
        type: TIMELINE_SELECTED_THREE
    }
}

export const singleDatesSelectedThree = () => {
    return{
        type: SINGLE_DATES_SELECTED_THREE
    }
}

export const addDateThree = () => {
    return{
        type: ADD_DATE_THREE
    }
}

export const timelineSelectedFour = () => {
    return{
        type: TIMELINE_SELECTED_FOUR
    }
}

export const singleDatesSelectedFour = () => {
    return{
        type: SINGLE_DATES_SELECTED_FOUR
    }
}

export const addDateFour = () => {
    return{
        type: ADD_DATE_FOUR
    }
}

export const timelineSelectedRequests = () => {
    return{
        type: TIMELINE_SELECTED_REQUESTS
    }
}

export const singleDatesSelectedRequests = () => {
    return{
        type: SINGLE_DATES_SELECTED_REQUESTS
    }
}

export const addDateRequests = () => {
    return{
        type: ADD_DATE_REQUESTS
    }
}

export const timelineSelectedDowntime = () => {
    return{
        type: TIMELINE_SELECTED_DOWNTIME
    }
}

export const singleDatesSelectedDowntime = () => {
    return{
        type: SINGLE_DATES_SELECTED_DOWNTIME
    }
}

export const addDateDowntime = () => {
    return{
        type: ADD_DATE_DOWNTIME
    }
}

export const timelineSelectedTimeouts = () => {
    return{
        type: TIMELINE_SELECTED_TIMEOUTS
    }
}

export const singleDatesSelectedTimeouts = () => {
    return{
        type: SINGLE_DATES_SELECTED_TIMEOUTS
    }
}

export const addDateTimeouts = () => {
    return{
        type: ADD_DATE_TIMEOUTS
    }
}

export const deleteDate = () => {
    return{
        type: DELETE_DATE
    }
}

export const deleteDateTwo = () => {
    return{
        type: DELETE_DATE_TWO
    }
}

export const deleteDateThree = () => {
    return{
        type: DELETE_DATE_THREE
    }
}

export const deleteDateFour = () => {
    return{
        type: DELETE_DATE_FOUR
    }
}

export const deleteDateRequests = () => {
    return{
        type: DELETE_DATE_REQUESTS
    }
}

export const deleteDateTimeouts = () => {
    return{
        type: DELETE_DATE_TIMEOUTS
    }
}

export const deleteDateDowntime = () => {
    return{
        type: DELETE_DATE_DOWNTIME
    }
}

export const handleCloseFontsize = () => {
    return{
        type: HANDLE_CLOSE_FONTSIZE
    }
}

export const handleOpenFontsize = () => {
    return{
        type: HANDLE_OPEN_FONTSIZE
    }
}

export const handleCloseFontstyle = () => {
    return{
        type: HANDLE_CLOSE_FONTSTYLE
    }
}

export const handleOpenFontstyle = () => {
    return{
        type: HANDLE_OPEN_FONTSTYLE
    }
}

export const handleOpenPrimaryColor = () => {
    return{
        type: HANDLE_OPEN_PRIMARY_COLOR
    }
}

export const handleClosePrimaryColor = () => {
    return{
        type: HANDLE_CLOSE_PRIMARY_COLOR
    }
}

export const handleOpenSecondaryColor = () => {
    return{
        type: HANDLE_OPEN_SECONDARY_COLOR
    }
}

export const handleCloseSecondaryColor = () => {
    return{
        type: HANDLE_CLOSE_SECONDARY_COLOR
    }
}

export const handleToggleLanguage = () => {
    return{
        type: HANDLE_TOGGLE_LANGUAGE
    }
}

export const handleToggleCharts = () => {
    return{
        type: HANDLE_TOGGLE_CHARTS
    }
}

export const handleToggleTooltips = () => {
    return{
        type: HANDLE_TOGGLE_TOOLTIPS
    }
}

export const handleLoggedInToggle = (stayLoggedIn) => {
    if(!stayLoggedIn){
        clearTimeout(timeout)
        return{
            type: HANDLE_TOGGLE_STAYLOGGEDIN
        }
    }else{
        return(dispatch) => {
            dispatch({
                type: HANDLE_TOGGLE_STAYLOGGEDIN
            });
            timeout = setTimeout(() => {dispatch(logOut())}, 600000)
        }
    }
}

export const handleDeleteChartsPossible = () => {
    return{
        type: HANDLE_TOGGLE_DELETECHARTSPOSSIBLE
    }
}

export const changeFontsize = (value) =>  {
    return{
        type: CHANGE_FONTSIZE,
        payload: value
    }
}

export const changeFontstyle = (style) =>  {
    return{
        type: CHANGE_FONTSTYLE,
        payload: style
    }
}

export const changeColorPrimary = (color) =>  {
    return{
        type: CHANGE_COLOR_PRIMARY,
        payload: color
    }
}

export const changeColorSecondary = (color) =>  {
    return{
        type: CHANGE_COLOR_SECONDARY,
        payload: color
    }
}

export const resetSettings = () => {
    return{
        type: RESET_SETTINGS
    }
}

export const openNewUserForm = () => {
    return{
        type: OPEN_NEW_USER_FORM
    }
}

export const closeNewUserForm = () => {
    return{
        type: CLOSE_NEW_USER_FORM
    }
}

export const addUser =(user) => {
    fetch('https://my-cool-api.herokuapp.com/api/addUser', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(user)
    })
    return{
        type: ADD_USER
    }
};

export const changeLanguage = (language) =>  {
    return{
        type: CHANGE_LANGUAGE,
        payload: language
    }
}

export const changeChartLibrary = (library) =>  {
    return{
        type: CHANGE_CHART_LIBRARY,
        payload: library
    }
}

export const handleFilter = () => {
    return{
        type: HANDLE_FILTER
    }
}

export const openHttpDialog = () => {
    return{
        type: OPEN_HTTP_DIALOG
    }
}
export const closeHttpDialog = () => {
    return{
        type: CLOSE_HTTP_DIALOG
    }
}

export const handleDrawerChange = (value) => {
    return{
        type: HANDLE_DRAWER_CHANGE,
        payload: value
    }
}

export const openTableComment = () => {
    return{
        type: OPEN_TABLE_COMMENT
    }
}

export const editTableComment = (id, comment, path)  => dispatch => {
    fetch('https://my-json-db.herokuapp.com/' + path + "/" + id, {
        method: 'PATCH',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({
            comment: comment
        })
    }).then(res => res.json())
        .then(comment => dispatch({
            type: EDIT_TABLE_COMMENT,
            payload: comment.comment
        }))
}

export const closeTableComment = () => {
    return{
        type: CLOSE_TABLE_COMMENT,
    }
}

export const getComment = (comment) => {
    return{
        type: GET_COMMENT,
        payload: comment
    }
}

export const openChartEdit = () => {
  return{
      type: OPEN_CHART_EDIT
  }
}

export const closeChartEdit = () => {
    return{
        type: CLOSE_CHART_EDIT
    }
}

export const editChart = (id, type, title, color, table, path)   => {
    if(table === true){
        fetch('https://my-json-db.herokuapp.com/tableData' + path + "/" + id, {
            method: 'PATCH',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                title: title,
            })
        })
    }else {
        fetch('https://my-json-db.herokuapp.com/chartData' + path + "/" + id, {
            method: 'PATCH',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                selection: type,
                title: title,
                color: color
            })
        })
    }
}

export const editChartComment = (id, comment, path)  => dispatch => {
    fetch('https://my-json-db.herokuapp.com/' + path  + "/" + id, {
        method: 'PATCH',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({
            comment: comment,
        })
    }).then(res => res.json())
        .then(comment => dispatch({
            type: EDIT_CHART_COMMENT,
            payload: comment.comment
        }))
}

export const openNoAdminAccess = () => {
    return{
        type: OPEN_NO_ADMIN_ACCESS
    }
}

export const closeNoAdminAccess = () => {
    return{
        type: CLOSE_NO_ADMIN_ACCESS
    }
}

export const openDotDetailsDialog = () => {
    return{
        type: OPEN_DOT_DETAILS_DIALOG
    }
}

export const closeDotDetailsDialog = () => {
    return{
        type: CLOSE_DOT_DETAILS_DIALOG
    }
}

export const getDotDetails = (dot) => {
    return{
        type: GET_DOT_DETAILS,
        payload: dot
    }
}

export const fetchDataAllRequests = () => dispatch => {
    fetch('https://my-json-db.herokuapp.com/AllRequests')
        .then(res => res.json())
        .then(data => dispatch({
            type: FETCH_REQUEST_DATA,
            payload: data
        }))
};








