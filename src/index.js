import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import App from './Layout/App';
import registerServiceWorker from './registerServiceWorker';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core'
import blue from '@material-ui/core/colors/blue'
import amber from '@material-ui/core/colors/amber'

/*const theme = createMuiTheme({
    palette:{
        primary:{
            light: blue[300],
            main: blue[700],
            dark: blue[900]
        } ,
        secondary:{
            main: amber[900]
        },
    },
    typography:{
        fontSize: 14,
        fontFamily: "Arial"
    }
})*/

ReactDOM.render(
//<MuiThemeProvider theme={theme}>
    <App />
//</MuiThemeProvider>
    , document.getElementById('root'));
registerServiceWorker();
