import {Link} from "react-router-dom";
import Tab from "@material-ui/core/Tab/Tab";
import Tabs from "@material-ui/core/Tabs/Tabs";
import React from "react";


class TabsEnglish extends React.Component{

    render(){

        const {value, handleChange} = this.props

        return(
            <Tabs
                value={value}
                indicatorColor="secondary"
                textColor="secondary"
                centered={true}
            >
                <Link to='/' onClick={() => handleChange(0)} style={{textDecoration: 'none'}}>
                    <Tab label="One"/>
                </Link>
                <Link to='/page=2' onClick={() => handleChange(1)} style={{textDecoration: 'none'}}>
                    <Tab label="Two"/>
                </Link>
                <Link to='/page=3' onClick={() => handleChange(2)} style={{textDecoration: 'none'}}>
                    <Tab label="Three"/>
                </Link>
                <Link to='/page=4' onClick={() => handleChange(3)} style={{textDecoration: 'none'}}>
                    <Tab label="Four"/>
                </Link>
            </Tabs>
        )
    }
}

export default TabsEnglish
