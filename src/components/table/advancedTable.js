import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import {handleFilter, openHttpDialog, closeHttpDialog} from '../../actions/dataActions'
import {connect} from 'react-redux'
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import TextField from "@material-ui/core/TextField/TextField";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import Form from "../../containers/Form";
import Dialog from "@material-ui/core/Dialog/Dialog";

let counter = 0;
function createData( http_body, request_uuid, request_url, http_method, origin, timestamp, content_type, incoming_number, primary_key, info) {
    counter += 1;
    return {counter, http_body, request_uuid, request_url, http_method, origin, timestamp, content_type, incoming_number, primary_key, info };
}

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
    { id: 'id', numeric: true, disablePadding: true, label: 'ID' },
    { id: 'http_body', numeric: false, disablePadding: false, label: 'HTTP Body Short' },
    { id: 'request_uuid', numeric: false, disablePadding: false, label: 'Request UUID' },
    { id: 'request_url', numeric: false, disablePadding: false, label: 'Request URL' },
    { id: 'http_method', numeric: false, disablePadding: false, label: 'HTTP Method' },
    { id: 'origin', numeric: false, disablePadding: false, label: 'Origin' },
    { id: 'timestamp', numeric: false, disablePadding: false, label: 'Timestamp' },
    { id: 'content_type', numeric: false, disablePadding: false, label: 'Content Type' },
    { id: 'incoming_number', numeric: true, disablePadding: true, label: 'Incoming Number' },
    { id: 'primary_key', numeric: true, disablePadding: true, label: 'Primary Key' },
];



class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const { onSelectAllClick, order, orderBy, numSelected, rowCount, fontsize } = this.props;

        return (
            <TableHead >
                <TableRow style={{backgroundColor:"#B3B5B5"}}>
                    <TableCell padding="checkbox" style={{width: "5.5%"}}>
                        <Checkbox
                            indeterminate={numSelected > 0 && numSelected < rowCount}
                            checked={numSelected === rowCount}
                            onChange={onSelectAllClick}
                        />
                    </TableCell>
                    {rows.map(row => {
                        var a
                        switch(row.id){
                            case "id":
                                a = "3.5%"
                                break;
                            case "http_body":
                                a = "14.25%"
                                break;
                            case "request_uuid":
                                a = "13.5%"
                                break;
                            case "request_url":
                                a = "9.5%"
                                break;
                            case "http_method":
                                a = "10%"
                                break;
                            case "origin":
                                a = "10.45%"
                                break;
                            case "timestamp":
                                a = "10%"
                                break;
                            case "content_type":
                                a = "10%"
                                break;
                            case "incoming_number":
                                a = "10%"
                                break;
                            case "primary_key":
                                a = "12%"
                                break;
                        }
                        return (
                            <TableCell
                                key={row.id}
                                style={{width: a, flexDirection: "row"}}
                                numeric={row.numeric}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === row.id}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        <Typography variant="caption" align="nowrap" style={{fontSize: fontsize + 2, color: "white"}}>
                                        {row.label}
                                        </Typography>
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>

        );
    }
}


EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },

});

class EnhancedTableToolbar extends React.Component {


    state={
        hello: this.props.columnToQuery,
        query: this.props.query
    }

    handleQChange = event => {
        this.props.handleQueryChange(event.target.value)
        this.setState({
            query: event.target.value
        })
    }

    handleCTQChange = event => {
        this.props.handleColumnChange(event.target.value)
        this.setState({
            hello: event.target.value
        })
    }

    handleFilterChange = () => {
        this.props.handleFilter()
        this.props.handleQueryChange("")
        this.setState({
            query: ""
        })
        this.props.handleColumnChange("")
        this.setState({
            hello: ""
        })

    }

    render() {

        const {numSelected, classes, show} = this.props;


        return (
            <Toolbar
                className={classNames(classes.root, {
                    [classes.highlight]: numSelected > 0,
                })}
            >
                <div className={classes.title}>
                    {numSelected > 0 ? (
                        <Typography color="inherit" variant="subheading">
                            {numSelected} selected
                        </Typography>
                    ) : (
                        <Typography variant="title" id="tableTitle">
                            Server-Data
                        </Typography>
                    )}
                </div>
                <div className={classes.spacer}/>
                {show === true ?
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <TextField
                                    label="Search for"
                                    value={this.state.query}
                                    onChange={this.handleQChange}
                                    margin="normal"
                                    style={{width: 250, marginRight: 25, marginBottom: 15}}
                                />
                            </td>
                            <td>
                                <FormControl style={{width: 250}}>
                                    <InputLabel htmlFor="filterby">
                                        Filter by
                                    </InputLabel>
                                    <Select
                                        value={this.state.hello}
                                        onChange={this.handleCTQChange}

                                        fullWidth
                                    >
                                        <MenuItem key={"http_body"} value={"http_body"}>
                                            HTTP-Body-Short
                                        </MenuItem>
                                        <MenuItem key={"request_uuid"} value={"request_uuid"}>
                                            Request-UUID
                                        </MenuItem>
                                        <MenuItem key={"request_url"} value={"request_url"}>
                                            Request-URL
                                        </MenuItem>
                                        <MenuItem key={"http_method"} value={"http_method"}>
                                            HTTP-Method
                                        </MenuItem>
                                        <MenuItem key={"origin"} value={"origin"}>
                                            Origin
                                        </MenuItem>
                                        <MenuItem key={"timestamp"} value={"timestamp"}>
                                            Timestamp
                                        </MenuItem>
                                        <MenuItem key={"content_type"} value={"content_type"}>
                                            Content-Type
                                        </MenuItem>
                                        <MenuItem key={"incoming_number"} value={"incoming_number"}>
                                            Incoming Number
                                        </MenuItem>
                                        <MenuItem key={"primary_key"} value={"primary_key"}>
                                            Primary-Key
                                        </MenuItem>
                                    </Select>
                                </FormControl>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    : null}
                <div className={classes.actions}>
                    {numSelected > 0 ? (
                        <Tooltip title="Delete">
                            <IconButton aria-label="Delete">
                                <DeleteIcon/>
                            </IconButton>
                        </Tooltip>
                    ) : (
                        <Tooltip title="Filter list">
                            <IconButton aria-label="Filter list" onClick={this.handleFilterChange}>
                                <FilterListIcon/>
                            </IconButton>
                        </Tooltip>
                    )}
                </div>
            </Toolbar>
        );
    };
}

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        align:"left",
    },
    table: {
        width: '100%',
        align:"left",

    },
    tableWrapper: {
        overflowX: 'auto',
        textAlign:"left"
    },
    row: {
        backgroundColor: "#D3E5F9",
        opacity: 0.7
    },
});



class EnhancedTable extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            tableData: props.tableData,
            serverData: props.data,
            order: 'asc',
            orderBy: 'counter',
            selected: [],
            data: this.getTableData(),
            page: 0,
            rowsPerPage: 12,
            text:"",
            query: "",
            columnToQuery: ""
        }
    }



    getTableData() {
       var dataArray = []
        var date = this.props.tableData.dateSelected
        if(date === ""){
            for(var i = 0; i < 2; i++){
                for(var j = 0; j < 12; j++){
                    var a = createData(this.props.data.Advanced[0].Tag[i][j][0],
                        this.props.data.Advanced[0].Tag[i][j][1],
                        this.props.data.Advanced[0].Tag[i][j][2],
                        this.props.data.Advanced[0].Tag[i][j][3],
                        this.props.data.Advanced[0].Tag[i][j][4],
                        this.props.data.Advanced[0].Tag[i][j][5],
                        this.props.data.Advanced[0].Tag[i][j][6],
                        this.props.data.Advanced[0].Tag[i][j][7],
                        this.props.data.Advanced[0].Tag[i][j][8],
                        this.props.data.Advanced[0].Tag[i][j][9])
                    dataArray.push(a)
                }
            }
        }
        else{
            var i
            switch(date){
                case "2018-09-17":
                    i = 0
                    break;
                case "2018-09-18":
                    i = 1
                    break;
            }
                for(var j = 0; j < 12; j++){
                    var a = createData(this.props.data.Advanced[0].Tag[i][j][0],
                        this.props.data.Advanced[0].Tag[i][j][1],
                        this.props.data.Advanced[0].Tag[i][j][2],
                        this.props.data.Advanced[0].Tag[i][j][3],
                        this.props.data.Advanced[0].Tag[i][j][4],
                        this.props.data.Advanced[0].Tag[i][j][5],
                        this.props.data.Advanced[0].Tag[i][j][6],
                        this.props.data.Advanced[0].Tag[i][j][7],
                        this.props.data.Advanced[0].Tag[i][j][8],
                        this.props.data.Advanced[0].Tag[i][j][9])
                    dataArray.push(a)
                }
            }
        counter = 0;
        return dataArray
    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({ order, orderBy });
    };

    handleSelectAllClick = event => {
        if (event.target.checked) {
            this.setState(state => ({ selected: state.data.map(n => n.primary_key) }));
            return;
        }
        this.setState({ selected: [] });
    };

    handleClick = (event, primary_key) => {
        const { selected } = this.state;
        const selectedIndex = selected.indexOf(primary_key);
        let newSelected = [];
        if (selectedIndex === -1) {
            newSelected = newSelected.concat(primary_key);
            this.props.openHttpDialog();
           this.setState({
             text: (this.state.data.filter(day => day.primary_key === newSelected[0]))[0].info
           })
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({ selected: newSelected });
    };

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    handleColumnChange = value => {
        this.setState({
          columnToQuery: value
        })
    }

    handleQueryChange = value => {
        this.setState({
            query: value
        })
    }

    isSelected = primary_key => this.state.selected.indexOf(primary_key) !== -1;

    render() {
        const { classes } = this.props;
        const { data, order, orderBy, selected, rowsPerPage, page, columnToQuery, query } = this.state;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

        const lowerCaseQuery = query.toLowerCase()



         var help
             switch(columnToQuery) {
                 case "http_body":
                    help = data.filter(x => x.http_body.toLowerCase().includes(lowerCaseQuery))
                    break;
                 case "request_uuid":
                     help = data.filter(x => x.request_uuid.includes(lowerCaseQuery))
                     break;
                 case "request_url":
                     help = data.filter(x => x.request_url.includes(lowerCaseQuery))
                     break;
                 case "http_method":
                     help = data.filter(x => x.http_method.toLowerCase().includes(lowerCaseQuery))
                     break;
                 case "origin":
                     help = data.filter(x => x.origin.includes(lowerCaseQuery))
                     break;
                 case "timestamp":
                     help = data.filter(x => x.timestamp.includes(lowerCaseQuery))
                     break;
                 case "content_type":
                     help = data.filter(x => x.content_type.includes(lowerCaseQuery))
                     break;
                 case "incoming_number":
                     help = data.filter(x => x.incoming_number.toString().includes(lowerCaseQuery))
                     break;
                 case "primary_key":
                     help = data.filter(x => x.primary_key.toString().includes(lowerCaseQuery))
                     break;
                 default:
                     help = data
             }
             var counter = 0
                     return (
                         <div>
                             <Dialog
                                 open={this.props.httpOpen}
                                 onClose={this.props.closeHttpDialog}
                                 fullWidth
                                 maxWidth="xs"
                             >
                                 <DialogTitle>
                                     Http-Body
                                 </DialogTitle>
                                 <DialogContent>
                                     <DialogContentText>
                                         {this.state.text}
                                     </DialogContentText>
                                 </DialogContent>
                             </Dialog>
                             <Paper className={classes.root}>
                                 <EnhancedTableToolbar  show={this.props.show} handleFilter={this.props.handleFilter} numSelected={selected.length} columnToQuery={columnToQuery} query={query} handleColumnChange={this.handleColumnChange} handleQueryChange={this.handleQueryChange} />
                                 <div className={classes.tableWrapper}>
                                     <Table className={classes.table } aria-labelledby="tableTitle">
                                         <div >
                                         <EnhancedTableHead
                                             fontsize={this.props.fontsize}
                                             numSelected={selected.length}
                                             order={order}
                                             orderBy={orderBy}
                                             onSelectAllClick={this.handleSelectAllClick}
                                             onRequestSort={this.handleRequestSort}
                                             rowCount={help.length}
                                         />
                                         </div>
                                         <div style={{height:"500px", overflow:"auto"}}>
                                         <TableBody>
                                             {stableSort(help, getSorting(order, orderBy))
                                                 .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                 .map(n => {
                                                     counter++
                                                     const isSelected = this.isSelected(n.primary_key);
                                                     return (
                                                         <TableRow
                                                             className={counter % 2 === 0 && classes.row}
                                                             hover
                                                             onClick={event => this.handleClick(event, n.primary_key)}
                                                             role="checkbox"
                                                             aria-checked={isSelected}
                                                             tabIndex={-1}
                                                             key={n.primary_key}
                                                             selected={isSelected}
                                                         >
                                                             <TableCell padding="checkbox" style={{width: "5.5%"}} >
                                                                 <Checkbox checked={isSelected} />
                                                             </TableCell>
                                                             <TableCell style={{width: "4%", textAlign:"left"}} numeric component="th" scope="row" padding="none">{n.counter}</TableCell>
                                                             <TableCell  style={{width: "10%"}}numeric>{n.http_body} </TableCell>
                                                             <TableCell  style={{width: "17%"}} numeric>{n.request_uuid}</TableCell>
                                                             <TableCell  style={{width: "11%"}} numeric>{n.request_url}</TableCell>
                                                             <TableCell  style={{width: "8%"}} numeric>{n.http_method}</TableCell>
                                                             <TableCell  style={{width: "11%"}} numeric>{n.origin}</TableCell>
                                                             <TableCell  style={{width: "11%"}} numeric>{n.timestamp}</TableCell>
                                                             <TableCell  style={{width: "12%"}} numeric>{n.content_type}</TableCell>
                                                             <TableCell style={{width: "8%"}} numeric>{n.incoming_number}</TableCell>
                                                             <TableCell style={{width: "11%"}} numeric>{n.primary_key}</TableCell>
                                                         </TableRow>
                                                     );
                                                 })}
                                             {emptyRows > 0 && (
                                                 <TableRow style={{ height: 49 * emptyRows }}>
                                                     <TableCell colSpan={6} />
                                                 </TableRow>
                                             )}
                                             </TableBody>
                                         </div>
                                     </Table>
                                 </div>
                                 <TablePagination
                                     component="div"
                                     count={data.length}
                                     rowsPerPage={rowsPerPage}
                                     page={page}
                                     backIconButtonProps={{
                                         'aria-label': 'Previous Page',
                                     }}
                                     nextIconButtonProps={{
                                         'aria-label': 'Next Page',
                                     }}
                                     rowsPerPageOptions={[12, 24, 48 ]}
                                     onChangePage={this.handleChangePage}
                                     onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                 />
                             </Paper>
                         </div>
                     );
             }
}

EnhancedTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    show: state.allReducers.show,
    httpOpen: state.allReducers.httpOpen,
    fontsize: state.allReducers.fontsize
});

export default withStyles(styles)(connect(mapStateToProps, {handleFilter, openHttpDialog, closeHttpDialog})(EnhancedTable));
