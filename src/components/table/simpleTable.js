import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import Switch from "@material-ui/core/Switch";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Form from "../../containers/Form";
import Add from "@material-ui/icons/Add";
import ChartForm from "../../containers/ChartForms/ChartForm";
import Delete from "@material-ui/icons/Delete";

const styles = theme => ({
    rootSimple: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {

    },
});



function simpleTable(table, data, indicator, fontSize){

    var startYear = parseInt(table.chartTimeStart.year)
    var endYear = parseInt(table.chartTimeEnd.year)
    var years = (endYear - startYear) +1
    var yearsArray = []
    while(startYear <= endYear ){
        yearsArray.push(startYear)
        startYear++;
    }

    var startYearTwo = parseInt(table.chartTimeStart.year)
    var endYearTwo = parseInt(table.chartTimeEnd.year)
    var startMonth = parseInt(table.chartTimeStart.month)
    var endMonth = parseInt(table.chartTimeEnd.month)
    var months = 0

    while (startYearTwo !== endYearTwo || startMonth !== endMonth){
        months++;
        if(months > 12){
            months = 12
        }
        startMonth++
        if(startMonth > 12){
            startYearTwo++;
            startMonth = 1
        }
    }

    var row = []
    var rows =[]

    createRows();


    function createRow(month, start)
    {
        row= []
        row.push(month)
        switch(indicator){
            case "Downtime":
                if((month > parseInt(table.chartTimeEnd.month))||(month < parseInt(table.chartTimeStart.month))){
                    row.push("-")
                }else{
                    row.push(data.Downtime[0].Jahr[start.year-2012][month - 1])
                }
                break;
            case "Requests":
                if((month > parseInt(table.chartTimeEnd.month))||(month < parseInt(table.chartTimeStart.month))){
                    row.push("-")
                }else{
                    row.push(data.Requests[0].Jahr[start.year-2012][month - 1])
                }
                break;
            case "Timeouts":
                if((month > parseInt(table.chartTimeEnd.month))||(month < parseInt(table.chartTimeStart.month))){
                    row.push("-")
                }else{
                    row.push(data.Timeouts[0].Jahr[start.year-2012][month - 1])
                }
                break;
        }
        rows.push(row)
    }

    function createRowTwo(month, start)
    {
        row= []
        row.push(month)
        switch(indicator){
            case "Downtime":
                if(month < parseInt(table.chartTimeStart.month)){
                    row.push("-")
                }else{
                    row.push(data.Downtime[0].Jahr[start.year-2012][month - 1])
                }
                if(month > parseInt(table.chartTimeEnd.month)){
                    row.push("-")
                }else{
                    row.push(data.Downtime[0].Jahr[start.year-2011][month - 1])
                }
                break;
            case "Requests":
                if(month < parseInt(table.chartTimeStart.month)){
                    row.push("-")
                }else{
                    row.push(data.Requests[0].Jahr[start.year-2012][month - 1])
                }
                if(month > parseInt(table.chartTimeEnd.month)){
                    row.push("-")
                }else{
                    row.push(data.Requests[0].Jahr[start.year-2011][month - 1])
                }
                break;
            case "Timeouts":
                if(month < parseInt(table.chartTimeStart.month)){
                    row.push("-")
                }else{
                    row.push(data.Timeouts[0].Jahr[start.year-2012][month - 1])
                }
                if(month > parseInt(table.chartTimeEnd.month)){
                    row.push("-")
                }else{
                    row.push(data.Timeouts[0].Jahr[start.year-2011][month - 1])
                }
                break;
        }
        rows.push(row)
    }

    function createRowThree(month, start)
    {
        row= []
        row.push(month)
        switch(indicator){
            case "Downtime":
                if(month < parseInt(table.chartTimeStart.month)){
                    row.push("-")
                }else{
                    row.push(data.Downtime[0].Jahr[start.year-2012][month - 1])
                }
                row.push(data.Downtime[0].Jahr[start.year-2011][month - 1])
                if(month > parseInt(table.chartTimeEnd.month)){
                    row.push("-")
                }else{
                    row.push(data.Downtime[0].Jahr[start.year-2010][month - 1])
                }
                break;
            case "Requests":
                if(month < parseInt(table.chartTimeStart.month)){
                    row.push("-")
                }else{
                    row.push(data.Requests[0].Jahr[start.year-2012][month - 1])
                }
                row.push(data.Requests[0].Jahr[start.year-2011][month - 1])
                if(month > parseInt(table.chartTimeEnd.month)){
                    row.push("-")
                }else{
                    row.push(data.Requests[0].Jahr[start.year-2010][month - 1])
                }
                break;
            case "Timeouts":
                if(month < parseInt(table.chartTimeStart.month)){
                    row.push("-")
                }else{
                    row.push(data.Timeouts[0].Jahr[start.year-2012][month - 1])
                }
                row.push(data.Timeouts[0].Jahr[start.year-2011][month - 1])
                if(month > parseInt(table.chartTimeEnd.month)){
                    row.push("-")
                }else{
                    row.push(data.Timeouts[0].Jahr[start.year-2010][month - 1])
                }
                break;
        }
        rows.push(row)
    }

    function createRowFour(month, start)
    {
        row= []
        row.push(month)
        switch(indicator){
            case "Downtime":

                break;
            case "Requests":

                break;
            case "Timeouts":

                break;
        }

        row = [month, data.Downtime[0].Jahr[start.year-2012][month - 1], data.Downtime[0].Jahr[start.year-2011][month - 1],
            data.Downtime[0].Jahr[start.year-2010][month - 1], data.Downtime[0].Jahr[start.year-2009][month - 1]]
        rows.push(row)
    }

    function createRowFive(month, start)
    {
        row= []
        row.push(month)
        switch(indicator){
            case "Downtime":

                break;
            case "Requests":

                break;
            case "Timeouts":

                break;
        }

        row = [month, data.Downtime[0].Jahr[start.year-2012][month - 1], data.Downtime[0].Jahr[start.year-2011][month - 1],
            data.Downtime[0].Jahr[start.year-2010][month - 1], data.Downtime[0].Jahr[start.year-2009][month - 1], data.Downtime[0].Jahr[start.year-2008][month - 1]]
        rows.push(row)
    }

    function createRowSix(month, start)
    {
        row= []
        row.push(month)
        switch(indicator){
            case "Downtime":

                break;
            case "Requests":

                break;
            case "Timeouts":

                break;
        }

        row = [month, data.Downtime[0].Jahr[start.year-2012][month - 1], data.Downtime[0].Jahr[start.year-2011][month - 1],
            data.Downtime[0].Jahr[start.year-2010][month - 1], data.Downtime[0].Jahr[start.year-2009][month - 1],
            data.Downtime[0].Jahr[start.year-2008][month - 1],  data.Downtime[0].Jahr[start.year-2007][month - 1]]
        rows.push(row)
    }

     function createRows(){
        var startMonthFour = 1
         var endYearFour = parseInt(table.chartTimeEnd.year)
         var startYearFour = parseInt(table.chartTimeStart.year)
        var endMonthFour = parseInt(table.chartTimeEnd.month)
         if(endYearFour > startYearFour){
             for(var i = 1; i <= 12; i++){
                 switch (years) {
                     case 1:
                         createRow(startMonthFour, table.chartTimeStart)
                         break;
                     case 2:
                         createRowTwo(startMonthFour, table.chartTimeStart)
                         break;
                     case 3:
                         createRowThree(startMonthFour, table.chartTimeStart)
                         break;
                     case 4:
                         createRowFour(startMonthFour, table.chartTimeStart)
                         break;
                     case 5:
                         createRowFive(startMonthFour, table.chartTimeStart)
                         break;
                     case 6:
                         createRowSix(startMonthFour, table.chartTimeStart)
                         break;
             }
             startMonthFour++
         }}else{
         while(startMonthFour <= endMonthFour){
        switch (years) {
            case 1:
                createRow(startMonthFour, table.chartTimeStart)
                break;
            case 2:
                createRowTwo(startMonthFour, table.chartTimeStart)
                break;
            case 3:
                createRowThree(startMonthFour, table.chartTimeStart)
                break;
            case 4:
                createRowFour(startMonthFour, table.chartTimeStart)
                break;
            case 5:
                createRowFive(startMonthFour, table.chartTimeStart)
                break;
            case 6:
                createRowSix(startMonthFour, table.chartTimeStart)
                break;
        }
        startMonthFour++
        }
    }}

    var counter = 1
    var summe1 = 0
    var summe2 = 0
    var summe3 = 0
    var summArray = []

    return (
            <Table style={{ width: '100%', overflowX: 'auto',}}>
                <TableHead>
                    <TableRow style={{backgroundColor:"#B3B5B5"}}>
                        <TableCell style={{textAlign: "center"}}><Typography variant="caption" align="nowrap" style={{fontSize: fontSize + 2, color: "white"}}>
                            Month / Year</Typography></TableCell>
                        {yearsArray.map(year =>{
                            return(<TableCell ><Typography variant="caption" align="nowrap" style={{fontSize: fontSize + 2, color: "white"}}>
                                {year + ""}</Typography></TableCell>)
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => {
                        if(counter % 2 === 0){
                        return (
                            <TableRow style={{backgroundColor: "#D3E5F9", opacity: 0.7}}>
                                {row.map(number => {
                                    if(row.indexOf(number) === 1 || row.indexOf(number) === 2 || row.indexOf(number) === 3){
                                        if(!isNaN(parseInt(number))) {
                                            if (row.indexOf(number) === 1) {
                                                summe1 += parseInt(number)
                                                summArray[0] = summe1
                                            } else if (row.indexOf(number) === 2) {
                                                summe2 += parseInt(number)
                                                summArray[1] = summe2
                                            } else if (row.indexOf(number) === 3) {
                                                summe3 += parseInt(number)
                                                summArray[2] = summe3
                                            }
                                        }
                                        return(<TableCell>{number}</TableCell>)
                                    }else {
                                        return (<TableCell style={{textAlign: "center", fontSize: fontSize + 2, color:"#778899"}}>{number}</TableCell>)
                                    }
                                }, counter++)}
                            </TableRow>
                        );
                    }else{
                            return (
                                <TableRow>
                                    {row.map(number => {
                                        if(row.indexOf(number) === 1 || row.indexOf(number) === 2 || row.indexOf(number) === 3){
                                            if(!isNaN(parseInt(number))) {
                                                if (row.indexOf(number) === 1) {
                                                    summe1 += parseInt(number)
                                                    summArray[0] = summe1
                                                } else if (row.indexOf(number) === 2) {
                                                    summe2 += parseInt(number)
                                                    summArray[1] = summe2
                                                } else if (row.indexOf(number) === 3) {
                                                    summe3 += parseInt(number)
                                                    summArray[2] = summe3
                                                }
                                            }
                                            return(<TableCell>{number}</TableCell>)
                                        }else {
                                            return (<TableCell style={{textAlign: "center", fontSize: fontSize + 2, color:"#778899"}}>{number}</TableCell>)
                                        }
                                    }, counter++)}
                                </TableRow>)
                        }
                    })}
                    <TableRow>
                        <TableCell style={{textAlign: "center", fontSize: fontSize + 2, borderTop:"2px solid", borderTopColor:"#B3B5B5" }}><Typography>Summe</Typography></TableCell>
                        {summArray.map( sum => {
                           return(<TableCell style={{ borderTop:"2px solid" , borderTopColor:"#B3B5B5" }}><Typography>{sum}</Typography></TableCell>)
                        })}
                    </TableRow>
                </TableBody>
            </Table>
    );
}

export default simpleTable