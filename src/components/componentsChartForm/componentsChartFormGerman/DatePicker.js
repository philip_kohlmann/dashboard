import TextField from "@material-ui/core/TextField/TextField";
import React from "react";
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: "6%"
    }
});

class DatePicker extends React.Component{

    render(){

        const {hello} = this.props

        const { classes } = this.props;

        return(
            <form className={classes.container} noValidate>
                <TextField
                    id="date"
                    label="Wähle Datum aus"
                    type="date"
                    defaultValue=""
                    className={classes.textField}
                    onChange={hello}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </form>
        )
    }
}

export default withStyles(styles)(DatePicker)

