import Button from "@material-ui/core/Button/Button";
import React from "react";
import ChartTitleField from "./ChartTitleField"
import ChartTypeSelectGerman from "./ChartTypeSelectGerman"
import ChartColorSelectGerman from "./ChartColorSelectGerman"
import ChartIndicatorSelectGerman from "./ChartIndicatorSelectGerman"
import RbSimpleAdvanced from "./RbSimpleAdvanced"
import RbTimelineSingle from "./RbTimelineSingle"
import DatePicker from "./DatePicker"
import TimelineSelected from "./TimelineSelected"
import SingleDatesSelected from "./SingleDatesSelected"



class ChartFormGerman extends React.Component{

    state={

    }

    render(){

        const {handleChange, handleChangeRb, hello, handleSubmit, addDate, deleteDate, type, color, simple, indicator, isTimelineSelected,
            startYear, startMonth, endMonth, endYear, isSingleDatesSelected, dates, years, months, title, rbValue, origin, formType} = this.props

        return(
            <div>
                <ChartTitleField handleChange={handleChange} title={title}/>
                <br/>
                <ChartTypeSelectGerman handleChange={handleChange} type={type} formType={formType}/>
                <br/>
                {type !== "Pie" && type!=="Table"?
                    <ChartColorSelectGerman handleChange={handleChange} color={color}/>
                    : null}
                {simple !== false && origin === "Home" ?
                    <ChartIndicatorSelectGerman handleChange={handleChange} indicator={indicator}/>
                    : null}
                {type === "Table" ?
                    <RbSimpleAdvanced handleChangeRb={handleChangeRb} rbValue={rbValue}/>
                    :null}
                {type !== "Table" ?
                    <RbTimelineSingle handleChangeRb={handleChangeRb} rbValue={rbValue}/>
                    : null}
                {simple === false ?
                    <DatePicker hello={hello}/>
                    : null}
                {isTimelineSelected || (type === "Table" && simple === true) ?
                    <TimelineSelected handleChange={handleChange} startYear={startYear} startMonth={startMonth} endYear={endYear} endMonth={endMonth}/>
                    : isSingleDatesSelected && type !== "Table" ?
                        <SingleDatesSelected addDate={addDate} deleteDate={deleteDate} dates={dates}/>
                        :null
                }
                <br/>
                {simple !== false ?
                    <Button
                        color="primary"
                        variant="raised"
                        onClick={handleSubmit}
                        disabled={!title || !type  || (origin === "Home" && !indicator) || !((startMonth && startYear && endMonth && endYear )||(years[0]&&months[0]))}>
                        Grafik erstellen
                    </Button> :
                    <Button
                        color="primary"
                        variant="raised"
                        onClick={handleSubmit}
                        disabled={!title || !type }>
                        Grafik erstellen
                    </Button>}
            </div>
        )
    }
}

export default ChartFormGerman
