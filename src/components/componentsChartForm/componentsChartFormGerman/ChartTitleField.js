import TextField from "@material-ui/core/TextField/TextField";
import React from "react";

class ChartTitleField extends React.Component{

    render(){

        const {handleChange, title} = this.props

        return(
            <TextField
                label="Titel"
                value={title}
                onChange={handleChange('title')}
                margin="normal"
                fullWidth
            />
        )
    }
}

export default ChartTitleField