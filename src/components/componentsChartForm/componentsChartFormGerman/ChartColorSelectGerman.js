import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";

class ChartColorSelectGerman extends React.Component{

    render(){

        const {handleChange, color} = this.props

        return(
            <FormControl fullWidth>
                <InputLabel htmlFor="color">
                    Chartfarbe
                </InputLabel>
                <Select
                    value={color}
                    onChange={handleChange('color')}
                    fullWidth
                >
                    <MenuItem key={"Green"} value={"Green"}>
                        Grün
                    </MenuItem>
                    <MenuItem key={"Blue"} value={"Blue"}>
                        Blau
                    </MenuItem>
                    <MenuItem key={"Red"} value={"Red"}>
                        Rot
                    </MenuItem>
                </Select>
            </FormControl>
        )
    }
}

export default ChartColorSelectGerman
