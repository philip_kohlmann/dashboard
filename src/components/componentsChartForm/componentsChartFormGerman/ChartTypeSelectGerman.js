import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";

class ChartTypeSelectGerman extends React.Component{

    render(){

        const{handleChange, type, formType} = this.props

        return(
            <FormControl fullWidth>
                {formType === "chart" ?
                    <div>
                <InputLabel htmlFor="type">
                    Charttyp
                </InputLabel>
                <Select
                    value={type}
                    onChange={handleChange('type')}
                    fullWidth
                >
                    <MenuItem key={"Pie"} value={"Pie"}>
                        Torte
                    </MenuItem>
                    <MenuItem key={"Bar"} value={"Bar"}>
                        Balken
                    </MenuItem>
                    <MenuItem key={"Line"} value={"Line"}>
                        Linie
                    </MenuItem>
                </Select>
                    </div>
                :                      <div>
                        <InputLabel htmlFor="type">
                            Charttyp
                        </InputLabel>
                        <Select
                            value={type}
                            onChange={handleChange('type')}
                            fullWidth
                        >
                            <MenuItem key={"Table"} value={"Table"}>
                                Tabelle
                            </MenuItem>
                        </Select>
                    </div> }
            </FormControl>
        )
    }
}

export default ChartTypeSelectGerman