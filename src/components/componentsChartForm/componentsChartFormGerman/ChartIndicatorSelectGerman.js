import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";

class ChartIndicatorSelectGerman extends React.Component{

    render(){

        const {handleChange, indicator} = this.props

        return(
            <FormControl fullWidth>
                <InputLabel htmlFor="indicator">
                    Messgröße
                </InputLabel>
                <Select
                    value={indicator}
                    onChange={handleChange('indicator')}
                    fullWidth
                >
                    <MenuItem key={"Requests"} value={"Requests"}>
                        Anfragen
                    </MenuItem>
                    <MenuItem key={"Timeouts"} value={"Timeouts"}>
                        Zeitüberschreitungen
                    </MenuItem>
                    <MenuItem key={"Downtime"} value={"Downtime"}>
                        Ausfallzeit
                    </MenuItem>
                </Select>
            </FormControl>
        )
    }
}

export default ChartIndicatorSelectGerman

