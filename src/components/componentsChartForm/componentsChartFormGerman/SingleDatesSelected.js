import IconButton from "@material-ui/core/IconButton/IconButton";
import Add from "@material-ui/icons/Add";
import Typography from "@material-ui/core/Typography/Typography";
import React from "react";
import Remove from "@material-ui/icons/Remove";

class SingleDatesSelected extends React.Component{

    render(){

        const {addDate, deleteDate, dates} = this.props

        return(
            <div>
                <table width = "100%">
                    <tbody>
                    <tr>
                        <td align="left">
                            <IconButton
                                color="secondary"
                                onClick={addDate}
                            >
                                <Add style={{ minWidth: '30px', minHeight: '30px'}} color="secondary"/>
                            </IconButton>
                        </td>
                        <td align="center">
                            <Typography> Datum hinzufügen (Maximum 10)</Typography>
                        </td>
                        <td align="right">
                            <IconButton
                                color="secondary"
                                onClick={deleteDate}
                            >
                                <Remove style={{ minWidth: '30px', minHeight: '30px'}} color="secondary"/>
                            </IconButton>
                        </td>
                    </tr>
                    </tbody>
                </table>
                {dates}
            </div>
        )
    }

}

export default SingleDatesSelected

