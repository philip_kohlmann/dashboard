import FormLabel from "@material-ui/core/FormLabel/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Radio from "@material-ui/core/Radio/Radio";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    formControl: {
        margin: theme.spacing.unit * 3,
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
    }
});

class RbSimpleAdvanced extends React.Component{

    state={

    }
    render(){

        const {handleChangeRb, rbValue} = this.props

        const { classes } = this.props;

        return(
            <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">Wähle das Tabellenformat</FormLabel>
                <RadioGroup
                    className={classes.group}
                    value={rbValue}
                    onChange={handleChangeRb}
                >
                    <FormControlLabel value="simple" control={<Radio />} label="Einfache Tabelle" />
                    <FormControlLabel value="advanced" control={<Radio/>} label="Erweiterte Tabelle"/>
                </RadioGroup>
            </FormControl>
        )
    }
}

export default withStyles(styles)(RbSimpleAdvanced)