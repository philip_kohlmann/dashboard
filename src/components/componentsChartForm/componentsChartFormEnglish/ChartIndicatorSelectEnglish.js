import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";

class ChartIndicatorSelectEnglish extends React.Component{

    render(){

        const {handleChange, indicator} = this.props

        return(
            <FormControl fullWidth>
                <InputLabel htmlFor="indicator">
                    Indicator
                </InputLabel>
                <Select
                    value={indicator}
                    onChange={handleChange('indicator')}
                    fullWidth
                >
                    <MenuItem key={"Requests"} value={"Requests"}>
                        Requests
                    </MenuItem>
                    <MenuItem key={"Timeouts"} value={"Timeouts"}>
                        Timeouts
                    </MenuItem>
                    <MenuItem key={"Downtime"} value={"Downtime"}>
                        Downtime
                    </MenuItem>
                </Select>
            </FormControl>
        )
    }
}

export default ChartIndicatorSelectEnglish

