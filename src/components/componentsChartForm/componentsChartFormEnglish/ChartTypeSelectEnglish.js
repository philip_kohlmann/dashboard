import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";

class ChartTypeSelectEnglish extends React.Component{

    render(){

        const{handleChange, type, formType} = this.props

        console.log(formType)
        return(

            <FormControl fullWidth>
                {formType === "chart" ?
                 <div>
                <InputLabel htmlFor="type">
                    Chart-Type
                </InputLabel>
                <Select
                    value={type}
                    onChange={handleChange('type')}
                    fullWidth
                >
                            <MenuItem key={"Pie"} value={"Pie"}>
                                Pie
                            </MenuItem>
                            <MenuItem key={"Bar"} value={"Bar"}>
                                Bar
                            </MenuItem>
                            <MenuItem key={"Line"} value={"Line"}>
                                Line
                            </MenuItem>
                </Select>
                 </div>
               :                 <div>
                        <InputLabel htmlFor="type">
                            Chart-Type
                        </InputLabel>
                        <Select
                            value={type}
                            onChange={handleChange('type')}
                            fullWidth
                        >
                            <MenuItem key={"Table"} value={"Table"}>
                                Table
                            </MenuItem>
                        </Select>
                    </div> }
            </FormControl>
        )
    }
}

export default ChartTypeSelectEnglish