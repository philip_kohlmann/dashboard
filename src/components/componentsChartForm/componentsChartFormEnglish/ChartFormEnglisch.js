import Button from "@material-ui/core/Button/Button";
import React from "react";
import ChartTitleField from "./ChartTitleField"
import ChartTypeSelectEnglish from "./ChartTypeSelectEnglish"
import ChartColorSelectEnglish from "./ChartColorSelectEnglish"
import ChartIndicatorSelectEnglish from "./ChartIndicatorSelectEnglish"
import RbSimpleAdvanced from "./RbSimpleAdvanced"
import RbTimelineSingle from "./RbTimelineSingle"
import DatePicker from "./DatePicker"
import TimelineSelected from "./TimelineSelected"
import SingleDatesSelected from "./SingleDatesSelected"



class ChartFormEnglisch extends React.Component{

    state={

    }

render(){

    const {handleChange, handleChangeRb, hello, handleSubmit, addDate, deleteDate, type, color, simple, indicator, isTimelineSelected,
    startYear, startMonth, endMonth, endYear, isSingleDatesSelected, dates, years, months, title, rbValue, origin, formType} = this.props

return(
    <div>
    <ChartTitleField handleChange={handleChange} title={title}/>
    <br/>
    <ChartTypeSelectEnglish handleChange={handleChange} type={type} formType={formType}/>
    <br/>
    {type !== "Pie" && type!=="Table"?
        <ChartColorSelectEnglish handleChange={handleChange} color={color}/>
        : null}
    {simple !== false && origin === "Home" ?
        <ChartIndicatorSelectEnglish handleChange={handleChange} indicator={indicator}/>
        : null}
    {type === "Table" ?
        <RbSimpleAdvanced handleChangeRb={handleChangeRb} rbValue={rbValue}/>
        :null}
    {type !== "Table" ?
        <RbTimelineSingle handleChangeRb={handleChangeRb} rbValue={rbValue}/>
        : null}
    {simple === false ?
        <DatePicker hello={hello}/>
        : null}
    {isTimelineSelected || (type === "Table" && simple === true) ?
        <TimelineSelected handleChange={handleChange} startYear={startYear} startMonth={startMonth} endYear={endYear} endMonth={endMonth}/>
        : isSingleDatesSelected && type !== "Table" ?
            <SingleDatesSelected addDate={addDate} deleteDate={deleteDate} dates={dates}/>
            :null
    }
    <br/>
    {simple !== false ?
        <Button
            color="primary"
            variant="raised"
            onClick={handleSubmit}
            disabled={!title || !type || (simple === true && !indicator) || (origin === "Home" && !indicator) || !((startMonth && startYear && endMonth && endYear )||(years[0]&&months[0]))}>
            Create Chart
        </Button> :
        <Button
            color="primary"
            variant="raised"
            onClick={handleSubmit}
            disabled={!title || !type }>
            Create Chart
        </Button>}
    </div>
)
}
}

export default ChartFormEnglisch
