import FormControl from "@material-ui/core/FormControl/FormControl";
import FormLabel from "@material-ui/core/FormLabel/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Radio from "@material-ui/core/Radio/Radio";
import React from "react";
import {withStyles} from "@material-ui/core";

const styles = theme => ({
    formControl: {
        margin: theme.spacing.unit * 3,
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
    }
});

class RbTimelineSingle extends React.Component{

    state={

    }

    render(){

        const {handleChangeRb, rbValue} = this.props

        const { classes } = this.props;

        return(
            <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">Select Dates or Timeline</FormLabel>
                <RadioGroup
                    className={classes.group}
                    value={rbValue}
                    onChange={handleChangeRb}
                >
                    <FormControlLabel value="Timeline" control={<Radio />} label="Timeline" />
                    <FormControlLabel value="Single Dates" control={<Radio/>} label="Single Dates"/>
                </RadioGroup>
            </FormControl>
        )
    }
}

export default withStyles(styles)(RbTimelineSingle)

