import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";

class ChartColorSelectEnglish extends React.Component{

    render(){

        const {handleChange, color} = this.props

        return(
            <FormControl fullWidth>
                <InputLabel htmlFor="color">
                    Chart-Color
                </InputLabel>
                <Select
                    value={color}
                    onChange={handleChange('color')}
                    fullWidth
                >
                    <MenuItem key={"Green"} value={"Green"}>
                        Green
                    </MenuItem>
                    <MenuItem key={"Blue"} value={"Blue"}>
                        Blue
                    </MenuItem>
                    <MenuItem key={"Red"} value={"Red"}>
                        Red
                    </MenuItem>
                </Select>
            </FormControl>
        )
    }
}

export default ChartColorSelectEnglish
