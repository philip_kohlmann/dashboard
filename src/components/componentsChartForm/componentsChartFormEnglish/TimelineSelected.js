import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import React from "react";

class TimelineSelected extends React.Component{

    render(){

        const {handleChange, startYear, startMonth, endYear, endMonth} = this.props

        return(
            <div>
                <FormControl fullWidth>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <FormControl fullWidth margin="normal">
                                    <InputLabel htmlFor="startYear">
                                        Start-Year
                                    </InputLabel>
                                    <Select
                                        value={startYear}
                                        onChange={handleChange('startYear')}
                                    >
                                        <MenuItem key={"2012"} value={"2012"}>
                                            2012
                                        </MenuItem>
                                        <MenuItem key={"2013"} value={"2013"}>
                                            2013
                                        </MenuItem>
                                        <MenuItem key={"2014"} value={"2014"}>
                                            2014
                                        </MenuItem>
                                        <MenuItem key={"2015"} value={"2015"}>
                                            2015
                                        </MenuItem>
                                        <MenuItem key={"2016"} value={"2016"}>
                                            2016
                                        </MenuItem>
                                        <MenuItem key={"2017"} value={"2017"}>
                                            2017
                                        </MenuItem>
                                        <MenuItem key={"2018"} value={"2018"}>
                                            2018
                                        </MenuItem>
                                    </Select>
                                </FormControl>
                            </td>
                            <td>
                                <FormControl fullWidth margin="normal">
                                    <InputLabel htmlFor="startMonth">
                                        Start-Month
                                    </InputLabel>
                                    <Select
                                        value={startMonth}
                                        onChange={handleChange('startMonth')}
                                    >
                                        <MenuItem key={"1"} value={"1"}>
                                            January
                                        </MenuItem>
                                        <MenuItem key={"2"} value={"2"}>
                                            February
                                        </MenuItem>
                                        <MenuItem key={"3"} value={"3"}>
                                            March
                                        </MenuItem>
                                        <MenuItem key={"4"} value={"4"}>
                                            April
                                        </MenuItem>
                                        <MenuItem key={"5"} value={"5"}>
                                            May
                                        </MenuItem>
                                        <MenuItem key={"6"} value={"6"}>
                                            June
                                        </MenuItem>
                                        <MenuItem key={"7"} value={"7"}>
                                            July
                                        </MenuItem>
                                        <MenuItem key={"8"} value={"8"}>
                                            August
                                        </MenuItem>
                                        <MenuItem key={"9"} value={"9"}>
                                            September
                                        </MenuItem>
                                        <MenuItem key={"10"} value={"10"}>
                                            October
                                        </MenuItem>
                                        <MenuItem key={"11"} value={"11"}>
                                            November
                                        </MenuItem>
                                        <MenuItem key={"12"} value={"12"}>
                                            December
                                        </MenuItem>
                                    </Select>
                                </FormControl>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </FormControl>
                <FormControl fullWidth>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <FormControl fullWidth margin="normal">
                                    <InputLabel htmlFor="endYear">
                                        End-Year
                                    </InputLabel>
                                    <Select
                                        value={endYear}
                                        onChange={handleChange('endYear')}
                                    >
                                        <MenuItem key={"2012"} value={"2012"}>
                                            2012
                                        </MenuItem>
                                        <MenuItem key={"2013"} value={"2013"}>
                                            2013
                                        </MenuItem>
                                        <MenuItem key={"2014"} value={"2014"}>
                                            2014
                                        </MenuItem>
                                        <MenuItem key={"2015"} value={"2015"}>
                                            2015
                                        </MenuItem>
                                        <MenuItem key={"2016"} value={"2016"}>
                                            2016
                                        </MenuItem>
                                        <MenuItem key={"2017"} value={"2017"}>
                                            2017
                                        </MenuItem>
                                        <MenuItem key={"2018"} value={"2018"}>
                                            2018
                                        </MenuItem>
                                    </Select>
                                </FormControl>
                            </td>
                            <td>
                                <FormControl fullWidth margin="normal">
                                    <InputLabel htmlFor="startMonth">
                                        End-Month
                                    </InputLabel>
                                    <Select
                                        value={endMonth}
                                        onChange={handleChange('endMonth')}
                                    >
                                        <MenuItem key={"1"} value={"1"}>
                                            January
                                        </MenuItem>
                                        <MenuItem key={"2"} value={"2"}>
                                            February
                                        </MenuItem>
                                        <MenuItem key={"3"} value={"3"}>
                                            March
                                        </MenuItem>
                                        <MenuItem key={"4"} value={"4"}>
                                            April
                                        </MenuItem>
                                        <MenuItem key={"5"} value={"5"}>
                                            May
                                        </MenuItem>
                                        <MenuItem key={"6"} value={"6"}>
                                            June
                                        </MenuItem>
                                        <MenuItem key={"7"} value={"7"}>
                                            July
                                        </MenuItem>
                                        <MenuItem key={"8"} value={"8"}>
                                            August
                                        </MenuItem>
                                        <MenuItem key={"9"} value={"9"}>
                                            September
                                        </MenuItem>
                                        <MenuItem key={"10"} value={"10"}>
                                            October
                                        </MenuItem>
                                        <MenuItem key={"11"} value={"11"}>
                                            November
                                        </MenuItem>
                                        <MenuItem key={"12"} value={"12"}>
                                            December
                                        </MenuItem>
                                    </Select>
                                </FormControl>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </FormControl>
            </div>
        )
    }
}

export default TimelineSelected
