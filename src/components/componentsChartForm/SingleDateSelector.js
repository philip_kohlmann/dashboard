import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import React from "react";

class SingleDateSelector extends React.Component {

    render() {

        const {date, handleChange, year1,year2, year0, year3, year4, year5, year6, year7, year8, year9, year10,
            month0, month1, month2, month3, month4, month5, month6, month7, month8, month9, month10} = this.props
        return (

            <FormControl fullWidth>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <FormControl fullWidth margin="normal">
                                <InputLabel htmlFor="Year">
                                    Year
                                </InputLabel>
                                <Select
                                    name={"year" + date.key}
                                    value={date.key === 0 ? year0 : date.key === 1 ? year1 : date.key === 2 ? year2
                                        : date.key === 3 ? year3 : date.key === 4 ? year4 : date.key === 5 ? year5
                                            : date.key === 6 ? year6 : date.key === 7 ? year7 : date.key === 8 ? year8
                                                : date.key === 9 ? year9 : year10}
                                    onChange={handleChange}
                                >
                                    <MenuItem key={"2012"} value={"2012"}>
                                        2012
                                    </MenuItem>
                                    <MenuItem key={"2013"} value={"2013"}>
                                        2013
                                    </MenuItem>
                                    <MenuItem key={"2014"} value={"2014"}>
                                        2014
                                    </MenuItem>
                                    <MenuItem key={"2015"} value={"2015"}>
                                        2015
                                    </MenuItem>
                                    <MenuItem key={"2016"} value={"2016"}>
                                        2016
                                    </MenuItem>
                                    <MenuItem key={"2017"} value={"2017"}>
                                        2017
                                    </MenuItem>
                                    <MenuItem key={"2018"} value={"2018"}>
                                        2018
                                    </MenuItem>
                                </Select>
                            </FormControl>
                        </td>
                        <td>
                            <FormControl fullWidth margin="normal">
                                <InputLabel htmlFor="Month">
                                    Month
                                </InputLabel>
                                <Select
                                    name={"month" + date.key}
                                    value={date.key === 0 ? month0 : date.key === 1 ? month1 : date.key === 2 ? month2
                                        : date.key === 3 ? month3 : date.key === 4 ? month4 : date.key === 5 ? month5
                                            : date.key === 6 ? month6 : date.key === 7 ? month7 : date.key === 8 ? month8
                                                : date.key === 9 ? month9 : month10}
                                    onChange={handleChange}
                                >
                                    <MenuItem key={"1"} value={"1"}>
                                        January
                                    </MenuItem>
                                    <MenuItem key={"2"} value={"2"}>
                                        February
                                    </MenuItem>
                                    <MenuItem key={"3"} value={"3"}>
                                        March
                                    </MenuItem>
                                    <MenuItem key={"4"} value={"4"}>
                                        April
                                    </MenuItem>
                                    <MenuItem key={"5"} value={"5"}>
                                        May
                                    </MenuItem>
                                    <MenuItem key={"6"} value={"6"}>
                                        June
                                    </MenuItem>
                                    <MenuItem key={"7"} value={"7"}>
                                        July
                                    </MenuItem>
                                    <MenuItem key={"8"} value={"8"}>
                                        August
                                    </MenuItem>
                                    <MenuItem key={"9"} value={"9"}>
                                        September
                                    </MenuItem>
                                    <MenuItem key={"10"} value={"10"}>
                                        October
                                    </MenuItem>
                                    <MenuItem key={"11"} value={"11"}>
                                        November
                                    </MenuItem>
                                    <MenuItem key={"12"} value={"12"}>
                                        December
                                    </MenuItem>
                                </Select>
                            </FormControl>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </FormControl>
        )
    }
}

export default SingleDateSelector