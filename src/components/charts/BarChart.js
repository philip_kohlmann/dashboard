import React, {Component} from "react";
import XAxis from 'recharts/lib/cartesian/XAxis';
import YAxis from 'recharts/lib/cartesian/YAxis';
import CartesianGrid from 'recharts/lib/cartesian/CartesianGrid';
import Tooltip from 'recharts/lib/component/Tooltip';
import Legend from 'recharts/lib/component/Legend';
import BarChart from 'recharts/lib/chart/BarChart';
import Bar from 'recharts/lib/cartesian/Bar';
import ResponsiveContainer from "recharts/lib/component/ResponsiveContainer";
import Label from "recharts/lib/component/Label";

class BarRechart extends Component{
    constructor(props){
        super(props);
        this.state = {
            chartData:props.chartData,
            chartColor: props.color
        }
    }

    render () {

        const {openDotDetailsDialog, self} = this.props

        return (
            <ResponsiveContainer width={"100%"} height={450}>
            <BarChart  data={this.props.chartData}
                      margin={{top: 5, right: 10, left: 50, bottom: 15}}>
                <CartesianGrid vertical={false} strokeDasharray="3 3"/>
                <XAxis dataKey="name">
                    <Label value="Datum" offset={-10} position="insideBottomRight" />
                </XAxis>
                <YAxis >
                    {this.props.indicator === "Downtime" ?
                        <Label angle={-90} value={this.props.indicator + " in Sekunden"} offset={-5} position="insideLeft" />
                        : <Label angle={-90} value={"Anzahl der " + this.props.indicator} offset={-5} position="insideLeft" />}
                </YAxis>
                <Tooltip/>
                <Bar  barSize={40} dataKey={this.props.indicator} fill={this.props.color} onClick={(e) => openDotDetailsDialog( "HTTP-Requests vom " + e.payload.name, self)}/>
            </BarChart>
            </ResponsiveContainer>
        );
    }
}

export default BarRechart

