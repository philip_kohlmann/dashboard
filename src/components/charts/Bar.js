import React, {Component} from 'react';
import {Bar} from 'react-chartjs-2';

class BarChart extends Component{
    constructor(props){
        super(props);
        this.state = {
            chartData:props.chartData,
            title:props.title
        }
    }

    static defaultProps = {
        displayTitle:false,
        displayLegend: true,
        legendPosition:'right',
    }

    render(){
        return (
            <div className="chart" style={{height:"450px", width:"100%" , marginLeft: "5%"}}>
                <Bar
                    data={this.props.chartData}
                    options={{
                        responsive: true,
                        maintainAspectRatio: false,
                        title:{
                            text: this.props.title,
                            display:this.props.displayTitle,
                            fontSize:25
                        },
                        legend:{
                            display:this.props.displayLegend,
                            position:this.props.legendPosition
                        },
                        tooltips:{
                            enabled: true,
                            borderColor: '#000',
                            borderWidth: 1
                        },
                        scales : {
                            yAxes:[{
                                ticks:{
                                    min:0
                                }
                            }]
                        }
                    }}
                />
            </div>
        )
    }
}

export default BarChart;