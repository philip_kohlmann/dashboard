import React, {Component} from "react";
import XAxis from 'recharts/lib/cartesian/XAxis';
import YAxis from 'recharts/lib/cartesian/YAxis';
import CartesianGrid from 'recharts/lib/cartesian/CartesianGrid';
import Tooltip from 'recharts/lib/component/Tooltip';
import Legend from 'recharts/lib/component/Legend';
import LineChart from 'recharts/lib/chart/LineChart';
import Line from 'recharts/lib/cartesian/Line';
import ResponsiveContainer from 'recharts/lib/component/ResponsiveContainer';
import Label from 'recharts/lib/component/Label';


class LineRechart extends Component{
    constructor(props){
        super(props);
        this.state = {
            chartData:props.chartData,
            chartColor: props.color
        }
    }

    render () {

        const {openDotDetailsDialog, self} = this.props

        return (
            <ResponsiveContainer width={"100%"} height={450}>
            <LineChart data={this.props.chartData}
                      margin={{top: 5, right: 10, left: 50, bottom: 15}}>
                <CartesianGrid vertical={false} strokeDasharray="3 3"/>
                <XAxis dataKey="name">
                <Label value="Datum" offset={-10} position="insideBottomRight" />
                </XAxis>
                <YAxis>
                    {this.props.indicator === "Downtime" ?
                        <Label angle={-90} value={this.props.indicator + " in Sekunden"} offset={-5} position="insideLeft" />
                        : <Label angle={-90} value={"Anzahl der " + this.props.indicator} offset={-5} position="insideLeft" />}
                </YAxis>
                <Tooltip/>
                    <Line type="monotone" stroke="#82ca9d" dataKey={this.props.indicator} fill={this.props.color} activeDot={{ onClick: (e) => openDotDetailsDialog( "HTTP-Requests vom " + e.payload.name, self) }} />
            </LineChart>
            </ResponsiveContainer>
        );
    }
}

export default LineRechart

