import React, {Component} from 'react';
import {Pie} from 'react-chartjs-2';

class PieChart extends Component{
    constructor(props){
        super(props);
        this.state = {
            chartData:props.chartData,
            title: props.title
        }
    }

    static defaultProps = {
        displayTitle:false,
        displayLegend: true,
        legendPosition:'right',
    }

    render(){
        return (
            <div style={{width:"100%", height: 450, }} >
                <Pie height={"80%"}
                    data={this.props.chartData}
                    options={{
                        title:{
                            text: this.props.title,
                            display:this.props.displayTitle,
                            fontSize:25,
                        },
                        tooltips:{
                            enabled: true,
                            borderColor: '#000',
                            borderWidth: 1
                        },
                        legend:{
                            display:this.props.displayLegend,
                            position:this.props.legendPosition
                        },
                        layout:{
                            padding:{
                                left:0,
                                bottom: 50
                            },

                        }

                    }}
                />
            </div>
        )
    }
}

export default PieChart;