import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import IconButton from "@material-ui/core/IconButton/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography/Typography";
import logo from "../../dzhyp.jpg";
import AppBar from "@material-ui/core/AppBar/AppBar";
import React from "react";
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {Link} from "react-router-dom";


const drawerWidth = 240;


const styles = theme => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
        minWidth: 50,
        minHeight: 50
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 0.4,
    },
    logo:{
        flexGrow: 0.6
    },
    appBarSpacer: theme.mixins.toolbar,
});

class MenuBarDeprecated extends React.Component{

    render(){

        const { classes } = this.props;

        const {isDrawerOpen, handleDrawerOpen, language} = this.props

        return(

            <AppBar
                position="absolute"
                className={classNames(classes.appBar, isDrawerOpen && classes.appBarShift)}
            >
                <Toolbar disableGutters={!isDrawerOpen} className={classes.toolbar}>
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="1%">
                    <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        onClick={handleDrawerOpen}
                        className={classNames(
                            classes.menuButton,
                            isDrawerOpen && classes.menuButtonHidden,
                        )}
                    >
                        <MenuIcon style={{ minWidth: '40px', minHeight: '50px'}} />
                    </IconButton>
                            </td>
                            <td width="28.5%">
                    {language === "English"?
                        <Typography variant="title" color="inherit" noWrap className={classes.title}>
                            Dashboard-Settings
                        </Typography> :
                        language === "German" ?
                            <Typography variant="title" color="inherit" noWrap className={classes.title}>
                                Dashboard-Einstellungen
                            </Typography> :
                            null}
                            </td>
                            <td width="50%">
                    <div className={classes.logo}>
                        <Link to= '/' style={{ textDecoration: 'none' }}>
                            <img src={logo} alt="DzHypLogo" style={{ maxWidth: '300px', maxHeight: '60px'}} />
                        </Link>
                    </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Toolbar>
            </AppBar>
        )
    }
}

export default withStyles(styles)(MenuBarDeprecated)