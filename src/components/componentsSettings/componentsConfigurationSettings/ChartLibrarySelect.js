import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import React from "react";
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    formControlCharts: {
        margin: theme.spacing.unit,
        minWidth: 150,
    }
});

class ChartLibrarySelect extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, chartsOpen, handleToggleCharts, library, handleChange} = this.props

        return(
            <div>
                <td width="400%">
                    {language === "English" ?
                        <Typography style={{marginLeft: "4%"}}>
                            Change the Chart Library
                        </Typography> :
                        language === "German" ?
                            <Typography style={{marginLeft: "4%"}}>
                                Änderen Sie die Chart-Bibliothek
                            </Typography>
                            : null
                    }
                </td>
                <td align="right">
                    <FormControl className={classes.formControlCharts}>
                        {language === "English" ?
                            <InputLabel htmlFor="demo-controlled-open-select">Chart-Library</InputLabel> :
                            language === "German" ?
                                <InputLabel htmlFor="demo-controlled-open-select">Chart-Bibliothek</InputLabel> :
                                null}
                        <Select
                            open={chartsOpen}
                            onClose={handleToggleCharts}
                            onOpen={handleToggleCharts}
                            value={library}
                            onChange={handleChange}
                            inputProps={{
                                name: 'chartLibrary',
                                id: 'demo-controlled-open-select',
                            }}
                        >
                            <MenuItem value="Recharts">Recharts</MenuItem>
                            <MenuItem value={"ChartJS"}>ChartJs</MenuItem>
                        </Select>
                    </FormControl>
                </td>
            </div>
        )
    }
}

export default withStyles(styles)(ChartLibrarySelect)