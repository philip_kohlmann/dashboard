import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import React from "react";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    formControlLanguage: {
        margin: theme.spacing.unit,
        minWidth: 150,
    }
});

class LanguageSelect extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, languageOpen, handleToggleLanguage, handleChange} = this.props
        return(
            <div>
                <td width="400%">{language === "English" ?
                    <Typography style={{marginLeft: "4%"}}>
                        Change the language settings
                    </Typography> :
                    language === "German" ?
                        <Typography style={{marginLeft: "4%"}}>
                            Änderen Sie die Spracheinstellung
                        </Typography>
                        : null
                }
                </td>
                <td align="right">
                    <FormControl className={classes.formControlLanguage} >
                        {language === "English" ?
                            <InputLabel htmlFor="demo-controlled-open-select">Language</InputLabel> :
                            language === "German" ?
                                <InputLabel htmlFor="demo-controlled-open-select">Sprache</InputLabel>
                                : null
                        }
                        <Select
                            open={languageOpen}
                            onClose={handleToggleLanguage}
                            onOpen={handleToggleLanguage}
                            value={language}
                            onChange={handleChange}
                            inputProps={{
                                name: 'language',
                                id: 'demo-controlled-open-select',
                            }}
                        >
                            {language === "English" ?
                                <MenuItem value={"German"}>German</MenuItem> :
                                language === "German" ?
                                    <MenuItem value={"German"}>Deutsch</MenuItem> :
                                    null}
                            {language === "English" ?
                                <MenuItem value={"English"}>English</MenuItem> :
                                language === "German" ?
                                    <MenuItem value={"English"}>Englisch</MenuItem> :
                                    null}
                        </Select>
                    </FormControl>
                </td>
            </div>

        )
    }
}

export default withStyles(styles)(LanguageSelect)