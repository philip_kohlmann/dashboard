import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import Switch from "@material-ui/core/Switch/Switch";
import React from "react";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    formControlTooltips: {
        margin: theme.spacing.unit,
        minWidth: 150,
    }
});

class TooltipSettings extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, tooltipsEnabled, handleToggleTooltips} = this.props

        return(

            <div>
                <td width="400%">
                    {language === "English" ?
                        <Typography style={{marginLeft: "4%"}}>
                            Change the Tooltip Settings
                        </Typography> :
                        language === "German" ?
                            <Typography style={{marginLeft: "4%"}}>
                                Änderen Sie die Kurzinfo-Einstellungen
                            </Typography>
                            : null
                    }
                </td>
                <td align="right">
                    <FormControl className={classes.formControlTooltips}>
                        <Switch
                            checked={tooltipsEnabled}
                            onChange={handleToggleTooltips}
                            value="Tooltips"
                            color="secondary"
                        />
                    </FormControl>
                </td>
            </div>
        )
    }
}

export default withStyles(styles)(TooltipSettings)