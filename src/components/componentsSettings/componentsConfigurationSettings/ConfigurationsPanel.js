import Typography from "@material-ui/core/Typography/Typography";
import Paper from "@material-ui/core/Paper/Paper";
import OpenSelectOptionKonfig from "../../../containers/Settings/OpenSelectOptionsKonfig";
import React from "react";


class ConfigurationsPanel extends React.Component{

    render(){

        const {language} = this.props

        return(
            <div>
                <div style={{marginBottom: "10px", marginLeft:"30%"}}>
                     {language === "English" ?
                        <Typography variant="subheading">
                            Configuration
                        </Typography> :
                         language === "German" ?
                         <Typography variant="subheading">
                            Konfiguration
                        </Typography>
                        : null
                }
                 </div>
                     <Paper style={{width:"40%",  marginLeft:"30%"}}>
                        <Typography>
                            <OpenSelectOptionKonfig/>
                         </Typography>
                     </Paper>
                 </div>
        )
    }
}

export default ConfigurationsPanel

