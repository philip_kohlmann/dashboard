import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import NewUserForm from "../../containers/NewUserForm";
import Dialog from "@material-ui/core/Dialog/Dialog";
import React from "react";


class NewUserDialog extends React.Component{

    render(){

        const {language, newUserFormOpen, closeNewUserForm} = this.props

        return(

            <Dialog
                open={newUserFormOpen}
                onClose={closeNewUserForm}
                fullWidth
                maxWidth="xs"
            >
                {language === "English" ?
                    <DialogTitle>
                        Create a new User
                    </DialogTitle>:
                    language === "German" ?
                        <DialogTitle>
                            Erstellen Sie einen neuen Benutzer
                        </DialogTitle>:
                        null}
                <DialogContent>
                    {language === "English" ?
                        <DialogContentText>
                            Please enter Username and Password
                        </DialogContentText>:
                        language === "German" ?
                            <DialogContentText>
                                Bitte geben Sie Benutzernamen und Passwort ein
                            </DialogContentText>:
                            null}
                    <NewUserForm/>
                </DialogContent>
            </Dialog>
        )
    }
}

export default NewUserDialog
