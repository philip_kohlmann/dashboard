import Typography from "@material-ui/core/Typography/Typography";
import Paper from "@material-ui/core/Paper/Paper";
import FormControl from "@material-ui/core/FormControl/FormControl";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Refresh from "@material-ui/icons/Refresh";
import React from "react";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    formControlTooltips: {
        marginLeft: 10,
        margin: theme.spacing.unit,
        minWidth: 150,
    }
});


class ResetSettingsPanel extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, resetSettings, fontsize} = this.props

        return(
            <div>
                <div style={{marginBottom: "10px",marginLeft:"30%"}}>
                    <br/>
                    {language === "English" ?
                        <Typography variant="subheading" >
                            Reset Settings
                        </Typography> :
                        language === "German" ?
                            <Typography variant="subheading">
                                Einstellungen zurücksetzen
                            </Typography>
                            : null
                    }
                </div>
                <Paper style={{width:"40%",  marginLeft:"30%"}}>
                    <table width="95%">
                        <tr>
                            <td>
                                {language === "English" ?
                                    <Typography variant="subheading" style={{marginLeft: "4%", fontSize: fontsize}}>
                                        Restore all the default factory settings
                                    </Typography> :
                                    language === "German" ?
                                        <Typography variant="subheading" style={{marginLeft: "4%", fontSize: fontsize}} >
                                            Setzten Sie alle Einstellungen auf Werkeinstellungen zurück
                                        </Typography>
                                        : null
                                }
                            </td>
                            <td align="right">
                                <FormControl className={classes.formControlTooltips}>
                                    <IconButton
                                        color="secondary"
                                        onClick={resetSettings}
                                    >
                                        <Refresh style={{ minWidth: '30px', minHeight: '30px'}} color="secondary"/>
                                    </IconButton>
                                </FormControl>
                            </td>
                        </tr>
                    </table>
                </Paper>
            </div>

        )
    }
}

export default withStyles(styles)(ResetSettingsPanel)
