import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import Switch from "@material-ui/core/Switch/Switch";
import React from "react";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    formControlDeleteCharts: {
        margin: theme.spacing.unit,
        minWidth: 50,
    }
});

class DeleteChartsSettings extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, deleteChartsPossible, handleDeleteChartsPossible} = this.props

        return(

            <div>
                <td width="400%">
                    {language === "English" ?
                        <Typography style={{marginLeft: "4%"}}>
                            Deleting Charts Possible
                        </Typography> :
                        language === "German" ?
                            <Typography style={{marginLeft: "4%"}}>
                                Grafiken löschbar
                            </Typography>
                            : null
                    }
                </td>
                <td align="right">
                    <FormControl className={classes.formControlDeleteCharts}>
                        <Switch
                            checked={deleteChartsPossible}
                            onChange={handleDeleteChartsPossible}
                            value="deleteCharts"
                            color="secondary"
                        />
                    </FormControl>
                </td>
            </div>
        )
    }
}

export default withStyles(styles)(DeleteChartsSettings)