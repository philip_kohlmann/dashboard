import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import IconButton from "@material-ui/core/IconButton/IconButton";
import PersonAdd from "@material-ui/icons/PersonAdd";
import React from "react";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    formControlNewUser: {
        margin: theme.spacing.unit,
        minWidth: 50,
    }
});

class AddNewUser extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, openNewUserForm} = this.props

        return(
            <div>
                <td width="400%">
                    {language === "English" ?
                        <Typography style={{marginLeft: "4%"}}>
                            Add a new User
                        </Typography> :
                        language === "German" ?
                            <Typography style={{marginLeft: "4%"}}>
                                Fügen Sie einen neuen Benutzer hinzu
                            </Typography>
                            : null
                    }
                </td>
                <td align="right">
                    <FormControl className={classes.formControlNewUser}>
                        <IconButton
                            color="secondary"
                            onClick={openNewUserForm}
                        >
                            <PersonAdd style={{ minWidth: '30px', minHeight: '30px'}} color="secondary"/>
                        </IconButton>
                    </FormControl>
                </td>
            </div>
        )
    }
}

export default withStyles(styles)(AddNewUser)