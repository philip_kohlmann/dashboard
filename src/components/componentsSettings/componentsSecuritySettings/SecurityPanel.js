import Typography from "@material-ui/core/Typography/Typography";
import Paper from "@material-ui/core/Paper/Paper";
import OpenSelectOptionSecurity from "../../../containers/Settings/OpenSelectOptionsSecurity";
import React from "react";


class SecurityPanel extends React.Component{

    render(){

        const {language} = this.props

        return(

            <div>
                <div style={{marginBottom: "10px",marginLeft:"30%"}}>
                    <br/>
                    {language === "English" ?
                        <Typography variant="subheading">
                            Security
                        </Typography> :
                        language === "German" ?
                            <Typography variant="subheading">
                                Sicherheit
                            </Typography>
                            : null
                    }
                </div>
                <Paper style={{width:"40%",  marginLeft:"30%"}}>
                    <Typography>
                        <OpenSelectOptionSecurity/>
                    </Typography>
                </Paper>
            </div>
        )
    }
}

export default SecurityPanel
