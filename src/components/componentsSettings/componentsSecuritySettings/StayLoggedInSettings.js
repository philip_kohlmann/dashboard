import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import Switch from "@material-ui/core/Switch/Switch";
import React from "react";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    formControlLoggedIn: {
        margin: theme.spacing.unit,
        minWidth: 50,
    }
});

class StayLoggedInSettings extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, stayLoggedIn, onChange} = this.props

        return(
            <div>
                <td width="400%">
                    {language === "English" ?
                        <Typography style={{marginLeft: "4%"}}>
                            Stay Logged In
                        </Typography> :
                        language === "German" ?
                            <Typography style={{marginLeft: "4%"}}>
                                Eingeloggt bleiben
                            </Typography>
                            : null
                    }
                </td>
                <td>
                    <FormControl className={classes.formControlLoggedIn}>
                        <Switch
                            checked={stayLoggedIn}
                            onChange={onChange}
                            value="LoggedIn"
                            color="secondary"
                        />
                    </FormControl>
                </td>
            </div>
        )
    }
}

export default withStyles(styles)(StayLoggedInSettings)