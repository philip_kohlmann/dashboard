import Typography from "@material-ui/core/Typography/Typography";
import Paper from "@material-ui/core/Paper/Paper";
import OpenSelectOption from "../../../containers/Settings/OpenSelectOptions";
import React from "react";


class DisplayPanel extends React.Component{

    render(){

        const {language} = this.props

        return(
            <div>
                <div style={{marginBottom: "10px",marginLeft:"30%"}}>
                    <br/>
                    {language === "English" ?
                        <Typography variant="subheading">
                            Display
                        </Typography> :
                        language === "German" ?
                            <Typography variant="subheading">
                                Darstellung & Anzeige
                            </Typography>
                            : null
                    }
                </div>
                <Paper style={{width:"40%",  marginLeft:"30%"}}>
                    <OpenSelectOption/>
                </Paper>
            </div>
        )
    }
}

export default DisplayPanel