import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import { withStyles } from '@material-ui/core/styles';
import React from "react";

const styles = theme => ({
    formControlFontstyle:{
        margin: theme.spacing.unit,
        minWidth: 150,
    }
});

class FontstyleSelect extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, fontstyleOpen, handleCloseFontstyle, handleOpenFontstyle, fontstyle, handleChange} = this.props

        return(
            <React.Fragment>
                <td>
                    {language === "English" ?
                        <Typography style={{marginLeft: "8%"}}>
                            Change the Fontstyle
                        </Typography> :
                        language === "German" ?
                            <Typography style={{marginLeft: "8%"}}>
                                Ändern Sie die Schriftart
                            </Typography>
                            : null
                    }
                </td>
                <td>
                </td>
                <td align="right">
                    <FormControl className={classes.formControlFontstyle}>
                        {language === "English" ?
                            <InputLabel htmlFor="demo-controlled-open-select">Fontstyle</InputLabel>:
                            language === "German" ?
                                <InputLabel htmlFor="demo-controlled-open-select">Schriftart</InputLabel>:
                                null}
                        <Select
                            open={fontstyleOpen}
                            onClose={handleCloseFontstyle}
                            onOpen={handleOpenFontstyle}
                            value={fontstyle}
                            onChange={handleChange}
                            inputProps={{
                                name: 'fontstyle',
                                id: 'demo-controlled-open-select',
                            }}
                        >
                            <MenuItem value={"Helvetica Neue"}>Helvetica</MenuItem>
                            <MenuItem value={"Segoe UI"}>Segoe UI</MenuItem>
                            <MenuItem value={"BlinkMacSystemFont"}>BlinkMacSystemFont</MenuItem>
                            <MenuItem value={"Arial"}>Arial</MenuItem>
                        </Select>
                    </FormControl>
                </td>
            </React.Fragment>
        )
    }
}

export default withStyles(styles)(FontstyleSelect)
