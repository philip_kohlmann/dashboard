import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    formControlSecondary: {
        margin: theme.spacing.unit,
        minWidth: 150,
    }
});

class SecondaryColorSelect extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, secondaryColorOpen, handleCloseSecondaryColor, handleOpenSecondaryColor, secondaryColor, handleChange} = this.props

        return(
            <td align="right">
            <FormControl className={classes.formControlSecondary}>
                {language === "English" ?
                    <InputLabel htmlFor="demo-controlled-open-select-secondary">Secondary-Color</InputLabel> :
                    language === "German" ?
                        <InputLabel htmlFor="demo-controlled-open-select-secondary">Sekundärfarbe</InputLabel> :
                        null}
                <Select
                    open={secondaryColorOpen}
                    onClose={handleCloseSecondaryColor}
                    onOpen={handleOpenSecondaryColor}
                    value={secondaryColor.name}
                    onChange={handleChange}
                    inputProps={{
                        name: 'secondary',
                        id: 'demo-controlled-open-select-secondary',
                    }}
                >
                    {language === "English" ?
                        <MenuItem value={"Red"}>Red</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Red"}>Rot</MenuItem> :
                            null}
                    {language === "English" ?
                        <MenuItem value={"Green"}>Green</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Green"}>Grün</MenuItem> :
                            null}
                    {language === "English" ?
                        <MenuItem value={"Blue"}>Blue</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Blue"}>Blau</MenuItem> :
                            null}
                    {language === "English" ?
                        <MenuItem value={"Yellow"}>Yellow</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Yellow"}>Gelb</MenuItem> :
                            null}
                    {language === "English" ?
                        <MenuItem value={"Amber"}>Amber</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Amber"}>Bernstein</MenuItem> :
                            null}
                </Select>
            </FormControl>
            </td>
        )
    }
}

export default withStyles(styles)(SecondaryColorSelect)