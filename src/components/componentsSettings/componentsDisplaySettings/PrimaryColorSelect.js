import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";
import React from "react";
import Typography from "@material-ui/core/Typography/Typography";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    formControlPrimary: {
        marginTop: 10,
        margin: theme.spacing.unit,
        minWidth: 150,
    }
});

class PrimaryColorSelect extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, primaryColorOpen, handleClosePrimaryColor, handleOpenPrimaryColor, primaryColor, handleChange} = this.props

        return(
            <React.Fragment>
            <td>
                {language === "English" ?
                    <Typography style={{marginLeft: "8%"}}>
                        Change the Main Color Theme
                    </Typography> :
                    language === "German" ?
                        <Typography style={{marginLeft: "8%"}}>
                            Änderen Sie das Farbenthema
                        </Typography>
                        : null
                }
            </td>
            <td  align="right">
            <FormControl className={classes.formControlPrimary} >
                {language === "English" ?
                    <InputLabel htmlFor="demo-controlled-open-select-primary">Primary-Color</InputLabel> :
                    language === "German" ?
                        <InputLabel htmlFor="demo-controlled-open-select-primary">Hauptfarbe</InputLabel> :
                        null}
                <Select
                    open={primaryColorOpen}
                    onClose={handleClosePrimaryColor}
                    onOpen={handleOpenPrimaryColor}
                    value={primaryColor.name}
                    onChange={handleChange}
                    inputProps={{
                        name: 'primary',
                        id: 'demo-controlled-open-select-primary',
                    }}
                >
                    {language === "English" ?
                        <MenuItem value={"Red"}>Red</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Red"}>Rot</MenuItem> :
                            null}
                    {language === "English" ?
                        <MenuItem value={"Green"}>Green</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Green"}>Grün</MenuItem> :
                            null}
                    {language === "English" ?
                        <MenuItem value={"Blue"}>Blue</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Blue"}>Blau</MenuItem> :
                            null}
                    {language === "English" ?
                        <MenuItem value={"Yellow"}>Yellow</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Yellow"}>Gelb</MenuItem> :
                            null}
                    {language === "English" ?
                        <MenuItem value={"Amber"}>Amber</MenuItem> :
                        language === "German" ?
                            <MenuItem value={"Amber"}>Bernstein</MenuItem> :
                            null}
                </Select>
            </FormControl>
             </td>
            </React.Fragment>
        )
    }
}

export default withStyles(styles)(PrimaryColorSelect)