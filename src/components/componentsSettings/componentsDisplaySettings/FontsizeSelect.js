import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import React from "react";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    formControlFontsize: {
        margin: theme.spacing.unit,
        minWidth: 150
    }
});


class FontsizeSelect extends React.Component{

    render(){

        const { classes } = this.props;

        const {language, fontsizeOpen, handleCloseFontsize, handleOpenFontsize, fontsize, handleChange} = this.props

        return(
            <React.Fragment>
                <td>
                    {language === "English" ?
                        <Typography style={{marginLeft: "8%"}}>
                            Change the Fontsize
                        </Typography> :
                        language === "German" ?
                            <Typography style={{marginLeft: "8%"}}>
                                Ändern Sie die Schriftgröße
                            </Typography>
                            : null
                    }
                </td>
                <td>
                </td>
                <td align="right">
                    <FormControl className={classes.formControlFontsize}>
                        {language === "English" ?
                            <InputLabel htmlFor="demo-controlled-open-select">Fontsize</InputLabel>:
                            language === "German" ?
                                <InputLabel htmlFor="demo-controlled-open-select">Schriftgröße</InputLabel>:
                                null}
                        <Select
                            open={fontsizeOpen}
                            onClose={handleCloseFontsize}
                            onOpen={handleOpenFontsize}
                            value={fontsize}
                            onChange={handleChange}
                            inputProps={{
                                name: 'fontsize',
                                id: 'demo-controlled-open-select',
                            }}
                        >
                            <MenuItem value={8}>8</MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={12}>12</MenuItem>
                            <MenuItem value={14}>14</MenuItem>
                            <MenuItem value={16}>16</MenuItem>
                        </Select>
                    </FormControl>
                </td>
            </React.Fragment>

        )
    }
}

export default withStyles(styles)(FontsizeSelect)
