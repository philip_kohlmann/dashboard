import Drawer from "@material-ui/core/Drawer/Drawer";
import IconButton from "@material-ui/core/IconButton/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Divider from "@material-ui/core/Divider/Divider";
import MainListItemsGerman from "../../drawerItems/listItemsGerman";
import React from "react";
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";


const styles = theme => ({
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    drawerPaper: {
        position: 'fixed',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        height: drawerHeight,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9,
        },
    },
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: -10,
    },
});

const drawerWidth = 240;
const drawerHeight = 1322;


class DrawerDashboardGerman extends React.Component{

    render(){

        const { classes } = this.props;

        const { isDrawerOpen, handleDrawerClose } = this.props

        return(
                        <Drawer
                            variant="permanent"
                            classes={{
                                paper: classNames(classes.drawerPaper, !isDrawerOpen && classes.drawerPaperClose),
                            }}
                            open={isDrawerOpen}
                        >
                            <div className={classes.toolbarIcon}>
                                <IconButton onClick={handleDrawerClose}>
                                    <ChevronLeftIcon />
                                </IconButton>
                            </div>
                            <Divider />
                            <MuiThemeProvider>
                                <MainListItemsGerman/>
                            </MuiThemeProvider>
                            <Divider />
                        </Drawer>

        )
    }
}

export default withStyles(styles)(DrawerDashboardGerman)
