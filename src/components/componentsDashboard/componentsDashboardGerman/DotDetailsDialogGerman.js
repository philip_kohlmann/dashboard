import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Typography from "@material-ui/core/Typography/Typography";
import React from "react";


class DotDetailsDialogGerman extends React.Component{

    render(){

        const {closeDotDetailsDialog, dotDetailsDialogOpen, dotDetails} = this.props

        return(
            <Dialog
                fullWidth
                style={{maxHeight: "1000px", overflow:"auto", whiteSpace: "pre-line"}}
                open={dotDetailsDialogOpen}
                onClose={closeDotDetailsDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Details</DialogTitle>
                <DialogContent>
                    <Typography>
                        {dotDetails}
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeDotDetailsDialog} color="primary">
                        Schließen
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default DotDetailsDialogGerman

