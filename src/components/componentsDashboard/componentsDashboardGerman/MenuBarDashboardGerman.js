import classNames from "classnames";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import IconButton from "@material-ui/core/IconButton/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import AppBar from "@material-ui/core/AppBar/AppBar";
import React from "react";
import Typography from "@material-ui/core/Typography/Typography";
import logo from "../../../dzhyp.jpg";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import Switch from "@material-ui/core/Switch/Switch";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import { withStyles } from '@material-ui/core/styles';
import {Link} from "react-router-dom";
import Avatar from '@material-ui/core/Avatar';



const styles = theme => ({
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    appBar: {
        position:"fixed",
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
        minWidth: 50,
        minHeight: 50
    },
    menuButtonHidden: {
        display: 'none',
    },
    logo: {
        flexGrow: 0.55
    },
    title: {
        flexGrow: 0.45,
    },
    avatar: {
        margin: 10,
        backgroundColor: '#FF6600',
    }
});

const drawerWidth = 240;

class MenuBarDashboardGerman extends React.Component{

    getCapitalizedFirstLetter(name){
        return name.charAt(0).toUpperCase()
    }

    logOut(){
        this.props.logOut()
        this.props.handleDrawerChange(0)
    }

    render(){

        const { classes } = this.props;

        const {isDrawerOpen, userName, handleDrawerOpen, loggedIn,
            showChartForm, closeChartForm, tooltipsEnabled, ChartForm, nameGerman, handleDrawerChange} = this.props

        return(
            <AppBar
                position="absolute"
                className={classNames(classes.appBar, isDrawerOpen && classes.appBarShift)}
            >
                <Toolbar disableGutters={!isDrawerOpen} className={classes.toolbar}>
                    <table width="110%">
                        <tbody>
                        <tr>
                        <td width="5%">
                    <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        onClick={handleDrawerOpen}
                        className={classNames(
                            classes.menuButton,
                            isDrawerOpen && classes.menuButtonHidden,
                        )}
                    >
                        <MenuIcon style={{ minWidth: '40px', minHeight: '50px'}} />
                    </IconButton>
                    </td>
                    <td width="35%">
                    <Typography variant="title" color="inherit" noWrap className={classes.title}>
                        Dashboard-{nameGerman}
                    </Typography>
                        </td>
                        <td width="50%">
                    <div className={classes.logo}>
                        <Link to= '/' style={{ textDecoration: 'none' }} onClick={() => handleDrawerChange(0)}>
                            <img src={logo} alt="DzHypLogo" style={{ maxWidth: '300px', maxHeight: '60px'}} />
                        </Link>
                    </div>
                        </td>
                        <td>
                    { tooltipsEnabled ?
                    <Tooltip title="Ausloggen">
                        <Switch
                            checked={loggedIn}
                            onChange={() => this.logOut()}
                            value="Logout"
                            color="secondary"
                        />
                    </Tooltip>
                    : <Switch
                            checked={loggedIn}
                            onChange={() => this.logOut()}
                            value="Logout"
                            color="secondary"
                        />}
                        </td>
                            <td>
                                {tooltipsEnabled ?
                                    <Link to='/settings' style={{ textDecoration: 'none' }} onClick={() => handleDrawerChange(4)}>
                                        <Tooltip title={userName}>
                                            <Avatar className={classes.avatar}>{this.getCapitalizedFirstLetter(userName)}</Avatar>
                                        </Tooltip>
                                    </Link> :
                                    <Link to='/settings' style={{ textDecoration: 'none' }} onClick={() => handleDrawerChange(4)}>

                                        <Avatar className={classes.avatar}>{this.getCapitalizedFirstLetter(userName)}</Avatar>

                                    </Link>}
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    <Dialog
                        open={showChartForm}
                        onClose={closeChartForm}
                        fullWidth
                        maxWidth="xs"
                    >
                        <DialogTitle>
                            Erstellen Sie eine Grafik
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                Bitte spezifizieren Sie die Art von Grafik, die Sie erstellen wollen
                            </DialogContentText>
                            <ChartForm/>
                        </DialogContent>
                    </Dialog>
                </Toolbar>
            </AppBar>

        )
    }
}

export default withStyles(styles)(MenuBarDashboardGerman)