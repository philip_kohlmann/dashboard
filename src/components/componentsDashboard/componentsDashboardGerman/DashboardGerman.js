import MenuBarDashboardGerman from "./MenuBarDashboardGerman"
import DrawerDashboardGerman from "./DrawerDashboardGerman"
import ChartsDashboardGerman from "./ChartsDashboardGerman"
import EditDialogGerman from "./EditDialogGerman"
import CommentDialogGerman from "./CommentDialogGerman"
import NoAdminAccessDialogGerman from "./NoAdminAccessDialogGerman"
import DotDetailsDialogGerman from "./DotDetailsDialogGerman"
import React from "react"
class DashboardGerman extends React.Component {

    render(){

        const {isDrawerOpen, handleDrawerOpen, handleDrawerClose, tooltipsEnabled, loggedIn, logOut, loginFormOpen, closeLoginForm, openLoginForm, noAccess, closeNoAccess, openChartForm,
            showChartForm, closeChartForm, loggingIn, Chart, Table, chartsExist, tablesExist, ChartForm, nameGerman, handleDrawerChange, userName,
            tableCommentOpen, closeTableComment, comment, clearComment, handleChange, handleCommentEdit, chartEditOpen, type, closeChartEdit,
            handleEditChange, editChart, handleChartCommentEdit, typeComment, noAdminAccessOpen, closeNoAdminAccess,
            dotDetailsDialogOpen, closeDotDetailsDialog, dotDetails, title, color, table} = this.props

        return(
            <div>
                <MenuBarDashboardGerman isDrawerOpen={isDrawerOpen} handleDrawerOpen={handleDrawerOpen} loggedIn={loggedIn} handleDrawerChange={handleDrawerChange} userName={userName}
                                         logOut={logOut} loginFormOpen={loginFormOpen} closeLoginForm={closeLoginForm} openLoginForm={openLoginForm}
                                        noAccess={noAccess} closeNoAccess={closeNoAccess} ChartForm={ChartForm} showChartForm={showChartForm}
                                        closeChartForm={closeChartForm} nameGerman={nameGerman} tooltipsEnabled={tooltipsEnabled}/>
                <DotDetailsDialogGerman dotDetailsDialogOpen={dotDetailsDialogOpen} closeDotDetailsDialog={closeDotDetailsDialog} dotDetails={dotDetails}/>
                <NoAdminAccessDialogGerman noAdminAccessOpen={noAdminAccessOpen} closeNoAdminAccess={closeNoAdminAccess}/>
                <EditDialogGerman chartEditOpen={chartEditOpen} closeChartEdit={closeChartEdit} type={type} handleEditChange={handleEditChange}
                                   editChart={editChart}  title={title} handleChange={handleChange} color={color} table={table}/>
                <CommentDialogGerman tableCommentOpen={tableCommentOpen} closeTableComment={closeTableComment} comment={comment} handleChange={handleChange}
                                      clearComment={clearComment} typeComment={typeComment} handleCommentEdit={handleCommentEdit} handleChartCommentEdit={handleChartCommentEdit}/>
                <DrawerDashboardGerman isDrawerOpen={isDrawerOpen} handleDrawerClose={handleDrawerClose} userName={userName} tooltipsEnabled={tooltipsEnabled}/>
                <ChartsDashboardGerman loggingIn={loggingIn} Chart={Chart} Table={Table} isDrawerOpen={isDrawerOpen} chartsExist={chartsExist}
                                       tablesExist={tablesExist} openChartForm={openChartForm} tooltipsEnabled={tooltipsEnabled}/>
            </div>
        )
    }

}

export default DashboardGerman
