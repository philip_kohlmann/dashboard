import React from "react";
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    }
});
class CircularProgress extends React.Component{

    render(){

        const { classes } = this.props;

        const {loggingIn} = this.props

        return(
            <div>
            {this.props.loggingIn === true?
                <td align="left">
                    <div>
                        <CircularProgress className={classes.progress} size={50} color="secondary"  />
                    </div>
                </td>
                : null}
            </div>
        )
    }
}

export default withStyles(styles)(CircularProgress)