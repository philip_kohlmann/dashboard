import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Typography from "@material-ui/core/Typography/Typography";
import React from "react";


class NoAdminAccessDialogEnglish extends React.Component{

    render(){

        const {closeNoAdminAccess, noAdminAccessOpen} = this.props

        return(
            <Dialog
                fullWidth
                open={noAdminAccessOpen}
                onClose={closeNoAdminAccess}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">No Admin Access</DialogTitle>
                <DialogContent>
                    <Typography>
                        No Admin Access - Get Admin Access to continue to Settings
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeNoAdminAccess} color="primary">
                        Okay
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default NoAdminAccessDialogEnglish

