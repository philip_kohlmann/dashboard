import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import React from "react";


class CommentDialogEnglish extends React.Component{

    render(){

        const {tableCommentOpen, closeTableComment, comment, handleChange, clearComment,
                typeComment, handleCommentEdit, handleChartCommentEdit} = this.props

        return(
            <Dialog
                fullWidth
                open={tableCommentOpen}
                onClose={closeTableComment}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Comment</DialogTitle>
                <DialogContent>
                    <TextField
                        name = "comment"
                        id="outlined-full-width"
                        value={comment}
                        onChange={handleChange}
                        multiline
                        rows="10"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={clearComment} color="primary">
                        Delete
                    </Button>
                    {typeComment === "table" ?
                        <Button onClick={handleCommentEdit} color="primary" autoFocus>
                            Confirm
                        </Button>
                        :<Button onClick={handleChartCommentEdit} color="primary" autoFocus>
                            Confirm
                        </Button>}
                </DialogActions>
            </Dialog>
        )
    }
}

export default CommentDialogEnglish

