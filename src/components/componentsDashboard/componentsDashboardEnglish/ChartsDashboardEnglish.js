import Typography from "@material-ui/core/Typography/Typography";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import React from "react";
import { withStyles } from '@material-ui/core/styles';
import classNames from "classnames";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Add from "@material-ui/icons/AddCircle";

const styles = theme => ({
    appBarSpacer: theme.mixins.toolbar,

    containers:{
        width:"93.5%",
        marginBottom: 40,
        marginTop:80,
        marginLeft: 100,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        })
    },
    drawerPaperOpen:{
        width: `calc(100% - ${drawerWidth + 70}px)`,
        marginTop:80,
        marginLeft: 270,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        })

    }
});

const drawerWidth = 240;


class ChartsDashboardEnglish extends React.Component{

    render(){

        const { classes } = this.props;

        const {loggingIn, Chart, Table, isDrawerOpen, chartsExist, tablesExist, tooltipsEnabled, openChartForm } = this.props

        return(
            <div className={ classNames(classes.containers, isDrawerOpen && classes.drawerPaperOpen)} >
                <table width = "100%">
                    <tbody>
                    <tr>
                        <div>
                        <td>
                            <Typography variant="display1" gutterBottom>
                                Charts
                            </Typography>
                        </td>
                            <td>
                            {tooltipsEnabled ?
                                <Tooltip title="Add Chart">
                                    <IconButton
                                        color="secondary"
                                        onClick={() => openChartForm("chart")}
                                        style={{marginBottom:15}}
                                    >
                                        <Add style={{ minWidth: '35px', minHeight: '35px'}} color="secondary"/>
                                    </IconButton>
                                </Tooltip>
                                : <IconButton
                                    color="secondary"
                                    onClick={() => openChartForm("chart")}
                                    style={{marginBottom:15}}
                                >
                                    <Add style={{ minWidth: '35px', minHeight: '35px'}} color="secondary"/>
                                </IconButton> }
                        </td>
                        </div>
                        {loggingIn === true?
                            <td align="left">
                                <div>
                                    <CircularProgress loggingIn={loggingIn} size={50} color="secondary"  />
                                </div>
                            </td>
                            : null}
                    </tr>
                    </tbody>
                </table>

                <div>
                    {Chart}
                </div>
                        <table>
                        <tbody>
                        <tr>
                        <td>
                            <Typography variant="display1" gutterBottom>
                                Tables
                            </Typography>
                        </td>
                        <td>
                            {tooltipsEnabled ?
                                <Tooltip title="Add Table">
                                    <IconButton
                                        color="secondary"
                                        onClick={() => openChartForm("table")}
                                        style={{marginBottom:15}}
                                    >
                                        <Add style={{ minWidth: '35px', minHeight: '35px' }} color="secondary"/>
                                    </IconButton>
                                </Tooltip>
                                : <IconButton
                                    color="secondary"
                                    onClick={() => openChartForm("table")}
                                    style={{marginBottom:15}}
                                >
                                    <Add style={{ minWidth: '35px', minHeight: '35px'}} color="secondary"/>
                                </IconButton> }
                        </td>
                        </tr>
                        </tbody>
                        </table>
            <div>
                {Table}
            </div>
        </div>
        )
    }
}

export default withStyles(styles)(ChartsDashboardEnglish)