import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import React from "react";
import TextField from "@material-ui/core/TextField/TextField";


class EditDialogEnglish extends React.Component{

    render(){

        const {chartEditOpen, closeChartEdit, type, handleEditChange, editChart, handleChange, title, color, table} = this.props

        return(

            <Dialog
                fullWidth
                open={chartEditOpen}
                onClose={closeChartEdit}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                {table === true ?
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">Edit Table</DialogTitle>
                        <DialogContent>
                            <table width="100%">
                                <tbody>
                                <tr>
                                    <td>
                                        <Typography style={{marginTop:20, align:"center"}}>
                                            Change the Table-Title
                                        </Typography>
                                    </td>
                                    <td>
                                        <TextField
                                            id="outlined-full-width"
                                            value={title}
                                            onChange={handleChange}
                                            multiline
                                            rows="2"
                                            fullWidth
                                        />
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={editChart} color="primary" autoFocus disabled={!title}>
                                Confirm
                            </Button>
                        </DialogActions>
                    </React.Fragment>
                    :
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">Edit Chart</DialogTitle>
                        <DialogContent>
                            <table width="100%">
                                <tbody>
                                <tr>
                                    <td>
                                        <Typography style={{marginTop:20, align:"center"}}>
                                           Change the Chart-Title
                                        </Typography>
                                    </td>
                                    <td>
                                        <TextField
                                            id="outlined-full-width"
                                            value={title}
                                            onChange={handleChange}
                                            multiline
                                            rows="2"
                                            fullWidth
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <Typography style={{marginTop:20, align:"center"}}>
                                            Change the Chart-Type
                                        </Typography>
                                    </td>
                                    <td width="50%">
                                        <FormControl fullWidth>
                                            <div>
                                                <InputLabel htmlFor="type">
                                                    Chart-Typ
                                                </InputLabel>
                                                <Select
                                                    value={type}
                                                    onChange={handleEditChange("type")}
                                                    fullWidth
                                                >
                                                    <MenuItem key={"Pie"} value={"Pie"}>
                                                        Pie
                                                    </MenuItem>
                                                    <MenuItem key={"Bar"} value={"Bar"}>
                                                        Bar
                                                    </MenuItem>
                                                    <MenuItem key={"Line"} value={"Line"}>
                                                        Line
                                                    </MenuItem>
                                                </Select>
                                            </div>
                                        </FormControl>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <Typography style={{marginTop:20, align:"center"}}>
                                            Change the Chart Color
                                        </Typography>
                                    </td>
                                    <td width="50%">
                                        <FormControl fullWidth>
                                            <div>
                                                <InputLabel htmlFor="type">
                                                    Chart-Color
                                                </InputLabel>
                                                <Select
                                                    value={color}
                                                    onChange={handleEditChange("color")}
                                                    fullWidth
                                                >
                                                    <MenuItem key={"Green"} value={"Green"}>
                                                        Green
                                                    </MenuItem>
                                                    <MenuItem key={"Blue"} value={"Blue"}>
                                                        Blue
                                                    </MenuItem>
                                                    <MenuItem key={"Red"} value={"Red"}>
                                                        Red
                                                    </MenuItem>
                                                </Select>
                                            </div>
                                        </FormControl>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={editChart} color="primary" autoFocus disabled={!title || !color || !type}>
                                Confirm
                            </Button>
                        </DialogActions>
                    </React.Fragment>
                }
            </Dialog>
        )
    }
}

export default EditDialogEnglish
