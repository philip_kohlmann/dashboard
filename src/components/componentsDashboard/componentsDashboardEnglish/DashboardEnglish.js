import MenuBarDashboardEnglish from "./MenuBarDashboardEnglish"
import DrawerDashboardEnglish from "./DrawerDashboardEnglish"
import ChartsDashboardEnglish from "./ChartsDashboardEnglish"
import EditDialogEnglish from "./EditDialogEnglish"
import CommentDialogEnglish from "./CommentDialogEnglish"
import NoAdminAccessDialogEnglish from "./NoAdminAccessDialogEnglish"
import DotDetailsDialogEnglish from "./DotDetailsDialogEnglish"
import React from "react"


class DashboardEnglish extends React.Component {

    render(){

        const {isDrawerOpen, handleDrawerOpen, handleDrawerClose, tooltipsEnabled, loggedIn, logOut, loginFormOpen, closeLoginForm, openLoginForm, noAccess, closeNoAccess, openChartForm,
            showChartForm, closeChartForm, loggingIn, Chart, Table, chartsExist, tablesExist, ChartForm, nameEnglish, handleDrawerChange,
            userName,  tableCommentOpen, closeTableComment, comment, clearComment, handleChange, handleCommentEdit, chartEditOpen, type,
            closeChartEdit, handleEditChange, editChart, handleChartCommentEdit, typeComment, noAdminAccessOpen, closeNoAdminAccess,
            dotDetailsDialogOpen, closeDotDetailsDialog, dotDetails, title, color, table} = this.props


        return(
            <div>
                <MenuBarDashboardEnglish isDrawerOpen={isDrawerOpen} handleDrawerOpen={handleDrawerOpen} loggedIn={loggedIn} handleDrawerChange={handleDrawerChange}
                                         logOut={logOut} loginFormOpen={loginFormOpen} closeLoginForm={closeLoginForm} userName={userName} tooltipsEnabled={tooltipsEnabled}
                                         openLoginForm={openLoginForm} noAccess={noAccess} closeNoAccess={closeNoAccess} ChartForm={ChartForm}
                                         showChartForm={showChartForm} closeChartForm={closeChartForm} nameEnglish={nameEnglish}/>
                <DotDetailsDialogEnglish dotDetailsDialogOpen={dotDetailsDialogOpen} closeDotDetailsDialog={closeDotDetailsDialog} dotDetails={dotDetails}/>
                <NoAdminAccessDialogEnglish noAdminAccessOpen={noAdminAccessOpen} closeNoAdminAccess={closeNoAdminAccess}/>
            <EditDialogEnglish chartEditOpen={chartEditOpen} closeChartEdit={closeChartEdit} type={type} handleEditChange={handleEditChange}
                               editChart={editChart}  title={title} handleChange={handleChange} color={color} table={table}/>
             <CommentDialogEnglish tableCommentOpen={tableCommentOpen} closeTableComment={closeTableComment} comment={comment} handleChange={handleChange}
                                   clearComment={clearComment} typeComment={typeComment} handleCommentEdit={handleCommentEdit} handleChartCommentEdit={handleChartCommentEdit}/>
                <DrawerDashboardEnglish isDrawerOpen={isDrawerOpen} handleDrawerClose={handleDrawerClose} />
                <ChartsDashboardEnglish loggingIn={loggingIn} Chart={Chart} Table={Table} isDrawerOpen={isDrawerOpen} chartsExist={chartsExist}
                                        tablesExist={tablesExist} openChartForm={openChartForm} tooltipsEnabled={tooltipsEnabled}/>
            </div>
        )
    }

}

export default DashboardEnglish