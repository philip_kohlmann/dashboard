import TextField from "@material-ui/core/TextField/TextField";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Input from "@material-ui/core/Input/Input";
import InputAdornment from "@material-ui/core/InputAdornment/InputAdornment";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Button from "@material-ui/core/Button/Button";
import React from "react";

class LoginFormGerman extends React.Component{

    render(){

        const {handleChangeUsername, handleChangePassword, showPassword, password, username, handleSubmit, handleClickShowPassword, handleMouseDownPassword} = this.props

        return(
            <div>
                <TextField
                    label="Benutzername"
                    value={username}
                    onChange={handleChangeUsername}
                    margin="normal"
                    fullWidth
                />
                <br/>
                <FormControl fullWidth>
                    <InputLabel htmlFor="adornment-password">Passwort</InputLabel>
                    <Input
                        id="adornment-password"
                        type={showPassword ? 'text' : 'password'}
                        value={password}
                        onChange={handleChangePassword}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="Toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <Button
                    style={{marginTop: 20}}
                    color="primary"
                    variant="raised"
                    onClick={handleSubmit}
                    disabled={!username || !password}>
                    Einloggen
                </Button>
            </div>
        )
    }
}

export default LoginFormGerman