import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Home from '@material-ui/icons/Home';
import Settings from '@material-ui/icons/Settings';
import Downtime from '@material-ui/icons/TrendingDown';
import Timeout from '@material-ui/icons/AlarmOff';
import Requests from '@material-ui/icons/ArrowRightAlt';
import { Link } from 'react-router-dom'
import { MenuItem } from 'material-ui/Menu';
import List from "@material-ui/core/List/List";
import {handleDrawerChange} from "../../actions/dataActions";
import {connect} from "react-redux"
import { withStyles } from '@material-ui/core/styles';
import amber from '@material-ui/core/colors/amber'

const styles = theme => ({
    root: {
        backgroundColor: amber[900],
        opacity: 0.7
    },
});

class mainListItemsGerman extends React.Component {

    drawerChange(value){
        this.props.handleDrawerChange(value)
    }

    render() {

        const { classes } = this.props;


        return (
            <List style={{paddingTop:0, paddingBottom:0}}>
                <Link to='/' style={{textDecoration: 'none'}}>
                    <div style={{height:50}} className={this.props.drawerValue === 0 && classes.root}>
                        <ListItem button onClick={() => this.drawerChange(0)} >
                            <ListItemIcon>
                                <Home/>
                            </ListItemIcon>
                            <ListItemText primary="Start" />
                        </ListItem>
                    </div>
                </Link>
                <Link to='/requests' style={{textDecoration: 'none'}}>
                    <div style={{height:50}} className={this.props.drawerValue === 1 && classes.root}>
                        <ListItem button onClick={() => this.drawerChange(1)}>
                            <ListItemIcon>
                                <Requests/>
                            </ListItemIcon>
                            <ListItemText primary="Anfragen" />
                        </ListItem>
                    </div>
                </Link>
                <Link to='/downtime' style={{textDecoration: 'none'}}>
                    <div style={{height:50}} className={this.props.drawerValue === 2 && classes.root}>
                        <ListItem button onClick={() => this.drawerChange(2)}>
                            <ListItemIcon>
                                <Downtime/>
                            </ListItemIcon>
                            <ListItemText primary="Ausfallzeit" />
                        </ListItem>
                    </div>
                </Link>
                <Link to='/timeouts' style={{textDecoration: 'none'}}>
                    <div style={{height:50}} className={this.props.drawerValue === 3 && classes.root}>
                        <ListItem button onClick={() => this.drawerChange(3)}>
                            <ListItemIcon>
                                <Timeout/>
                            </ListItemIcon>
                            <ListItemText primary="Zeitüberschreitungen" />
                        </ListItem>
                    </div>
                </Link>
                <Link to='/settings' style={{textDecoration: 'none'}}>
                    <div style={{height:50}} className={this.props.drawerValue === 4 && classes.root}>
                        <ListItem button onClick={() => this.drawerChange(4)}>
                            <ListItemIcon>
                                <Settings/>
                            </ListItemIcon>
                            <ListItemText primary="Einstellungen" />
                        </ListItem>
                    </div>
                </Link>
            </List>
        )
    }
}

const mapStateToProps = state => ({
    drawerValue: state.allReducers.drawerIndex
})

export default withStyles(styles)(connect(mapStateToProps,{handleDrawerChange})(mainListItemsGerman));