import React from  'react'
import Button from "@material-ui/core/Button";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';

class NewUserFormEnglish extends React.Component{

    render(){

        const {username, password, handleChangeUsername, handleChangePassword, handleSubmit} = this.props

        return(
            <div>
                <FormControl fullWidth>
                <InputLabel htmlFor="username">Username</InputLabel>
                    <Input
                        id="username"
                        value={username}
                        onChange={handleChangeUsername}
                        margin="normal"
                        fullWidth
                    />
                </FormControl>
                <br/>
                <br/>
                <FormControl fullWidth>
                    <InputLabel htmlFor="password">Password</InputLabel>
                    <Input
                        id="adornment-password"
                        value={password}
                        onChange={handleChangePassword}
                        margin="normal"
                        fullWidth
                    />
                </FormControl>
                <Button
                    style={{marginTop: 20}}
                    color="primary"
                    variant="raised"
                    onClick={handleSubmit}
                    disabled={!username || !password}>
                    Create New User
                </Button>
            </div>

        )
    }
}

export default NewUserFormEnglish