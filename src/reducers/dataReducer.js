import {
    HANDLE_DRAWER_OPEN,
    HANDLE_DRAWER_CLOSE,
    LOGIN_PRESSED,
    ACCESS_GRANTED,
    ACCESS_DENIED,
    OPEN_LOGIN_FORM,
    CLOSE_LOGIN_FORM,
    LOG_OUT,
    OPEN_CHART_FORM,
    CLOSE_CHART_FORM,
    OPEN_CHART_FORM_TIMEOUTS,
    CLOSE_CHART_FORM_TIMEOUTS,
    OPEN_CHART_FORM_DOWNTIME,
    CLOSE_CHART_FORM_DOWNTIME,
    OPEN_CHART_FORM_REQUESTS,
    CLOSE_CHART_FORM_REQUESTS,
    CREATE_TIMEOUTS_CHART,
    CREATE_REQUESTS_CHART,
    CREATE_DOWNTIME_CHART,
    CREATE_DOWNTIME_TABLE,
    CREATE_REQUESTS_TABLE,
    CREATE_TIMEOUTS_TABLE,
    CREATE_HOME_CHART,
    FETCH_TIMEOUTS_DATA,
    FETCH_REQUESTS_DATA,
    FETCH_DOWNTIME_DATA,
    FETCH_HOME_DATA,
    FETCH_TABLE_DOWNTIME_DATA,
    FETCH_TABLE_TIMEOUTS_DATA,
    FETCH_TABLE_REQUESTS_DATA,
    FETCH_DATA,
    FETCH_TABLE_DATA_HOME,
    CREATE_HOME_TABLE,
    HANDLE_TAB_CHANGE,
    DELETE_CHART_HOME,
    DELETE_CHART_REQUESTS,
    DELETE_CHART_DOWNTIME,
    DELETE_CHART_TIMEOUTS,
    DELETE_TABLE_TIMEOUTS,
    DELETE_TABLE_REQUESTS,
    DELETE_TABLE_HOME,
    DELETE_TABLE_DOWNTIME,
    CREATE_HOME_CHART_TWO,
    CREATE_HOME_CHART_THREE,
    CREATE_HOME_CHART_FOUR,
    CREATE_HOME_TABLE_TWO,
    CREATE_HOME_TABLE_THREE,
    CREATE_HOME_TABLE_FOUR,
    DELETE_CHART_HOME_TWO,
    DELETE_CHART_HOME_THREE,
    DELETE_CHART_HOME_FOUR,
    DELETE_TABLE_HOME_TWO,
    DELETE_TABLE_HOME_THREE,
    DELETE_TABLE_HOME_FOUR,
    FETCH_HOME_DATA_TWO,
    FETCH_HOME_DATA_THREE,
    FETCH_HOME_DATA_FOUR,
    FETCH_TABLE_DATA_HOME_TWO,
    FETCH_TABLE_DATA_HOME_THREE,
    FETCH_TABLE_DATA_HOME_FOUR,
    OPEN_CHART_FORM_TWO,
    OPEN_CHART_FORM_THREE,
    OPEN_CHART_FORM_FOUR,
    OPEN_LOGIN_FORM_TWO,
    OPEN_LOGIN_FORM_THREE,
    OPEN_LOGIN_FORM_FOUR,
    CLOSE_LOGIN_FORM_TWO,
    CLOSE_CHART_FORM_TWO,
    CLOSE_LOGIN_FORM_THREE,
    CLOSE_CHART_FORM_THREE,
    CLOSE_LOGIN_FORM_FOUR,
    CLOSE_CHART_FORM_FOUR,
    TIMELINE_SELECTED,
    TIMELINE_SELECTED_TWO,
    TIMELINE_SELECTED_THREE,
    TIMELINE_SELECTED_FOUR,
    TIMELINE_SELECTED_TIMEOUTS,
    TIMELINE_SELECTED_DOWNTIME,
    TIMELINE_SELECTED_REQUESTS,
    SINGLE_DATES_SELECTED,
    SINGLE_DATES_SELECTED_TWO,
    SINGLE_DATES_SELECTED_THREE,
    SINGLE_DATES_SELECTED_FOUR,
    SINGLE_DATES_SELECTED_TIMEOUTS,
    SINGLE_DATES_SELECTED_DOWNTIME,
    SINGLE_DATES_SELECTED_REQUESTS,
    ADD_DATE,
    ADD_DATE_TWO,
    ADD_DATE_THREE,
    ADD_DATE_FOUR,
    ADD_DATE_TIMEOUTS,
    ADD_DATE_DOWNTIME,
    ADD_DATE_REQUESTS,
    DELETE_DATE,
    DELETE_DATE_TWO,
    DELETE_DATE_THREE,
    DELETE_DATE_FOUR,
    DELETE_DATE_DOWNTIME,
    DELETE_DATE_TIMEOUTS,
    DELETE_DATE_REQUESTS,
    HANDLE_CLOSE_FONTSTYLE,
    HANDLE_OPEN_FONTSIZE,
    HANDLE_CLOSE_FONTSIZE,
    HANDLE_OPEN_SECONDARY_COLOR,
    HANDLE_OPEN_PRIMARY_COLOR,
    HANDLE_OPEN_FONTSTYLE,
    HANDLE_CLOSE_SECONDARY_COLOR,
    HANDLE_CLOSE_PRIMARY_COLOR,
    HANDLE_TOGGLE_TOOLTIPS,
    HANDLE_TOGGLE_CHARTS,
    HANDLE_TOGGLE_LANGUAGE,
    HANDLE_TOGGLE_STAYLOGGEDIN,
    HANDLE_TOGGLE_DELETECHARTSPOSSIBLE,
    CHANGE_FONTSIZE,
    CHANGE_FONTSTYLE,
    CHANGE_COLOR_SECONDARY,
    CHANGE_COLOR_PRIMARY,
    RESET_SETTINGS,
    OPEN_NEW_USER_FORM,
    CLOSE_NEW_USER_FORM,
    ADD_USER,
    CHANGE_LANGUAGE, CHANGE_CHART_LIBRARY,
    HANDLE_FILTER,
    OPEN_HTTP_DIALOG,
    CLOSE_HTTP_DIALOG,
    HANDLE_DRAWER_CHANGE,
    OPEN_TABLE_COMMENT,
    CLOSE_TABLE_COMMENT,
    GET_COMMENT,
    EDIT_TABLE_COMMENT,
    OPEN_CHART_EDIT,
    CLOSE_CHART_EDIT,
    EDIT_CHART_COMMENT,
    OPEN_NO_ADMIN_ACCESS,
    CLOSE_NO_ADMIN_ACCESS,
    OPEN_DOT_DETAILS_DIALOG,
    CLOSE_DOT_DETAILS_DIALOG,
    GET_DOT_DETAILS,
    FETCH_REQUEST_DATA

} from "../actions/types";

const initialState = {
    itemsHome:[],
    itemsHomeTwo:[],
    itemsHomeThree:[],
    itemsHomeFour:[],
    itemsRequests:[],
    itemsDowntime:[],
    tablesDowntime:[],
    tablesTimeouts:[],
    tablesRequests:[],
    tablesHome:[],
    tablesHomeThree:[],
    tablesHomeFour:[],
    tablesHomeTwo:[],
    itemsTimeouts:[],
    data:{},
    requestData:{},
    drawerOpen: false,
    loggingIn: false,
    loginFormOpen: false,
    loginFormOpenTwo: false,
    loginFormOpenThree: false,
    loginFormOpenFour: false,
    chartFormOpen: false,
    chartFormOpenTwo: false,
    chartFormOpenThree: false,
    chartFormOpenFour: false,
    chartFormRequestsOpen: false,
    chartFormDowntimeOpen: false,
    chartFormTimeoutsOpen: false,
    isAccessGranted: false ,
    accessRestricted: false,
    tabsValue: 0,
    timelineSelected: false,
    singleDatesSelected: false,
    dates: [],
    dateCounter:0,
    timelineSelectedTwo: false,
    singleDatesSelectedTwo: false,
    datesTwo: [],
    dateCounterTwo:0,
    timelineSelectedThree: false,
    singleDatesSelectedThree: false,
    datesThree: [],
    dateCounterThree: 0,
    timelineSelectedFour: false,
    singleDatesSelectedFour: false,
    datesFour: [],
    dateCounterFour: 0,
    timelineSelectedRequests: false,
    singleDatesSelectedRequests: false,
    datesRequests: [],
    dateCounterRequests:0,
    timelineSelectedTimeouts: false,
    singleDatesSelectedTimeouts: false,
    datesTimeouts: [],
    dateCounterTimeouts:0,
    timelineSelectedDowntime: false,
    singleDatesSelectedDowntime: false,
    datesDowntime: [],
    dateCounterDowntime:0,
    primaryColorOpen: false,
    secondaryColorOpen: false,
    fontsizeOpen: false,
    fontstyleOpen: false,
    languageOpen: false,
    chartsOpen: false,
    tooltipsEnabled: true,
    stayLoggedIn: false,
    deleteChartsPossible: true,
    test: "Blue",
    colorPrimary: {
        value: "#0065B3",
        name: "Blue"
    },
    colorSecondary: {
        value: "#FF6600",
        name: "Amber"
    },
    fontsize: 14,
    fontstyle: "Segoe UI",
    newUserFormOpen: false,
    language: "German",
    timeout: null,
    library: "Recharts",
    show: false,
    httpOpen: false,
    drawerIndex: 0,
    userName:'Admin',
    formType: "",
    tableCommentOpen: false,
    comment: "",
    chartEditOpen: false,
    isAdminAccessGranted: false,
    noAdminAccessOpen: false,
    dotDetailDialogOpen: false,
    dotDetails: null,
    chartEditData: null

}


export default function(state = initialState, action){
    switch(action.type){
        case HANDLE_DRAWER_OPEN:
            return{
                ...state,
                drawerOpen: true,
            }
        case HANDLE_DRAWER_CLOSE:
            return{
                ...state,
                drawerOpen: false
            }
        case LOGIN_PRESSED:
            return{
                ...state,
                loggingIn: true,
                isAccessGranted: action.payload.accessGranted,
                isAdminAccessGranted: action.payload.adminAccessGranted,
                loginFormOpen: !action.payload.accessGranted,
                userName: action.userName.username
            };
        case ACCESS_GRANTED:
            return{
                ...state,
                loggingIn: false,
            }
        case ACCESS_DENIED:
            return{
                ...state,
                loggingIn: false
            }
        case OPEN_LOGIN_FORM:
        return{
            ...state,
            loginFormOpen: true
        }
        case CLOSE_LOGIN_FORM:
            return{
                ...state,
                loginFormOpen: false
            }
        case OPEN_CHART_FORM:
            return{
                ...state,
                chartFormOpen: true,
                formType: action.payload
            }
        case CLOSE_CHART_FORM:
            return{
                ...state,
                chartFormOpen: false,
                timelineSelected: false,
                singleDatesSelected: false,
                dates: [],
                dateCounter: 0
            }
        case OPEN_LOGIN_FORM_TWO:
            return{
                ...state,
                loginFormOpenTwo: true,

            }
        case CLOSE_LOGIN_FORM_TWO:
            return{
                ...state,
                loginFormOpenTwo: false
            }
        case OPEN_CHART_FORM_TWO:
            return{
                ...state,
                chartFormOpenTwo: true,
                formType: action.payload
            }
        case CLOSE_CHART_FORM_TWO:
            return{
                ...state,
                chartFormOpenTwo: false,
                timelineSelectedTwo: false,
                singleDatesSelectedTwo: false,
                dateCounterTwo: 0,
                datesTwo: []
            }
        case OPEN_LOGIN_FORM_THREE:
            return{
                ...state,
                loginFormOpenThree: true
            }
        case CLOSE_LOGIN_FORM_THREE:
            return{
                ...state,
                loginFormOpenThree: false
            }
        case OPEN_CHART_FORM_THREE:
            return{
                ...state,
                chartFormOpenThree: true,
                formType: action.payload
            }
        case CLOSE_CHART_FORM_THREE:
            return{
                ...state,
                chartFormOpenThree: false,
                timelineSelectedThree: false,
                singleDatesSelectedThree: false,
                dateCounterThree: 0,
                datesThree:[]
            }
        case OPEN_LOGIN_FORM_FOUR:
            return{
                ...state,
                loginFormOpenFour: true
            }
        case CLOSE_LOGIN_FORM_FOUR:
            return{
                ...state,
                loginFormOpenFour: false
            }
        case OPEN_CHART_FORM_FOUR:
            return{
                ...state,
                chartFormOpenFour: true,
                formType: action.payload
            }
        case CLOSE_CHART_FORM_FOUR:
            return{
                ...state,
                chartFormOpenFour: false,
                timelineSelectedFour: false,
                singleDatesSelectedFour: false,
                dateCounterFour: 0,
                datesFour:[]
            }
        case OPEN_CHART_FORM_REQUESTS:
            return{
                ...state,
                chartFormRequestsOpen: true,
                formType: action.payload
            }
        case CLOSE_CHART_FORM_REQUESTS:
            return{
                ...state,
                chartFormRequestsOpen: false,
                timelineSelectedRequests: false,
                singleDatesSelectedRequests: false,
                dateCounterRequests: 0,
                datesRequests: []
            }
        case OPEN_CHART_FORM_TIMEOUTS:
            return{
                ...state,
                chartFormTimeoutsOpen: true,
                formType: action.payload
            }
        case CLOSE_CHART_FORM_TIMEOUTS:
            return{
                ...state,
                chartFormTimeoutsOpen: false,
                timelineSelectedTimeouts: false,
                singleDatesSelectedTimeouts: false,
                dateCounterTimeouts:0,
                datesTimeouts: []
            }
        case OPEN_CHART_FORM_DOWNTIME:
            return{
                ...state,
                chartFormDowntimeOpen: true,
                formType: action.payload
            }
        case CLOSE_CHART_FORM_DOWNTIME:
            return{
                ...state,
                chartFormDowntimeOpen: false,
                timelineSelectedDowntime: false,
                singleDatesSelectedDowntime: false,
                dateCounterDowntime: 0,
                datesDowntime:[]
            }

        case CREATE_REQUESTS_CHART:
            return{
                ...state,
                itemsRequests: state.itemsRequests.concat(action.payload)
            };
        case CREATE_TIMEOUTS_CHART:
            return{
                ...state,
                itemsTimeouts: state.itemsTimeouts.concat(action.payload)
            };
        case CREATE_DOWNTIME_CHART:
            return{
                ...state,
                itemsDowntime: state.itemsDowntime.concat(action.payload)
            };
        case CREATE_DOWNTIME_TABLE:
            return{
                ...state,
                tablesDowntime: state.tablesDowntime.concat(action.payload)
            };
        case CREATE_REQUESTS_TABLE:
            return{
                ...state,
                tablesRequests: state.tablesRequests.concat(action.payload)
            };
        case CREATE_TIMEOUTS_TABLE:
            return{
                ...state,
                tablesTimeouts: state.tablesTimeouts.concat(action.payload)
            };
        case CREATE_HOME_TABLE:
            return{
                ...state,
                tablesHome: state.tablesHome.concat(action.payload)
            };
        case CREATE_HOME_CHART:
            return{
                ...state,
                itemsHome: state.itemsHome.concat(action.payload)
            };
        case CREATE_HOME_TABLE_TWO:
            return{
                ...state,
                tablesHomeTwo: state.tablesHomeTwo.concat(action.payload)
            };
        case CREATE_HOME_CHART_TWO:
            return{
                ...state,
                itemsHomeTwo: state.itemsHomeTwo.concat(action.payload)
            };
        case CREATE_HOME_TABLE_THREE:
            return{
                ...state,
                tablesHomeThree: state.tablesHomeThree.concat(action.payload)
            };
        case CREATE_HOME_CHART_THREE:
            return{
                ...state,
                itemsHomeThree: state.itemsHomeThree.concat(action.payload)
            };
        case CREATE_HOME_TABLE_FOUR:
            return{
                ...state,
                tablesHomeFour: state.tablesHomeFour.concat(action.payload)
            };
        case CREATE_HOME_CHART_FOUR:
            return{
                ...state,
                itemsHomeFour: state.itemsHomeFour.concat(action.payload)
            };
        case FETCH_REQUESTS_DATA:
            return{
                ...state,
                itemsRequests: action.payload
            };

        case FETCH_DATA:
            return{
                ...state,
                data : action.payload
            };
        case FETCH_TIMEOUTS_DATA:
            return{
                ...state,
                itemsTimeouts: action.payload
            };
        case FETCH_HOME_DATA:
            return{
                ...state,
                itemsHome: action.payload
            };
        case FETCH_HOME_DATA_TWO:
            return{
                ...state,
                itemsHomeTwo: action.payload
            };
        case FETCH_HOME_DATA_THREE:
            return{
                ...state,
                itemsHomeThree: action.payload
            };
        case FETCH_HOME_DATA_FOUR:
            return{
                ...state,
                itemsHomeFour: action.payload
            };
        case FETCH_DOWNTIME_DATA:
            return {
                ...state,
                itemsDowntime: action.payload
        };
        case FETCH_TABLE_DOWNTIME_DATA:
            return{
                ...state,
                tablesDowntime: action.payload
            }
        case FETCH_TABLE_REQUESTS_DATA:
            return{
                ...state,
                tablesRequests: action.payload
            }
        case FETCH_TABLE_TIMEOUTS_DATA:
            return{
                ...state,
                tablesTimeouts: action.payload
            }
        case FETCH_TABLE_DATA_HOME:
            return{
                ...state,
                tablesHome: action.payload
            }
        case FETCH_TABLE_DATA_HOME_TWO:
            return{
                ...state,
                tablesHomeTwo: action.payload
            }
        case FETCH_TABLE_DATA_HOME_THREE:
            return{
                ...state,
                tablesHomeThree: action.payload
            }
        case FETCH_TABLE_DATA_HOME_FOUR:
            return{
                ...state,
                tablesHomeFour: action.payload
            }
        case LOG_OUT:
            return{
                ...state,
                isAccessGranted: false,
                isAdminAccessGranted: false
            }
        case HANDLE_TAB_CHANGE:
            return{
                ...state,
                tabsValue: action.payload
            }
        case DELETE_CHART_HOME:
            return {
                ...state,
                itemsHome: state.itemsHome.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_CHART_HOME_TWO:
            return {
                ...state,
                itemsHomeTwo: state.itemsHomeTwo.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_CHART_HOME_THREE:
            return {
                ...state,
                itemsHomeThree: state.itemsHomeThree.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_CHART_HOME_FOUR:
            return {
                ...state,
                itemsHomeFour: state.itemsHomeFour.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_CHART_REQUESTS:
            return {
                ...state,
                itemsRequests: state.itemsRequests.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_CHART_TIMEOUTS:
            return {
                ...state,
                itemsTimeouts: state.itemsTimeouts.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_CHART_DOWNTIME:
            return {
                ...state,
                itemsDowntime: state.itemsDowntime.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_TABLE_HOME:
            return {
                ...state,
                tablesHome: state.tablesHome.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_TABLE_HOME_TWO:
            return {
                ...state,
                tablesHomeTwo: state.tablesHomeTwo.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_TABLE_HOME_THREE:
            return {
                ...state,
                tablesHomeThree: state.tablesHomeThree.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_TABLE_HOME_FOUR:
            return {
                ...state,
                tablesHomeFour: state.tablesHomeFour.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_TABLE_REQUESTS:
            return {
                ...state,
                tablesRequests: state.tablesRequests.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_TABLE_TIMEOUTS:
            return {
                ...state,
                tablesTimeouts: state.tablesTimeouts.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case DELETE_TABLE_DOWNTIME:
            return {
                ...state,
                tablesDowntime: state.tablesDowntime.filter(item => item.id !== parseInt(action.payload.url[action.payload.url.length - 1]))
            }
        case TIMELINE_SELECTED:
            return{
                ...state,
                timelineSelected: true,
                singleDatesSelected: false,
                dates:[],
                dateCounter: 0
            }
        case SINGLE_DATES_SELECTED:
            return{
                ...state,
                timelineSelected: false,
                singleDatesSelected: true
            }
        case ADD_DATE:
            return{
                ...state,
                dates: state.dates.concat({
                    key: state.dateCounter,
                    year: "",
                    month:""
        }),
                dateCounter: state.dateCounter + 1
            }
        case TIMELINE_SELECTED_TWO:
            return{
                ...state,
                timelineSelectedTwo: true,
                singleDatesSelectedTwo: false,
                datesTwo:[],
                dateCounterTwo: 0
            }
        case SINGLE_DATES_SELECTED_TWO:
            return{
                ...state,
                timelineSelectedTwo: false,
                singleDatesSelectedTwo: true
            }
        case ADD_DATE_TWO:
            return{
                ...state,
                datesTwo: state.datesTwo.concat({
                    key: state.dateCounterTwo,
                    year: "",
                    month:""
                }),
                dateCounterTwo: state.dateCounterTwo + 1
            }
        case TIMELINE_SELECTED_THREE:
            return{
                ...state,
                timelineSelectedThree: true,
                singleDatesSelectedThree: false,
                datesThree:[],
                dateCounterThree:0
            }
        case SINGLE_DATES_SELECTED_THREE:
            return{
                ...state,
                timelineSelectedThree: false,
                singleDatesSelectedThree: true
            }
        case ADD_DATE_THREE:
            return{
                ...state,
                datesThree: state.datesThree.concat({
                    key: state.dateCounterThree,
                    year: "",
                    month:""
                }),
                dateCounterThree: state.dateCounterThree + 1
            }
        case TIMELINE_SELECTED_FOUR:
            return{
                ...state,
                timelineSelectedFour: true,
                singleDatesSelectedFour: false,
                datesFour:[],
                dateCounterFour:0
            }
        case SINGLE_DATES_SELECTED_FOUR:
            return{
                ...state,
                timelineSelectedFour: false,
                singleDatesSelectedFour: true
            }
        case ADD_DATE_FOUR:
            return{
                ...state,
                datesFour: state.datesFour.concat({
                    key: state.dateCounterFour,
                    year: "",
                    month:""
                }),
                dateCounterFour: state.dateCounterFour + 1
            }
        case TIMELINE_SELECTED_REQUESTS:
            return{
                ...state,
                timelineSelectedRequests: true,
                singleDatesSelectedRequests: false,
                datesRequests:[],
                dateCounterRequests: 0
            }
        case SINGLE_DATES_SELECTED_REQUESTS:
            return{
                ...state,
                timelineSelectedRequests: false,
                singleDatesSelectedRequests: true
            }
        case ADD_DATE_REQUESTS:
            return{
                ...state,
                datesRequests: state.datesRequests.concat({
                    key: state.dateCounterRequests,
                    year: "",
                    month:""
                }),
                dateCounterRequests: state.dateCounterRequests + 1
            }
        case TIMELINE_SELECTED_TIMEOUTS:
            return{
                ...state,
                timelineSelectedTimeouts: true,
                singleDatesSelectedTimeouts: false,
                datesTimeouts:[],
                dateCounterTimeouts: 0
            }
        case SINGLE_DATES_SELECTED_TIMEOUTS:
            return{
                ...state,
                timelineSelectedTimeouts: false,
                singleDatesSelectedTimeouts: true
            }
        case ADD_DATE_TIMEOUTS:
            return{
                ...state,
                datesTimeouts: state.datesTimeouts.concat({
                    key: state.dateCounterTimeouts,
                    year: "",
                    month:""
                }),
                dateCounterTimeouts: state.dateCounterTimeouts + 1
            }
        case TIMELINE_SELECTED_DOWNTIME:
            return{
                ...state,
                timelineSelectedDowntime: true,
                singleDatesSelectedDowntime: false,
                datesDowntime: [],
                dateCounterDowntime: 0
            }
        case SINGLE_DATES_SELECTED_DOWNTIME:
            return{
                ...state,
                timelineSelectedDowntime: false,
                singleDatesSelectedDowntime: true
            }
        case ADD_DATE_DOWNTIME:
            return{
                ...state,
                datesDowntime: state.datesDowntime.concat({
                    key: state.dateCounterDowntime,
                    year: "",
                    month:""
                }),
                dateCounterDowntime: state.dateCounterDowntime + 1
            }
        case DELETE_DATE:
            return{
                ...state,
                dates: state.dates.splice(0, state.dates.length - 1),
                dateCounter: state.dateCounter - 1
            }
        case DELETE_DATE_TWO:
            return{
                ...state,
                datesTwo: state.datesTwo.splice(0, state.datesTwo.length - 1),
                dateCounterTwo: state.dateCounterTwo - 1
            }
        case DELETE_DATE_THREE:
            return{
                ...state,
                datesThree: state.datesThree.splice(0, state.datesThree.length - 1),
                dateCounterThree: state.dateCounterThree - 1
            }
        case DELETE_DATE_FOUR:
            return{
                ...state,
                datesFour: state.datesFour.splice(0, state.datesFour.length - 1),
                dateCounterFour: state.dateCounterFour - 1
            }
        case DELETE_DATE_REQUESTS:
            return{
                ...state,
                datesRequests: state.datesRequests.splice(0, state.datesRequests.length - 1),
                dateCounterRequests: state.dateCounterRequests - 1
            }
        case DELETE_DATE_TIMEOUTS:
            return{
                ...state,
                datesTimeouts: state.datesTimeouts.splice(0, state.datesTimeouts.length - 1),
                dateCounterTimeouts: state.dateCounterTimeouts - 1
            }
        case DELETE_DATE_DOWNTIME:
            return{
                ...state,
                datesDowntime: state.datesDowntime.splice(0, state.datesDowntime.length - 1),
                dateCounterDowntime: state.dateCounterDowntime - 1
            }
        case HANDLE_CLOSE_PRIMARY_COLOR:
            return{
                ...state,
                primaryColorOpen: false
            }
        case HANDLE_OPEN_PRIMARY_COLOR:
            return{
                ...state,
                primaryColorOpen: true
            }
        case HANDLE_CLOSE_SECONDARY_COLOR:
            return{
                ...state,
                secondaryColorOpen: false
            }
        case HANDLE_OPEN_SECONDARY_COLOR:
            return{
                ...state,
                secondaryColorOpen: true
            }
        case HANDLE_OPEN_FONTSTYLE:
            return{
                ...state,
                fontstyleOpen: true
            }
        case HANDLE_CLOSE_FONTSTYLE:
            return{
                ...state,
                fontstyleOpen: false
            }
        case HANDLE_OPEN_FONTSIZE:
            return{
                ...state,
                fontsizeOpen: true
            }
        case HANDLE_CLOSE_FONTSIZE:
            return{
                ...state,
                fontsizeOpen: false
            }
        case HANDLE_TOGGLE_LANGUAGE:
            return{
                ...state,
                languageOpen: !state.languageOpen
            }
        case HANDLE_TOGGLE_CHARTS:
            return{
                ...state,
                chartsOpen: !state.chartsOpen
            }
        case HANDLE_TOGGLE_TOOLTIPS:
            return{
                ...state,
                tooltipsEnabled: !state.tooltipsEnabled
            }
        case HANDLE_TOGGLE_STAYLOGGEDIN:
            return{
                ...state,
                stayLoggedIn: !state.stayLoggedIn
            }
        case HANDLE_TOGGLE_DELETECHARTSPOSSIBLE:
            return{
                ...state,
                deleteChartsPossible: !state.deleteChartsPossible
            }
        case CHANGE_FONTSIZE:{
            return{
                ...state,
                fontsize: action.payload
            }
        }
        case CHANGE_FONTSTYLE:{
            return{
                ...state,
                fontstyle: action.payload
            }
        }
        case CHANGE_COLOR_SECONDARY:{
            return{
                ...state,
                colorSecondary:{
                    value: action.payload.value,
                    name: action.payload.name
                }
            }
        }
        case CHANGE_COLOR_PRIMARY:{
            return{
                ...state,
                colorPrimary:{
                    value: action.payload.value,
                    name: action.payload.name
                }
            }
        }
        case RESET_SETTINGS:{
            return{
                ...state,
                colorPrimary:{
                    value: "#0065B3",
                    name: "Blue"
                },
                colorSecondary:{
                    value: "#FF6600",
                    name: "Amber"
                },
                fontsize: 14,
                fontstyle: "Segoe UI",
                deleteChartsPossible: true,
                tooltipsEnabled: true,
                stayLoggedIn: false,
                language: "German",
                library: "Recharts"
            }
        }
        case OPEN_NEW_USER_FORM:{
            return{
                ...state,
                newUserFormOpen: true,
            }
        }
        case CLOSE_NEW_USER_FORM:{
            return{
                ...state,
                newUserFormOpen: false,
            }
        }
        case ADD_USER:{
            return{
                ...state
            }
        }
        case CHANGE_LANGUAGE:{
            return{
                ...state,
                language: action.payload
            }
        }
        case CHANGE_CHART_LIBRARY:{
            return{
                ...state,
                library: action.payload
            }
        }
        case HANDLE_FILTER:{
            return{
                ...state,
                show: !state.show
            }
        }
        case OPEN_HTTP_DIALOG:{
            return{
                ...state,
                httpOpen: true
            }
        }
        case CLOSE_HTTP_DIALOG:{
            return{
                ...state,
                httpOpen: false
            }
        }
        case HANDLE_DRAWER_CHANGE:{
            return{
                ...state,
                drawerIndex: action.payload
            }
        }
        case OPEN_TABLE_COMMENT:{
            return{
                ...state,
                tableCommentOpen: true,
            }
        }
        case CLOSE_TABLE_COMMENT:{
            return{
                ...state,
                tableCommentOpen: false,
            }
        }
        case EDIT_TABLE_COMMENT:{
            return{
                ...state,
                comment: action.payload
            }
        }
        case GET_COMMENT: {
            return{
                ...state,
                comment: action.payload
            }
        }
        case OPEN_CHART_EDIT:{
            return{
                ...state,
                chartEditOpen: true,
            }
        }
        case CLOSE_CHART_EDIT:{
            return{
                ...state,
                chartEditOpen: false
            }
        }
        case EDIT_CHART_COMMENT:{
            return{
                ...state,
                comment: action.payload
            }
        }
        case OPEN_NO_ADMIN_ACCESS:{
            return{
                ...state,
                noAdminAccessOpen: true
            }
        }
        case CLOSE_NO_ADMIN_ACCESS:{
            return{
                ...state,
                noAdminAccessOpen: false
            }
        }
        case OPEN_DOT_DETAILS_DIALOG:{
            return{
                ...state,
                dotDetailDialogOpen: true
            }
        }
        case CLOSE_DOT_DETAILS_DIALOG:{
            return{
                ...state,
                dotDetailDialogOpen: false
            }
        }
        case GET_DOT_DETAILS:{
            return{
                ...state,
                dotDetails: action.payload
            }
        }
        case FETCH_REQUEST_DATA:
            return{
                ...state,
                requestData : action.payload
            };
        default:
            return state;
    }
}
