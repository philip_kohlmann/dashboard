import React, {Component} from "react";
import {connect} from "react-redux";
import {withStyles} from 'material-ui/styles';
//import * as colors from 'material-ui/colors';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core'
import { withRouter } from 'react-router-dom';

export class ThemeWrapper extends Component {
    constructor(props){
        super(props);
    }

    render(){
        var theme = createMuiTheme(
            {
                palette: {
                    primary: { main: this.props.colorPrimary.value},
                    secondary: { main: this.props.colorSecondary.value}
                },
                typography:{
                    fontSize: this.props.fontsize,
                    fontFamily: this.props.fontstyle
                }
    })
        return (
            <MuiThemeProvider theme={theme}>
                <div>
            { this.props.children }
                </div>
                </MuiThemeProvider>
                )
            }

}
    const mapStateToProps = state => ({
        colorPrimary: state.allReducers.colorPrimary,
        colorSecondary: state.allReducers.colorSecondary,
        fontsize: state.allReducers.fontsize,
        fontstyle: state.allReducers.fontstyle
    });

export default ThemeWrapper = withRouter(connect(mapStateToProps)(ThemeWrapper));