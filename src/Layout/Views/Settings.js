import React from 'react'
import Settings from '../../containers/Settings/Settings';

const SettingsView = () => (
    <div>
        <Settings/>
    </div>
);

export default SettingsView