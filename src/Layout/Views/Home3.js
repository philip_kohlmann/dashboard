import React from 'react'
import Dashboard from '../../containers/Dashboards/DashboardThree';
import MenuTabs from '../../containers/Footer';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { renderByOrder } from 'recharts/lib/util/ReactUtils';
import logo from '../../dzLogo2.png';
import TweenMax from 'gsap';
import Form from '../../containers/Form'
import { connect } from 'react-redux';




var myDiv = {
    position: 'relative',
    zIndex: '1',
    textAlign: 'center',
    opacity: '1'
};

var bg = {
    position: 'absolute',
    zIndex: -1,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    background: "url(" + logo + ")",
    backgroundPosition: 'center',
    //backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    textAlign: 'center',
    opacity: .3,
    width: '100%',
    height: '100%',
};
class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loginOpacity: 1,
            boardOpacity: 1
        }


        this.render = this.render.bind(this);
        this.changeOpacity = this.changeOpacity.bind(this);
    }
    changeOpacity(){
        if(this.props.loggedIn){
            this.setState({loginOpacity: 0});
            console.log(this.state.loginOpacity);
        }else{
            this.setState({boardOpacity: 0});

        }
    }



    componentDidUpdate() {
        if(this.props.loggedIn){
            TweenMax.to('.board', 2, { opacity: '1' });


        }else{
            TweenMax.to('.login', 2, { opacity: '1' });


        }

    }
    componentWillUpdate(){

        TweenMax.to('.board', 0.0001, { opacity: '0' });
        TweenMax.to('.login', 0.0001, { opacity: '0' });



    }

    render() {
        if (this.props.loggedIn) {
            return (<div className='board' style={{ opacity: this.state.boardOpacity  }}>
                <Dashboard />
                <MenuTabs />
            </div>)
        } else {
            return (
                <div className='login' style={ {
                    position: 'relative',
                    zIndex: '1',
                    textAlign: 'center',
                    opacity: this.state.loginOpacity
                }} >
                    <div style={bg}>

                    </div>
                    <br />
                    <div style={{  padding: '20%' }}>
                        <div style={{
                            margin: 'auto',
                            padding: '2%',
                            width: '50%',
                            border: '1px solid black',
                            borderRadius: '5px',
                            backgroundColor: 'rgba(255, 255, 255, 0.8)'
                        }}>
                            <Form/>
                        </div>
                    </div>
                </div>)
        }


    };
};

const mapStateToProps = state => ({
    loggedIn: state.allReducers.isAccessGranted

});

export default connect(mapStateToProps)(Home)