import React, { Component } from 'react';
import '../styles/App.css';
import store from '../store';
import {Provider} from 'react-redux';
import Main2 from './Main2';
import {BrowserRouter} from 'react-router-dom';
import MenuTabs from '../containers/Footer';
import ThemeWrapper from "./ThemeWrapper";

class App extends Component {
    render() {
        document.body.style.backgroundColor = "#F5F5F5";
        return (
            <Provider store = {store}><BrowserRouter>
                <ThemeWrapper>
                <div className="App" >
                    <Main2/>
                </div>
                </ThemeWrapper>
            </BrowserRouter></Provider>
        );
    }
}


export default App
