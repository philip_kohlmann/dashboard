import React from 'react'
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import Home from "./Views/Home";
import HomeTwo from './Views/Home2'
import HomeThree from './Views/Home3'
import HomeFour from './Views/Home4'
import {connect} from 'react-redux'
import Requests from "./Views/Requests";
import Downtime from "./Views/Downtime";
import Timeouts from "./Views/Timeouts";
import {openNoAdminAccess, handleDrawerChange} from "../actions/dataActions"
import SettingsView from "./Views/Settings";




class Main2 extends React.Component {

    render() {

        const PrivateRoute = ({component: Component, ...rest}) => (
            <Route {...rest} render={(props) => (
                this.props.loggedInAdmin === true
                    ? <Component {...props} />
                    : this.props.handleDrawerChange(0) && this.props.openNoAdminAccess() && <Redirect to='/'/>
            )}/>
        )
        return (
            <main>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route exact path= '/page=2' component ={HomeTwo}/>
                    <Route exact path= '/page=3' component ={HomeThree}/>
                    <Route exact path= '/page=4' component ={HomeFour}/>
                    <PrivateRoute exact path = '/settings' component ={SettingsView}/>
                    <Route path="/requests" component={Requests}/>
                    <Route path='/downtime' component={Downtime}/>
                    <Route path='/timeouts' component={Timeouts}/>
                </Switch>
            </main>
        )
    }
}


const mapStateToProps = state => ({
    loggedInAdmin: state.allReducers.isAdminAccessGranted
})

export default withRouter(connect(mapStateToProps,{openNoAdminAccess, handleDrawerChange})(Main2))
